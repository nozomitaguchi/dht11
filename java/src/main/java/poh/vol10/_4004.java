package poh.vol10;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * ログの結合　(封印レベルC)
 * <p>
 * あなたは昔に使われていた古いチャットシステムのログの解析を依頼されました。
 * <p>
 * このログのフォーマットは、発言id, 発言内容 がスペース区切り一行毎に入れられています。
 * 発言id は発言ごとに付与されるユニークな id で、発言毎に一意であり連番で入れられています。
 * <p>
 * このログには、一行毎に発言が入れられていますが、
 * 昔のシステムではログの行の文字数に制限があったため、発言は分割されて入れられています。
 * 分割された発言には同じ 発言id が振られているため、発言id をみる事で元の発言を取り出せそうです。
 * <p>
 * 例えば、入力例 1 の場合を図示すると以下のようになります。
 * <p>
 * <p>
 * <p>
 * この例では、"paiza", "onlin", "ehack", "athon" という
 * 4 つの言葉が 発言id 0 で連番で記録されています。
 * このログから、発言id 0 の発言は "paizaonlinehackathon"
 * になります。
 * <p>
 * 分割されているログから分割前の発言を取り出し、発言id の昇順に並べてください。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * N
 * D_1 W_1
 * D_2 W_2
 * ...
 * D_N W_N
 * <p>
 * <p>
 * ・1行目には、ログの行数 N が整数で与えられます。
 * ・続く N 行のうちの i 行目 (1 ≦ i ≦ N) には、発言idを示す整数 D_i、発言内容を示す文字列 W_i が、この順に半角スペース区切りで与えられます。
 * ・入力は合計 N + 1 行であり、 最終行の末尾に改行が1つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・1 ≦ N ≦ 100
 * <p>
 * ・各 i (1 ≦ i ≦ N) に対し
 * 　・0 ≦ D_i ≦ N - 1
 * 　・1 ≦ (W_i の長さ) ≦ 100
 * 　・W_i は半角英小文字のみで構成されている。
 * <p>
 * ・i = 0 に対し D_i = 0 を満たす
 * <p>
 * ・各 i (1 ≦ i ≦ N - 1) に対し
 * 　・D{i + 1} = Di か D{i + 1} = Di + 1 のどちらかを満たす
 * <p>
 * ・各発言の分割前の発言内容は 1,000 文字以下である。
 * <p>
 * 期待する出力
 * <p>
 * 分割前の発言内容を以下の形式で出力してください
 * <p>
 * <p>
 * T_1 C_1
 * T_2 C_2
 * ...
 * T_X C_X
 * <p>
 * <p>
 * ・期待する出力は X (X = D_N + 1) 行からなります。
 * ・i 行目 (1 ≦ i ≦ X) には、それぞれ i 番目の発言の発言 id T_i とその発言 id での発言を結合した発言内容 C_i をこの順で半角スペース区切りで出力してください。
 * ・X 行目の出力の最後に改行を入れ、余計な文字、空行を含んではいけません。
 * 入力例１
 * <p>
 * 入力
 * 4
 * 0 paiza
 * 0 onlin
 * 0 ehack
 * 0 athon
 * <p>
 * 出力
 * 0 paizaonlinehackathon
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 8
 * 0 thequickbr
 * 0 ownfoxjump
 * 0 soverthela
 * 0 zydog
 * 1 jackdawslo
 * 1 vemybigsph
 * 1 inxofquart
 * 1 z
 * <p>
 * 出力
 * 0 thequickbrownfoxjumpsoverthelazydog
 * 1 jackdawslovemybigsphinxofquartz
 */
public class _4004 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    IntStream.range(0, sc.nextInt()).boxed()
        .collect(Collectors.groupingBy(i -> sc.nextInt(),
            Collectors.mapping(i -> sc.next(), Collectors.joining()))
        ).forEach((i, s) -> System.out.println(i + " " + s));
  }
}

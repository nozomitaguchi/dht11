package poh.vol10;

import java.util.Scanner;

/**
 * 回復アイテムを揃えろ　(封印レベルD)
 * <p>
 * あなたはダンジョンに潜るために回復アイテムを買い揃える事になりました。
 * <p>
 * 一人あたりに必要なアイテムの値段 M 円で、仲間の数 N 人分を買い揃える必要があります。
 * <p>
 * 計算して出力するプログラムを作成してください。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * M N
 * <p>
 * <p>
 * ・1 行目に必要なアイテムの値段 M 円、仲間の数 N 人が入力されます。
 * ・入力は 1 行となり、末尾に改行が 1 つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・1 ≦ M ≦ 1000
 * ・1 ≦ N ≦ 5
 * ・M, N はそれぞれ整数
 * <p>
 * 期待する出力
 * <p>
 * 一人あたりに必要なアイテムの値段 M 円で、仲間の数 N 人分を買い揃える際に合計でいくら必要かを出力してください。
 * <p>
 * 出力の最後に改行を入れ、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 250 4
 * <p>
 * 出力
 * 1000
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 124 3
 * <p>
 * 出力
 * 372
 */
public class _4002 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(sc.nextInt() * sc.nextInt());
  }
}

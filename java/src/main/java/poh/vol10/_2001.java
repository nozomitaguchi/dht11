package poh.vol10;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 圧縮　　(封印レベルC)
 * <p>
 * あなたは大量の紙のデータを転送しようとしています。
 * そのままでは時間がかかるため、二値化した後に圧縮をして送る事にしました。
 * <p>
 * 圧縮する方法は、データのうち連続する部分を連続回数に置換する事にしました。
 * <p>
 * 今回は、先頭の色を黒色に固定するため、データの最初に 0 個の黒を挿入して圧縮します。
 * <p>
 * 以下の入力例 1 の場合は、先頭の 0 個の黒と合わせて黒が 3 個、白が 5 個なので "3 5" となります。
 * <p>
 * <p>
 * <p>
 * 白と黒の順番が逆になった入力例 2 では、最初に 0 個の黒が入るので "0 5 3" となります。
 * <p>
 * <p>
 * <p>
 * このように入力されたデータを圧縮してください。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * S
 * <p>
 * <p>
 * ・文字列 S が与えられます。 S は "w"(白), "b"(黒) の二つの文字によって構成されます。
 * ・入力は 1 行となり、末尾に改行が 1 つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・1 ≦ ( S の長さ) ≦ 100
 * ・S には "w"(白) か "b"(黒) の文字しか含まれない
 * <p>
 * 期待する出力
 * <p>
 * 入力されたデータに対する圧縮した結果を半角スペース区切りで出力して下さい。
 * <p>
 * 出力の最後に改行を入れ、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * bbbwwwww
 * <p>
 * 出力
 * 3 5
 * <p>
 * 入力例２
 * <p>
 * 入力
 * wwwwwbbb
 * <p>
 * 出力
 * 0 5 3
 */
public class _2001 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String line = sc.next();
    Matcher matcher = Pattern.compile("w+|b+").matcher(line);
    ArrayList<String> list = new ArrayList<>();
    while (matcher.find()) list.add(String.valueOf(matcher.group().length()));
    System.out.println((line.indexOf('w') == 0 ? "0 " : "") + String.join(" ", list));
  }
}

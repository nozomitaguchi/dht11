package poh.vol10;

import java.util.Scanner;

/**
 * 始まりの呪文　(封印レベルD)
 * <p>
 * 始まりの呪文を唱えましょう。
 * <p>
 * 標準入力から呪文が入力されるので、呪文をそのまま出力してください。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * s
 * <p>
 * <p>
 * ・1 行目で文字列が与えられます。
 * ・入力は 1 行となり、末尾に改行が 1 つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・1 ≦ 文字列 s の長さ ≦ 100
 * ・s は半角英数字で構成された文字列
 * <p>
 * 期待する出力
 * <p>
 * 文字列が 1 行で与えられるので、そのまま文字列を出力してください。
 * 出力の最後に改行を入れ、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * logicsummoner
 * <p>
 * 出力
 * logicsummoner
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 123paiza
 * <p>
 * 出力
 * 123paiza
 */
public class _1003 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(sc.next());
  }
}

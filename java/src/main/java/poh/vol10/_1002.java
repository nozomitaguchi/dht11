package poh.vol10;

import java.util.LinkedList;
import java.util.Scanner;
import java.util.function.BiConsumer;
import java.util.stream.IntStream;

/**
 * 筒の中のボール　(封印レベルC)
 * <p>
 * あなたは筒にボールを収納しています。N 個のボールを筒に入れていきます。筒には左右どちらからでもボールを入れることができ、筒にはちょうど N 個のボールを入れることが出来ます。
 * <p>
 * 筒に i 回目に入れたボールには i という番号を振ります。
 * <p>
 * それぞれのボールを右から入れたか、左から入れたかが与えられる時、筒に入っているボールの番号を左から順に出力するプログラムをつくりましょう。
 * <p>
 * 例)
 * <p>
 * 入力例 1 の状況を考えてみましょう。 5 個のボールを筒に入れることを考えます。 1 番目のボールは左から、 2 番目のボールは左から、 3 番目のボールは右から、 4 番目のボールは左から、 5 番目のボールは右から入れます。
 * <p>
 * これを図で表すと、次のようになります。
 * <p>
 * <p>
 * <p>
 * よって、このときは
 * <p>
 * <p>
 * 4 2 1 3 5
 * <p>
 * <p>
 * と出力します。
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * N
 * s
 * <p>
 * <p>
 * ・1 行目に、筒に入れるボールの個数を表す整数 N が与えられます。
 * ・2 行目に、筒にどのようにボールを入れるかを表す文字列 s が与えられます。文字列 s の i 番目 (1 ≦ i ≦ N) の文字が "L" の場合、 i 番目のボールを左から入れることを表します。 文字列 s の j 番目 (1 ≦ j ≦ N) の文字が "R" の場合、 i 番目のボールを右から入れることを表します。
 * ・入力は合計で 2 行となり、入力値最終行の末尾に改行が １ つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・1 ≦ N ≦ 100
 * ・(文字列 s の長さ) = N
 * ・s の i 番目 (1 ≦ i ≦ N) の文字は必ず "L" もしくは "R"
 * <p>
 * 期待する出力
 * <p>
 * N 個のボールを筒に入れた後に、筒に入っているボールの番号を表す整数を左から順に以下の形式で出力してください。
 * <p>
 * <p>
 * a_1 a_2 ... a_N
 * <p>
 * <p>
 * 1 行に、左から i 番目 (1 ≦ i ≦ N) のボールの番号を表す整数 a_i を、半角スペース区切りで出力して下さい。
 * <p>
 * 出力の最後に改行を入れ、余計な文字、空行を含んではいけません。
 * 入力例１
 * <p>
 * 入力
 * 5
 * LLRLR
 * <p>
 * 出力
 * 4 2 1 3 5
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 6
 * LRLRLR
 * <p>
 * 出力
 * 5 3 1 2 4 6
 */
public class _1002 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    char[] s = sc.next().toCharArray();
    LinkedList<String> tube = new LinkedList<>();
    IntStream.range(0, n).forEach(i -> selectAdditionalMethod(s[i]).accept(tube, String.valueOf(i + 1)));
    System.out.println(String.join(" ", tube));
  }

  /**
   * リストへの追加方法を選択取得する。
   * Lならば先頭追加、それ以外は後方追加。
   *
   * @param c 追加方法の判定用文字
   * @return リストへの追加方法
   */
  private static BiConsumer<LinkedList, String> selectAdditionalMethod(Character c) {
    return 'L' == c ? LinkedList<String>::addFirst : LinkedList<String>::addLast;
  }
}

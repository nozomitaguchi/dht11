package poh.vol10;

import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 魔法陣　(封印レベルB)
 * <p>
 * あなたは魔法陣を作成しています。
 * <p>
 * ここで、N 行 N 列の魔方陣とは、
 * 1 から N^2 までの数字を N 行 N 列に並べたもので、各行、各列、および 2 つの対角線それぞれについて、そこに並んだ数の総和がいずれも等しくなるようなものを言います。
 * <p>
 * 例えば、3 行 3 列の魔方陣の一例は以下のようになります。
 * <p>
 * <p>
 * <p>
 * 誤ってこの魔方陣にコーヒーをこぼしてしまったため、魔方陣の中の 2 つの数字が見えなくなってしまいました。
 * <p>
 * 優秀な魔法使いであるあなたは、2 つの見えなくなってしまった数を補うことで、この魔方陣を修復しようと考えました。魔方陣の行数・列数を表す N、および見えている数字の情報が与えられるので、魔方陣を修復してください。
 * <p>
 * 例えば、入力例 1 を説明する図は以下のようになります。
 * <p>
 * <p>
 * <p>
 * この例では 3 行 3 列の魔法陣を作りましたが、3 行目の 2 列目と 3 行目の 3 列目が見えなくなってしまいました。魔法陣の中で見えていない数字は 4, 9 の二種類です。
 * <p>
 * 3 行目の 2 列目が 4 で、3 行目の 3 列目が 9 である可能性と3 行目の 2 列目が 9 で、3 行目の 3 列目が 4 である可能性との 2 つの可能性が考えられます。後者は正しい魔法陣となりますが、前者は正しい魔法陣とはなりません。よって、答えとしては図 1 の魔法陣を出力します。なお、正しい魔法陣が一通りであることは保証されています。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * N
 * m_{1, 1} m_{1, 2} ... m_{1, N}
 * m_{2, 1} m_{2, 2} ... m_{2, N}
 * ...
 * m_{N, 1} m_{N, 2} ... m_{N, N}
 * <p>
 * <p>
 * ・1 行目に作った魔法陣の大きさ N が与えられます。
 * ・続く N 行のうちの i 行目 (1 ≦ i ≦ N) には N 個の整数が半角スペース区切りで与えられます。i 行目の j 番目 (1 ≦ i ≦ N) の整数 m_{i, j} は作った魔法陣の i 行 j 列目の数を表します。ただし、コーヒーをこぼして見えなくなった部分は 0 になっています。
 * ・入力は合計で N + 1 行となり、入力値最終行の末尾に改行が 1 つ入ります
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・3 ≦ N ≦ 10
 * ・0 ≦ m_{i, j} ≦ N*N (1 ≦ i ≦ N, 1 ≦ j ≦ N)
 * ・m_{1, 1}, m_{1, 2}, ..., m_{N, N} の中で 2 つだけ 0 になるものが存在する
 * ・m_{i, j} ≠ 0, m_{k, l} ≠ 0 となる i, j, k, l について (i, j) ≠ (k, l) のとき m_{i, j} ≠ m_{k, l} が成り立つ (1 ≦ i, j, k, l ≦ N)
 * <p>
 * 期待する出力
 * <p>
 * 修復した魔法陣を出力してください。
 * 答えとしてあり得る魔法陣は一通りであることが保証されています。
 * <p>
 * 出力の最後に改行を入れ、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 3
 * 6 1 8
 * 7 5 3
 * 2 0 0
 * <p>
 * 出力
 * 6 1 8
 * 7 5 3
 * 2 9 4
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 5
 * 1 15 8 24 0
 * 19 3 21 12 10
 * 13 0 20 6 4
 * 25 9 2 18 11
 * 7 16 14 5 23
 * <p>
 * 出力
 * 1 15 8 24 17
 * 19 3 21 12 10
 * 13 22 20 6 4
 * 25 9 2 18 11
 * 7 16 14 5 23
 */
public class _2002 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int[] magic = IntStream.range(0, n * n).map(i -> sc.nextInt()).toArray(),
        zeros = IntStream.range(0, n * n).filter(i -> magic[i] == 0).toArray();
    // 横並びに０があるかにより向きを変える．
    Function<Integer, IntStream> groupByColumn = i -> IntStream.range(0, n).map(j -> magic[i + j * n]),
        groupByRow = i -> IntStream.range(0, n).map(j -> magic[j + i * n]),
        groupLine = zeros[0] / n == zeros[1] / n ? groupByColumn : groupByRow;
    // 列の合計から，置き換える数値を取得する．
    int[] lineSums = IntStream.range(0, n).map(i -> groupLine.apply(i).sum()).toArray();
    int max = Arrays.stream(lineSums).max().orElse(0);
    int[] targetNums = Arrays.stream(lineSums).filter(i -> i < max).map(i -> max - i).toArray();
    // 収まりのいい方に入れる．
    boolean inOrder = groupLine.apply(zeros[0] / n).sum() == max + targetNums[0];
    magic[zeros[0]] = targetNums[inOrder ? 1 : 0];
    magic[zeros[1]] = targetNums[inOrder ? 0 : 1];

    IntStream.range(0, n).mapToObj(i ->
        groupByRow.apply(i).mapToObj(String::valueOf).collect(Collectors.joining(" "))
    ).forEach(System.out::println);
  }
}

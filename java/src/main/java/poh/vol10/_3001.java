package poh.vol10;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * 攻撃コマンド　(封印レベルD)
 * <p>
 * あなたは敵との戦闘を行っています。
 * 戦闘はコマンド方式で行われています。
 * <p>
 * コマンドは攻撃コマンド Attack, 防御コマンド Defense のどちらかを選択します。
 * <p>
 * Attack を選択すると 100 のダメージを相手に与えます。
 * <p>
 * Defense を選択するとダメージを与えることはありません。
 * <p>
 * 5回分のコマンドが与えられるので合計で何ダメージを与えたかを出力してください。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * a_1
 * a_2
 * a_3
 * a_4
 * a_5
 * <p>
 * <p>
 * ・1 行目 から 5行目 に Attack もしくは Defense のどちらかの文字列が与えられます。
 * ・入力は 5 行となり、末尾に改行が 1 つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・a_1, a_2, a_3, a_4, a_5 はそれぞれ "Attack" もしくは "Defense" のどちらかの文字列
 * <p>
 * 期待する出力
 * <p>
 * 5回分のコマンドが与えられるので合計で何ダメージを与えたかを出力してください。
 * <p>
 * 出力の最後に改行を入れ、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * Attack
 * Attack
 * Defense
 * Defense
 * Attack
 * <p>
 * 出力
 * 300
 * <p>
 * 入力例２
 * <p>
 * 入力
 * Attack
 * Attack
 * Attack
 * Attack
 * Attack
 * <p>
 * 出力
 * 500
 */
public class _3001 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long count = IntStream.range(0, 5).filter(i -> "Attack".equals(sc.next())).count();
    System.out.println(count * 100);
  }
}

package poh.vol10;

import java.util.Arrays;
import java.util.Scanner;

/**
 * ダメージ床　(封印レベルD)
 * <p>
 * あなたはダンジョンの一本道を通ろうとしています。
 * しかし、その一本道にはダメージを受ける床があります。ダメージを受けても体力が0にならなければ通る事にしました。
 * <p>
 * 現在の体力と、一本道の床の地図データが与えられます。
 * 床のデータは 0 と 1 で構成された文字列で与えられ 1 はダメージ床を表します。
 * <p>
 * 体力が1以上残る場合は一本道を通った後の体力、体力が 0 以下になる場合は No を出力するプログラムを作成してください。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * s
 * t
 * <p>
 * <p>
 * ・1 行目は文字列で一本道の床の地図データが与えられます。 0 と 1 で構成された文字列で 1 はダメージ床を表します。
 * ・2 行目は整数で現在の体力が与えられます。
 * ・入力は 2 行となり、末尾に改行が 1 つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・1 ≦ 文字列 s の長さ ≦ 100
 * ・1 ≦ t ≦ 100
 * ・s は 0 と 1 で構成された文字列
 * ・t は整数
 * <p>
 * 期待する出力
 * <p>
 * 体力が1以上残る場合は一本道を通った後の体力、体力が 0 以下になる場合は "No" を出力してください。
 * <p>
 * 出力の最後に改行を入れ、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 011100110111
 * 10
 * <p>
 * 出力
 * 2
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 11110011010111011101111111010
 * 20
 * <p>
 * 出力
 * No
 */
public class _3003 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long damage = Arrays.stream(sc.next().split("")).filter("1"::equals).count();
    long hp = sc.nextInt() - damage;
    System.out.println(0 < hp ? hp : "No");
  }
}

package poh.vol09;

import java.util.Arrays;
import java.util.Scanner;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 桂乃梨子とピンチを乗り越えろ　(Cランク問題)
 * <p>
 * 桂乃梨子は100マス計算のアプリの開発をしています。
 * 更に拡張したアプリの開発を進め N 行 N 列 にしたものをリリースしようとしています。
 * あなたはアプリのリリースに向け開発の手助けをすることにしました。
 * <p>
 * この N^2 マス計算の例を以下の図で示します。この例は N = 3 で 入力例1 と同じ例です。
 * この計算では、 左側の表のマス目の空白部分を左端にある数字と上端にある数字を
 * 足し合わせた数で埋めます。埋めた結果が右側の表となります。
 * <p>
 * <p>
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * N
 * c_1 c_2 ... c_N
 * r_1 r_2 ... r_N
 * <p>
 * ・1 行目に、表の行と列の数を表す整数 N が与えられます。
 * ・2 行目に、N 個の整数が半角スペース区切りで与えられます。
 * 　この i 番目 (1 ≦ i ≦ N) の整数 c_i は i 列目に書かれている整数を表します。
 * ・3 行目に、N 個の整数が半角スペース区切りで与えられます。
 * 　この i 番目 (1 ≦ i ≦ N) の整数 r_i は i 行目に書かれている整数を表します。
 * ・入力は合計 3 行であり、 最終行の末尾に改行が1つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・1 ≦ N ≦ 50
 * ・各 i (1 ≦ i ≦ N) に対し、以下を満たします
 * 　・0 ≦ c_i ≦ 1,000
 * 　・0 ≦ r_i ≦ 1,000
 * <p>
 * 期待する出力
 * <p>
 * N^2 マス計算の結果を以下の形式で出力してください。
 * <p>
 * <p>
 * s_{1, 1} s_{1, 2} ... s_{1, N}
 * s_{2, 1} s_{2, 2} ... s_{2, N}
 * ...
 * s_{N, 1} s_{N, 2} ... s_{N, N}
 * <p>
 * <p>
 * ・期待する出力は N 行からなります。
 * ・i 行目 (1 ≦ i ≦ N) にはそれぞれ, i 列目 の数字 と j 行目 (1 ≦ j ≦ N) の数字を
 * 　足した結果である, s_{i, j} をスペース区切りで出力してください。
 * ・すべて整数で出力してください。
 * ・N 行目の出力の最後に改行を入れ、余計な文字、空行を含んではいけません。
 * 入力例１
 * <p>
 * 入力
 * 3
 * 1 2 3
 * 4 6 8
 * <p>
 * 出力
 * 5 6 7
 * 7 8 9
 * 9 10 11
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 5
 * 8 7 6 5 4
 * 1 2 3 4 5
 * <p>
 * 出力
 * 9 8 7 6 5
 * 10 9 8 7 6
 * 11 10 9 8 7
 * 12 11 10 9 8
 * 13 12 11 10 9
 */
public class _0006 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    Supplier<int[]> f = () -> Stream.generate(sc::nextInt).limit(n).mapToInt(Integer::valueOf).toArray();
    int[] row = f.get(), column = f.get();

    IntFunction<String> createMassCalculationLine = x -> Arrays.stream(row)
        .map(i -> i + x)
        .mapToObj(String::valueOf)
        .collect(Collectors.joining(" "));

    Arrays.stream(column).mapToObj(createMassCalculationLine).forEach(System.out::println);
  }
}

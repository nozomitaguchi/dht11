package poh.vol09;

import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 芦屋川雛乃からのヘルプ依頼　(Cランク問題)
 * <p>
 * 芦屋川雛乃は簡易的な暗号のプログラムを作成で困っています。
 * あなたはその暗号プログラムを完成させて下さい。
 * <p>
 * 通信される内容は数字列のみとします。
 * <p>
 * 暗号化は、変換対応表に従い各数字を別の数字に変換することで行います。
 * "257" を暗号化の例を次に示しました。 2, 5, 7 がそれぞれ、4, 3, 5 に変換されるためです。
 * <p>
 * <p>
 * <p>
 * また、通信には暗号化された数字列をもとの数字列に復元する必要があります。
 * この復元する作業を復号と呼ぶことにします。
 * 例えば、上の変換対応表にて、"8179" を復号すると、"0136" となります。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * a_0 a_1 a_2 ... a_9
 * s
 * t
 * <p>
 * 1 行目には、変換対応表が与えられます。暗号化の際、i は a_i に変換されます。復号の時は、a_i は i に変換されます。
 * 2 行目には暗号化すべきであるか復号すべきであるかを表す文字列 s が与えられます。s は "encode" もしくは "decode" です。"encode" の場合は暗号化、"decode" の場合は復号してください。
 * 3 行目には暗号化もしくは復号すべき文字列 t が与えられます。
 * 入力は合計で 3 行となり、入力値最終行の末尾に改行が１つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・a_i は 0 から 9 までの整数 (0 ≦ i ≦ 9)
 * ・i ≠ j のとき、a_i ≠ a_j
 * ・t は 0 から 9 までの数字からなる文字列
 * ・1 ≦ 文字列 t の長さ ≦ 10
 * <p>
 * 期待する出力
 * <p>
 * 文字列 t を暗号化あるいは復号した結果を出力してください。
 * <p>
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 8 1 4 7 2 3 9 5 6 0
 * encode
 * 257
 * <p>
 * 出力
 * 435
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 8 3 7 1 2 5 6 0 9 4
 * decode
 * 0728
 * <p>
 * 出力
 * 7240
 */
public class _0005 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int[] encodes = Stream.generate(sc::nextInt).limit(10).mapToInt(Integer::valueOf).toArray();
    Collector<Integer, ?, Map<String, Integer>> getMap = "encode".equals(sc.next()) ?
        Collectors.toMap(String::valueOf, i -> encodes[i]) :
        Collectors.toMap(i -> String.valueOf(encodes[i]), Function.identity());
    Map<String, Integer> map = IntStream.rangeClosed(0, 9).boxed().collect(getMap);
    String changed = Arrays.stream(sc.next().split(""))
        .map(map::get)
        .map(String::valueOf)
        .collect(Collectors.joining());
    System.out.println(changed);
  }
}

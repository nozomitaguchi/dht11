package poh.vol09;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntUnaryOperator;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 緑川つばめを窮地から救え　(Bランク問題)
 * <p>
 * 緑川つばめは会計システムのバグの調査のために特殊な四捨五入処理をしているか検証をしています。
 * あなたは以下のルールに従った四捨五入の処理のプログラムを作成することになりました。
 * <p>
 * 1 以上の整数 A が与えられます。
 * A の好きな桁に対して好きな順番で好きな回数だけ四捨五入をおこなって、できるだけ大きな整数を作ってください。
 * <p>
 * たとえば、A = 247 に対して 4 通りの方法で四捨五入をおこなったときの結果は次の図のようになります。
 * <p>
 * <p>
 * <p>
 * 247 をどのような方法で四捨五入しても 300 より大きい整数を作ることはできないので、答えは 300 になります。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * A
 * <p>
 * ・1 行目には整数 A が与えられます。
 * ・入力は合計で 1 行となり、入力値最終行の末尾に改行が１つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・1 ≦ A < 10^7
 * ・A は整数
 * <p>
 * 期待する出力
 * <p>
 * 整数 A の好きな桁を好きな順番で四捨五入して得られる最大の整数を出力してください。
 * <p>
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 247
 * <p>
 * 出力
 * 300
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 31
 * <p>
 * 出力
 * 31
 */
public class _0009 {
  /*
   * 数字を左からなめた時,
   * 最初に 5以上が出現する 1桁上が ++ポイント.
   * なお, 上に 4が続く限り ++ポイントが繰り上がる.
   */
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    // 桁が増える場合も考慮する
    String a = "0" + sc.next();
    int[] number = Arrays.stream(a.split("")).mapToInt(Integer::valueOf).toArray();
    Supplier<IntStream> indexRange = () -> IntStream.range(0, number.length);
    List<Integer> indicesHave4 = indexRange.get().filter(i -> 4 == number[i]).boxed().collect(Collectors.toList());
    // 繰り上げる必要がない場合は -1で表現する
    AtomicInteger p = new AtomicInteger(indexRange.get().filter(i -> 5 <= number[i]).findFirst().orElse(-1));
    while (0 < p.get()) if (!indicesHave4.contains(p.decrementAndGet())) break;
    // ++ポイント以下は 0埋めになる
    IntUnaryOperator f = i -> i < p.get() ? number[i] : i == p.get() ? number[i] + 1 : 0;
    String incremented = indexRange.get().map(f).mapToObj(String::valueOf).collect(Collectors.joining());
    System.out.println(Integer.valueOf(p.get() == -1 ? a : incremented));
  }
}

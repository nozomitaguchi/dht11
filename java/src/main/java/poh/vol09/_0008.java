package poh.vol09;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * 六村リオの緊急事態　(Bランク問題)
 * <p>
 * 六村リオは Unix系 のシステムのメンテナンスを担当しています。
 * 今まで相対パスで指定していた部分を絶対パスに変える事になりました。
 * <p>
 * 入力例1 では 相対パス ../test が /home/paiza がカレントディレクトリである際に与えられています。
 * このパスは以下の画像のように .. で親の home を一度指し、その子ディレクトリである test を指しています。
 * パスの表記において . は現在のディレクトリ、 .. は一つ手前のディレクトリを表しています。
 * <p>
 * <p>
 * <p>
 * また、ファイル階層の頂点である / ディレクトリの親ディレクトリは / 自身を指します。
 * このため、入力例2 では / の親ディレクトリである / を指します。
 * <p>
 * このような相対パスで指定した場所を絶対パスへ変換してください。
 * <p>
 * 入力には paiza の実行環境上で直接ディレクトリを読み込んだり、
 * ディレクトリに書き込んだりする動作を含む解法は不正解となるケースが含まれます。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * /c_1/c_2/.../c_M p_1/p_2/.../p_N
 * <p>
 * ・"/" (半角スラッシュ) に続き、カレントディレクトリの絶対パス C を表す整数 (= M) 個の文字列が半角スラッシュ区切りで与えられます。
 * 　この i 番目 (1 ≦ i ≦ M) の文字列 c_i は、このパスが i 番目に通るディレクトリの名前を表します。
 * ・さらに、半角スペースに続いてそこからの相対パス P を表す整数 (=N) 個の文字列が半角スラッシュ区切りで与えられます。
 * 　この j 番目 (1 ≦ j ≦ N) の文字列 p_j は、このパスが j 番目に通るディレクトリの名前、あるいはそのディレクトリを指す記号を表します。
 * ・入力は 1 行であり、末尾に改行が 1 つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・1 ≦ (絶対パス C の長さ) ≦ 500
 * ・1 ≦ (相対パス P の長さ) ≦ 500
 * ・各 i (1 ≦ i ≦ M) に対して以下を満たす
 * 　・c_i は英字小文字からなる文字列である
 * ・各 j (1 ≦ j ≦ N) に対して以下を満たす
 * 　・p_j は 英字小文字からなる文字列, "..", "." のいずれかである。
 * <p>
 * 期待する出力
 * <p>
 * カレントディレクトリと相対パスから絶対パスを出力してください。
 * 絶対パスの先頭は "/" から始まり、 絶対パスが "/" である場合を除き、末尾に "/" を入れないで答えてください。
 * <p>
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * /home/paiza ../test
 * <p>
 * 出力
 * /home/test
 * <p>
 * 入力例２
 * <p>
 * 入力
 * / ..
 * <p>
 * 出力
 * /
 * <p>
 * 入力例３
 * <p>
 * 入力
 * /root/paiza io/../../paiza/io/../../../../home/paiza
 * <p>
 * 出力
 * /home/paiza
 */
public class _0008 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Supplier<List<String>> f = () ->
        Arrays.stream(sc.next().split("/", -1))
            .filter(s -> !"".equals(s) && !".".equals(s))
            .collect(Collectors.toList());
    List<String> c = f.get(), p = f.get();

    AtomicInteger i = new AtomicInteger(c.size());
    p.forEach(s -> {
      if (!"..".equals(s)) {
        c.add(i.getAndIncrement(), s);
      } else if (0 < i.get()) {
        c.remove(i.decrementAndGet());
      }
    });

    System.out.println(c.size() == 0 ? "/" : c.stream().map(s -> "/" + s).collect(Collectors.joining()));
  }
}

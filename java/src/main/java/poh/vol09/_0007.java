package poh.vol09;

import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 水無瀬朋の一大事　(Cランク問題)
 * <p>
 * 水無瀬朋は八百屋さんの通販サイトのシステムを担当していますが、感謝の気持ちを込めてまとめ買い値引きキャンペーンを実施することになりその実装で困っています。
 * <p>
 * キャンペーン対象の商品は N 種類となっています。このキャンペーンでは、1 個あたり p_i 円の商品 i を s_i 個まとめて買うと、支払い金額から d_i 円値引きします。
 * <p>
 * K 個の購入情報 (購入した商品の番号 c_j と数量 a_j) が与えられるので、それぞれの割引後の支払金額を求めてください。
 * <p>
 * 例)
 * <p>
 * <p>
 * <p>
 * <p>
 * キャンペーン内容が上の図のようになっていた場合、購入内容から支払金額は以下のように求められます。
 * <p>
 * 商品 1 を 6 個購入
 * → 6 ÷ 5 = 1 あまり 1 なので割引が 1 回適用でき、支払いは 20 x 6 - 10 x 1 = 110 円
 * <p>
 * 商品 2 を 12 個購入
 * → 12 ÷ 6 = 2 なので割引が 2 回適用でき、支払いは 45 x 12 - 40 x 2 = 460 円
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * N
 * p_1 s_1 d_1
 * p_2 s_2 d_2
 * ...
 * p_N s_N d_N
 * M
 * c_1 a_1
 * c_2 a_2
 * ...
 * c_M a_M
 * <p>
 * ・1 行目には、キャンペーン対象となる商品の種類の総数を表す整数 N が与えられます。
 * ・続く N 行のうちの i 行目 (1 ≦ i ≦ N) には、商品 i の単価を表す整数 p_i と、キャンペーンの詳細を表す 2 つの整数 s_i, d_i がこの順に半角スペース区切りで与えられます。
 * 　・これは、商品 i を s_i 個まとめて買うと d_i 円値引きする、ということを表します。
 * ・続く行には、入力される購入内容の総数を表す整数 M が与えられます。
 * ・続く M 行の中の j 行目 (1 ≦ j ≦ M) には、購入された商品の番号を表す整数 c_j、その商品の購入数を表す整数 a_j がこの順に半角スペース区切りで与えられます。
 * ・入力は合計で N + M + 2 行となり、入力値最終行の末尾に改行が 1 つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・1 ≦ N ≦ 100
 * ・1 ≦ M ≦ 1,000
 * ・各 i (1 ≦ i ≦ N) について、
 * 　・1 ≦ p_i ≦ 1,000
 * 　・1 ≦ s_i ≦ 100
 * 　・1 ≦ d_i < p_i、すなわち割引価格は元の商品の単価より小さく、支払金額は負にはならない
 * ・各 j (1 ≦ j ≦ M) について、
 * 　・1 ≦ c_j ≦ N
 * 　・1 ≦ a_j ≦ 100
 * <p>
 * 期待する出力
 * <p>
 * 各購入内容に対する割引後の支払金額を以下の形式で出力してください。
 * <p>
 * <p>
 * P_1
 * P_2
 * ...
 * P_M
 * <p>
 * <p>
 * ・期待する出力は M 行からなります。
 * ・出力の j 行目 (1 ≦ j ≦ M) には、j 番目の購入内容に対する割引後の支払金額 P_j を出力して下さい。
 * ・最後は改行し、余計な文字、空行を含んではいけません。
 * 入力例１
 * <p>
 * 入力
 * 2
 * 20 5 10
 * 45 6 40
 * 2
 * 1 6
 * 2 12
 * <p>
 * 出力
 * 110
 * 460
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 4
 * 30 2 10
 * 55 5 40
 * 100 1 2
 * 25 10 25
 * 6
 * 1 1
 * 2 1
 * 3 3
 * 1 3
 * 2 9
 * 4 52
 * <p>
 * 出力
 * 30
 * 55
 * 294
 * 80
 * 455
 * 1175
 */
public class _0007 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    // 商品数を受け取って、計算する関数を作る
    Function<Integer, IntUnaryOperator> getCalc = i -> {
      int p = sc.nextInt(), s = sc.nextInt(), d = sc.nextInt();
      return a -> p * a - (a / s) * d;
    };
    Map<Integer, IntUnaryOperator> map =
        IntStream.rangeClosed(1, sc.nextInt()).boxed().collect(Collectors.toMap(Function.identity(), getCalc));

    int m = sc.nextInt();
    Stream.generate(() -> map.get(sc.nextInt()).applyAsInt(sc.nextInt()))
        .limit(m).forEach(System.out::println);
  }
}

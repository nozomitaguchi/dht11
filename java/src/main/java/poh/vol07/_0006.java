package poh.vol07;

import java.util.Comparator;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 「ポニーテールセット」ゲットチャレンジ！
 * <p>
 * paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * ある数字 n が入力されます。 n から 0 までカウントダウンし 0 の時のみ「0!!」と出力するプログラムを作成して下さい。
 * <p>
 * 例えば 5 からカウントが始まった場合
 * <p>
 * 5
 * 4
 * 3
 * 2
 * 1
 * 0!!
 * <p>
 * のように出力してください。
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * n
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ n ≦ 100
 * n は整数
 * 期待する出力
 * <p>
 * n から 1 までを順番に 1 ずつ減らしながら出力し、最後に 0!! と出力してください( ! は半角)。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 5
 * <p>
 * 出力
 * 5
 * 4
 * 3
 * 2
 * 1
 * 0!!
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 10
 * <p>
 * 出力
 * 10
 * 9
 * 8
 * 7
 * 6
 * 5
 * 4
 * 3
 * 2
 * 1
 * 0!!
 */
public class _0006 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Stream<Integer> countDown = IntStream.rangeClosed(0, sc.nextInt()).boxed().sorted(Comparator.reverseOrder());
    countDown.map(i -> i + (i == 0 ? "!!" : "")).forEach(System.out::println);
  }
}

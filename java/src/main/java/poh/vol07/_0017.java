package poh.vol07;

import java.util.Scanner;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 「縞ニーソセット」ゲットチャレンジ！
 * <p>
 * paizaランクC相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * 赤と白の等間隔の縞模様をあなたは作ろうとしています。
 * <p>
 * 各色の幅 n 、縞模様全体の長さ m が改行区切りで入力されます。
 * 赤を 'R' 、白を 'W' とした赤から始まる縞模様を出力して下さい。
 * <p>
 * 例えば
 * <p>
 * <p>
 * 3
 * 10
 * <p>
 * <p>
 * のような入力の場合
 * <p>
 * RRRWWWRRRW
 * <p>
 * <p>
 * のように出力してください。
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * n
 * m
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ n ≦ 10
 * 1 ≦ m ≦ 20
 * <p>
 * n, m は整数
 * 期待する出力
 * <p>
 * 各色の幅 n 、縞模様全体の長さ m の赤を 'R' 、白を 'W' とした赤から始まる縞模様を出力して下さい。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 3
 * 10
 * <p>
 * 出力
 * RRRWWWRRRW
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 5
 * 4
 * <p>
 * 出力
 * RRRR
 */
public class _0017 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt(), m = sc.nextInt();
    UnaryOperator<String> repeat = s ->
        Stream.generate(() -> s).limit(n).collect(Collectors.joining());
    Supplier<String> getRWs = () -> repeat.apply("R") + repeat.apply("W");
    String answer = Stream.generate(getRWs)
        .limit(m / n + 1).collect(Collectors.joining()).substring(0, m);
    System.out.println(answer);
  }
}

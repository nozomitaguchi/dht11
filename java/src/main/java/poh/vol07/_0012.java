package poh.vol07;

import java.awt.*;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 「サンタ服」ゲットチャレンジ！
 * <p>
 * paizaランクB相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * 幅 X cm、奥行き Y cm、高さ Z cm の直方体の形をしたケーキがあります。 このケーキに「側面と平行な方向」および「前面と平行な方向」に何回か包丁を入れて、小さなケーキに切り分けることを考えます。 上面と平行な方向（水平方向）には包丁を入れません。
 * <p>
 * <p>
 * 包丁を入れる場所が与えられたとき、切り分けられたケーキの中で最も体積が小さいものを求めてください。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * X Y Z N
 * d_1 a_1
 * d_2 a_2
 * ...
 * d_N a_N
 * <p>
 * 1 行目にはケーキの幅、奥行き、高さを表す整数 X、Y、Z および、包丁を入れる回数を表す整数 N が入力されます。 続く N 行には包丁を入れる場所の情報が整数で与えられます。
 * d_i a_i
 * <p>
 * は
 * d_i = 0 のとき、「側面と平行な方向」で左側面からの距離が a_i cm の位置に包丁を入れることを、
 * d_i = 1 のとき、「前面と平行な方向」で前面からの距離が a_i cm の位置に包丁を入れることを
 * <p>
 * それぞれ表しています。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ X, Y, Z, N ≦ 100
 * d_i は 0 または 1
 * d_i = 0 のとき 1 ≦ a_i ≦ X-1
 * d_i = 1 のとき 1 ≦ a_i ≦ Y-1
 * 同じ場所に 2 回以上包丁を入れることはない
 * 入力される値はすべて整数
 * 期待する出力
 * <p>
 * 切り分けられたケーキの中で体積が最も小さいものの体積 (単位は cm^3) を 1 行に出力してください。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 10 10 10 2
 * 0 3
 * 0 8
 * <p>
 * 出力
 * 200
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 20 40 10 5
 * 1 34
 * 1 17
 * 0 7
 * 1 6
 * 0 11
 * <p>
 * 出力
 * 240
 */
public class _0012 {
  @SuppressWarnings("SuspiciousNameCombination")
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int x = sc.nextInt(), y = sc.nextInt(), z = sc.nextInt(), n = sc.nextInt();
    Stream<Point> carvingPoints = Stream.generate(() -> new Point(sc.nextInt(), sc.nextInt())).limit(n);
    Map<Integer, List<Integer>> carvingSizes =
        Stream.concat(carvingPoints, Stream.of(new Point(0, 0), new Point(0, x), new Point(1, 0), new Point(1, y)))
            .collect(Collectors.groupingBy(i -> i.x, Collectors.mapping(i -> i.y, Collectors.toList())));

    Function<List<Integer>, Integer> minDifference = list -> {
      int[] array = list.stream().mapToInt(Integer::valueOf).sorted().toArray();
      return IntStream.range(1, array.length).map(i -> array[i] - array[i - 1]).min().orElse(0);
    };

    System.out.println(minDifference.apply(carvingSizes.get(0)) * minDifference.apply(carvingSizes.get(1)) * z);
  }
}

package poh.vol07;

import java.awt.*;
import java.util.Scanner;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 「めがね」ゲットチャレンジ！
 * <p>
 * paizaランクB相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * あなたはクライアントから画像分析の仕事を受けました。
 * <p>
 * N × N ピクセルの白黒画像と M × M ピクセルの白黒画像が与えられます。 白黒画像の各画素は 0 または 1 のいずれかです。 N × N ピクセルの画像を入力、M × M ピクセルの画像をパターンと呼ぶことにします。
 * <p>
 * あなたの仕事は、入力からパターンとの完全一致を探すことです。
 * <p>
 * 入力とパターンがピクセルの位置 (y, x) で完全一致するとは、 全ての i, j (i = 0, 1, ... M - 1, j = 0, 1, ... M - 1) について、 (入力の位置 (y + j, x + i) におけるピクセル) = (パターンの位置 (j, i) におけるピクセル) が成立することをいいます。
 * <p>
 * また、依頼主からは、入力にはパターンと完全一致する箇所は必ず 1 つだけ存在するということを 伝えられています。
 * <p>
 * ここで、N = 4 の入力の例を見てみましょう。
 * <p>
 * 図中の赤い線で囲まれている部分とパターンが完全一致していることがわかります。 完全一致のピクセルの位置、すなわち、(1, 0) を出力してください。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * N
 * q_{0, 0} q_{0, 1} q_{0, 2} ... q_{0, N-1}
 * q_{1, 0} q_{1, 1} q_{1, 2} ... q_{1, N-1}
 * ...
 * q_{N-1, 0} q_{N-1, 1} q_{N-1, 2} ... q_{N-1, N-1}
 * M
 * p_{0, 0} p_{0, 1} p_{0, 2} ... p_{0, M-1}
 * p_{1, 0} p_{1, 1} p_{1, 2} ... p_{1, M-1}
 * ...
 * p_{M-1, 0} p_{M-1, 1} p_{M-1, 2} ... p_{M-1, M-1}
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 10 ≦ N ≦ 100, 2 ≦ M ≦ 10
 * q_{i, j}, p_{i, j} は 0 または 1
 * パターンと完全一致する箇所は必ず1つだけ存在します
 * 期待する出力
 * <p>
 * パターンと完全一致する左上のピクセルの座標を半角スペース区切りで出力してください。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 4
 * 0 0 1 0
 * 0 1 1 0
 * 0 1 0 1
 * 1 1 1 0
 * 3
 * 0 1 1
 * 0 1 0
 * 1 1 1
 * <p>
 * 出力
 * 1 0
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 4
 * 0 0 0 0
 * 0 0 1 1
 * 0 0 1 1
 * 0 0 0 0
 * 2
 * 1 1
 * 1 1
 * <p>
 * 出力
 * 1 2
 */
public class _0011 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    IntFunction<int[][]> image = i -> Stream.generate(() ->
        IntStream.range(0, i).map(j -> sc.nextInt()).toArray()
    ).limit(i).toArray(int[][]::new);

    int[][] base = image.apply(sc.nextInt()), target = image.apply(sc.nextInt());
    int n = base.length, m = target.length;

    IntFunction<Stream<Point>> getMatchHeadPoints = i ->
        IntStream.rangeClosed(0, n - m).filter(j ->
            IntStream.range(0, m).allMatch(k -> base[i][j + k] == target[0][k])
        ).mapToObj(j -> new Point(j, i));

    Predicate<Point> allMatchPixels = point -> IntStream.range(0, m).allMatch(i ->
        IntStream.range(0, m).allMatch(j -> base[i + point.y][j + point.x] == target[i][j]));

    IntStream.rangeClosed(0, n - m)
        .mapToObj(getMatchHeadPoints)
        .flatMap(p -> p)
        .filter(allMatchPixels)
        .map(p -> p.y + " " + p.x)
        .forEach(System.out::println);
  }
}

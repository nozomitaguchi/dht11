package poh.vol07;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * 「カーディガンセット」ゲットチャレンジ！
 * <p>
 * paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * ある正の整数 n が入力されます。
 * <p>
 * 1 から n を全て掛けあわせた数を出力して下さい。
 * <p>
 * 例えば以下のような入力の場合
 * <p>
 * 4
 * <p>
 * <p>
 * 1 × 2 × 3 × 4 を計算し以下のように出力してください。
 * 24
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * n
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ n ≦ 7
 * n は正の整数
 * 期待する出力
 * <p>
 * 1 から n を全て掛けあわせた数を出力して下さい。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 4
 * <p>
 * 出力
 * 24
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 3
 * <p>
 * 出力
 * 6
 */
public class _0010 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int factorialized =
        IntStream.rangeClosed(1, sc.nextInt()).reduce((l, r) -> l * r).orElse(0);
    System.out.println(factorialized);
  }
}

package poh.vol07;

import java.util.Scanner;
import java.util.stream.Stream;

/**
 * 「ロングヘアセット」ゲットチャレンジ！
 * <p>
 * paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * yes か no か5票の投票が行われます。数の多い方を出力して下さい。
 * <p>
 * 例えば以下のような入力の場合
 * <p>
 * yes
 * yes
 * no
 * yes
 * no
 * <p>
 * <p>
 * 以下のように出力してください
 * yes
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * p_1
 * p_2
 * p_3
 * p_4
 * p_5
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * p_1、p_2、p_3、p_4、p_5 は yes か no の文字列
 * 期待する出力
 * <p>
 * yes か no の数の多い方を出力して下さい。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * yes
 * yes
 * no
 * yes
 * no
 * <p>
 * 出力
 * yes
 * <p>
 * 入力例２
 * <p>
 * 入力
 * no
 * yes
 * no
 * no
 * yes
 * <p>
 * 出力
 * no
 */
public class _0005 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long yesCount = Stream.generate(sc::next).limit(5).filter("yes"::equals).count();
    System.out.println(2 < yesCount ? "yes" : "no");
  }
}

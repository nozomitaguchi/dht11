package poh.vol07;

import java.util.Scanner;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.stream.Stream;

/**
 * 「メイド服セット」ゲットチャレンジ！
 * <p>
 * paizaランクC相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * あなたは毎日朝7時に起きて、残業がなければ8時間働いて、6時間眠ります。
 * 残業のあった日は疲れるので、残業した時間の1/3だけ、睡眠時間が増えます。
 * <p>
 * きちんと朝7時に起きるためには、前日の何時何分に寝ればよいかを計算してください。
 * 幸い、あなたの会社は3分刻みで残業時間を記録するので、何秒に寝ればよいかを考える必要はありません。
 * <p>
 * 入力例1の図
 * <p>
 * <p>
 * 睡眠、仕事、残業 以外の時間は空き時間となります。
 * <p>
 * <p>
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * N
 * m_1
 * m_2
 * ...
 * m_N
 * <p>
 * 入力は1行目にあなたはが働く日数 N が入力されます。
 * 続く N 行には、あなたはが残業した分数 m が入力されていきます。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ N ≦ 100
 * 0 ≦ m ≦ 450 (m は3の倍数)
 * 期待する出力
 * <p>
 * あなたが寝る時間をN行、24時間表記で(時間):(分)の形式で、 改行区切りで出力してください。 分が1桁の場合でも、0を付けて2桁にしてください。(例 09:07)
 * <p>
 * 23:59を超える場合は、00:00に戻して表示してください。
 * <p>
 * 例1 25:00 → 01:00
 * 例2 24:15 → 00:15
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 2
 * 30
 * 210
 * <p>
 * 出力
 * 00:50
 * 23:50
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 2
 * 0
 * 177
 * <p>
 * 出力
 * 01:00
 * 00:01
 */
public class _0020 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    // 日をまたぐことを考慮する。
    int baseWakeUpTime = (7 - 6 + 24) * 60, midnight = 24 * 60;
    IntUnaryOperator sleepTime = i -> (baseWakeUpTime - i / 3) % midnight;
    IntFunction<String> to2digits = i -> String.format("%02d", i),
        mold = time -> to2digits.apply(time / 60) + ":" + to2digits.apply(time % 60);

    int n = sc.nextInt();
    Stream.generate(sc::nextInt).limit(n)
        .map(sleepTime::applyAsInt)
        .map(mold::apply)
        .forEach(System.out::println);
  }
}

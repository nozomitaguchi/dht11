package poh.vol07;

import java.util.Scanner;
import java.util.function.LongBinaryOperator;
import java.util.stream.LongStream;

/**
 * 「水着」ゲットチャレンジ！
 * <p>
 * paizaランクA相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * 階乗とは数学の演算の一つで、N の階乗をN! と書きます。N が自然数であるとき、階乗は次のように計算できます。
 * <p>
 * <p>
 * N! = N * (N - 1) * ... * 2 * 1
 * <p>
 * <p>
 * N が与えられたとき、N! のすべての桁の代わりに、N! の最下位桁から続く0 をすべて除いた値の下位9桁を求めるプログラムを作成してください。
 * 9桁ではあるが先頭が0であるような場合は先頭の0を取り除いた値を出力してください。先頭に0のついた値を出力すると誤答となります。
 * 例えば N = 38 の時は以下のようになります。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * N
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ N ≦ 1000000
 * 期待する出力
 * <p>
 * N! の最下位桁から続く0 をすべて除いた値の下位9桁を出力して下さい。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 15
 * <p>
 * 出力
 * 307674368
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 10
 * <p>
 * 出力
 * 36288
 */
public class _0013 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    LongBinaryOperator f = (acc, i) -> {
      acc *= i;
      while (acc % 10 == 0) acc /= 10;
      return acc % 1000000000000L;
    };
    System.out.println(LongStream.rangeClosed(1, sc.nextLong()).reduce(1L, f) % 1000000000);
  }
}

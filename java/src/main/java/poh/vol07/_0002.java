package poh.vol07;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 「つり目セット」ゲットチャレンジ！
 * <p>
 * paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * ある正の整数 n が入力されます。
 * n 回「Ann」と繋げて「AnnAnnAnnAnn......」と出力して下さい。
 * <p>
 * 例えば以下のような入力の場合
 * <p>
 * 3
 * <p>
 * <p>
 * 以下のように出力してください
 * AnnAnnAnn
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * n
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ n ≦ 10
 * n は正の整数
 * 期待する出力
 * <p>
 * n 回「Ann」と繋げて出力して下さい。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 3
 * <p>
 * 出力
 * AnnAnnAnn
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 10
 * <p>
 * 出力
 * AnnAnnAnnAnnAnnAnnAnnAnnAnnAnn
 */
public class _0002 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(Stream.generate(() -> "Ann").limit(sc.nextInt()).collect(Collectors.joining()));
  }
}

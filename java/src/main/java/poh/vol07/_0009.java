package poh.vol07;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 「セーラー服セット」ゲットチャレンジ！
 * <p>
 * paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * プログラマーのあなたは複数の単語を繋ぎ合わせた変数名を自動で作るプログラムを作成しようと考えています。
 * <p>
 * 1 行目に単語の総数 n 、2行目以降改行区切りで n 個の単語が入力されるので「_」で繋ぎ合わせたものを出力してください。
 * <p>
 * 例えば以下のような入力の場合
 * <p>
 * 3
 * paiza
 * online
 * hackathon
 * <p>
 * <p>
 * 以下のように出力してください。
 * paiza_online_hackathon
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * n
 * s_1
 * s_2
 * ...
 * s_n
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 2 ≦ n ≦ 10
 * n は整数
 * 1 ≦ 文字列 s の長さ ≦ 10
 * s は半角アルファベットで構成された文字列
 * 期待する出力
 * <p>
 * n 個の単語を「_」(半角アンダーバー)で繋ぎ合わせた文字列を出力してください。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 3
 * paiza
 * online
 * hackathon
 * <p>
 * 出力
 * paiza_online_hackathon
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 2
 * is
 * Android
 * <p>
 * 出力
 * is_Android
 */
public class _0009 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    System.out.println(Stream.generate(sc::next).limit(n).collect(Collectors.joining("_")));
  }
}

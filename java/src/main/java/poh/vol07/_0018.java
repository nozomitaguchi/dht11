package poh.vol07;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 「猫耳セット」ゲットチャレンジ！
 * <p>
 * paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * ある半角小文字アルファベットで構成される文字列 S が与えられます。
 * S に含まれる文字列 "cat" の数を出力してください。
 * <p>
 * 例えば
 * <p>
 * acatbccatd
 * <p>
 * <p>
 * と入力された場合
 * <p>
 * 2
 * <p>
 * と出力して下さい
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * S
 * <p>
 * 入力値最終行の末尾に改行が１つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ 文字列 S の長さ ≦ 100
 * <p>
 * S は半角小文字アルファベットで構成された文字列
 * 期待する出力
 * <p>
 * S に含まれる文字列 "cat" の数を出力してください。
 * <p>
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * catcatcat
 * <p>
 * 出力
 * 3
 * <p>
 * 入力例２
 * <p>
 * 入力
 * acatbccatd
 * <p>
 * 出力
 * 2
 */
public class _0018 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String[] replacedCats = sc.next().replaceAll("cat", "_").split("");
    System.out.println(Arrays.stream(replacedCats).filter("_"::equals).count());
  }
}

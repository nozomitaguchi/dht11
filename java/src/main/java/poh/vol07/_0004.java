package poh.vol07;

import java.util.Scanner;

/**
 * 「ショートヘアセット」ゲットチャレンジ！
 * <p>
 * paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * 正の整数が a, b が改行区切りで入力されます。
 * <p>
 * a + b を計算し出力して下さい。
 * <p>
 * 例えば以下のような入力の場合
 * <p>
 * 9
 * 23
 * <p>
 * <p>
 * 以下のように出力してください
 * 32
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * a
 * b
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ a ≦ 101
 * 1 ≦ b ≦ 101
 * a, b は正の整数
 * 期待する出力
 * <p>
 * a と b を足し算した数を出力してください。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 9
 * 23
 * <p>
 * 出力
 * 32
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 99
 * 100
 * <p>
 * 出力
 * 199
 */
public class _0004 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(sc.nextInt() + sc.nextInt());
  }
}

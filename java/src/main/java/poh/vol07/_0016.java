package poh.vol07;

import java.util.List;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 「眼帯」ゲットチャレンジ！
 * <p>
 * paizaランクC相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * あなたは、巻数が全 N 巻の古い本を集めています。
 * <p>
 * 古本屋に訪れたあなたは売られている各巻のうち買うべきなのは何巻かを知りたいです。
 * <p>
 * あなたの持っている巻のリストと、中古本屋で売られている巻のリストを入力として与えられたとき、
 * あなたの買うべき巻のリストを出力してください。
 * <p>
 * <p>
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * <p>
 * N
 * M1
 * x_1 x_2 ... x_M1
 * M2
 * y_1 y_2 ... y_M2
 * <p>
 * <p>
 * N は、ある本の出版されている巻の総数です。
 * 出版されているのは、1 巻から始まり、N 巻までです。
 * <p>
 * M1 は、あなたの持っている巻の総数を表します。
 * 次の行には、あなたの持っている巻のリストが与えられます。
 * <p>
 * M2 は、中古本屋で売られている巻のリストの総数を表します。
 * 次の行には、中古本屋で売られている巻のリストが与えられます。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * <p>
 * 1 ≦ N ≦ 1000
 * 1 ≦ M1, M2 ≦ N
 * 1 ≦ x_i, y_i ≦ N
 * 期待する出力
 * <p>
 * あなたの買うべき巻を小さい順に空白区切りで出力してください。
 * ただし、買うべき巻がない場合は None と出力してください。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 5
 * 3
 * 1 3 4
 * 3
 * 2 3 5
 * <p>
 * 出力
 * 2 5
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 8
 * 5
 * 1 3 4 5 6
 * 3
 * 1 5 6
 * <p>
 * 出力
 * None
 */
public class _0016 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    sc.nextInt(); // n は使わない。
    Supplier<Stream<Integer>> books = () -> IntStream.range(0, sc.nextInt()).map(i -> sc.nextInt()).boxed().sorted();
    List<Integer> collections = books.get().collect(Collectors.toList()),
        wishList = books.get().filter(i -> !collections.contains(i)).collect(Collectors.toList());
    System.out.println(wishList.isEmpty() ? "None" : wishList.stream().map(String::valueOf).collect(Collectors.joining(" ")));
  }
}

package poh.vol07;

import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * 「猫セット」ゲットチャレンジ！
 * <p>
 * paizaランクC相当問題
 * paizaランクとは？
 * <p>
 * Poh7 question box top
 * ある半角小文字アルファベット "c", "a", "t" で構成される文字列 S が与えられます。
 * 文字列 Sに 含まれる "c", "a", "t" を1つずつ集め "cat" という文字列をあなたは作ろうとしています。
 * <p>
 * "cat" と完全に作れる個数、余った "c", "a", "t" から "cat" を作るのに必要なそれぞれの個数を求め
 * 完全に作れる個数、必要な "c" の個数、必要な "a" の個数、必要な "t" の個数の順に改行区切りで出力して下さい。
 * <p>
 * 例えば
 * <p>
 * ctacct
 * <p>
 * と入力された場合
 * <p>
 * c が 3文字 a が 1文字 t が 2文字
 * となり "cat" は1つ作ることができ、 "c" が2つ "t" が1つ余ります。
 * "a" があと 2つ、 "t" があと1つで2つの "cat" が作ることができるので
 * 1
 * 0
 * 2
 * 1
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * S
 * <p>
 * 入力値最終行の末尾に改行が１つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ 文字列 S の長さ ≦ 100
 * <p>
 * S は半角小文字アルファベット "c", "a", "t" で構成された文字列
 * 期待する出力
 * <p>
 * 文字列 S から "cat" と完全に作れる個数、余った "c", "a", "t" から "cat" を作るのに必要なそれぞれの個数を求め、完全に作れる個数、必要な "c" の個数、必要な "a" の個数、必要な "t" の個数の順に改行区切りで出力して下さい。
 * <p>
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * ctacct
 * <p>
 * 出力
 * 1
 * 0
 * 2
 * 1
 * <p>
 * 入力例２
 * <p>
 * 入力
 * cccc
 * <p>
 * 出力
 * 0
 * 0
 * 4
 * 4
 */
public class _0019 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Map<String, Long> map = Arrays.stream(sc.next().split(""))
        .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
    Supplier<LongStream> counts = () ->
        Stream.of("c", "a", "t").mapToLong(s -> map.getOrDefault(s, 0L));
    long min = counts.get().min().orElse(0L), max = counts.get().max().orElse(0L);
    System.out.println(min);
    counts.get().map(l -> max - l).forEach(System.out::println);
  }
}

package poh.vol08;

import java.util.Scanner;

/**
 * 「マイク」ゲットチャレンジ！
 * <p>
 * Rank 01 paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh8 question box top
 * あなたはアイドルが出演するライブイベントを企画することになりました。
 * <p>
 * イベントは必ず土日に行います。1日にこなせるイベント回数 n と計画されているイベント総回数 m が入力されるので、何週で全てのイベントが完了するか計算するプログラムを作成してください。
 * <p>
 * <p>
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * n
 * m
 * <p>
 * 1 行目に1日にこなせるイベント回数 n が与えられます。
 * 2 行目に計画されているイベント総回数 m が与えられます。
 * 入力は合計で 2 行となり、入力値最終行の末尾に改行が１つ入ります。
 * <p>
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * n, m は整数
 * 1 ≦ n ≦ 20
 * 1 ≦ m ≦ 20
 * <p>
 * 期待する出力
 * <p>
 * 何週で全てのイベントが完了するか出力してください。
 * <p>
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 2
 * 9
 * <p>
 * 出力
 * 3
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 1
 * 15
 * <p>
 * 出力
 * 8
 */
public class _special5 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    double n = sc.nextDouble(), m = sc.nextDouble();
    System.out.println((int) Math.ceil(m / (n * 2)));
  }
}

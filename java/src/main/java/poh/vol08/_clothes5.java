package poh.vol08;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 「制服」ゲットチャレンジ！
 * <p>
 * Rank 02 paizaランクB相当問題
 * paizaランクとは？
 * <p>
 * Poh8 question box top
 * 以下のルールのトランプゲームを考えましょう。
 * <p>
 * ・A, 2, 3, 4, 5, 6, 7, 8, 9, 10, J, Q, K の 13 種類のカードをそれぞれ 4 枚ずつ、計 52 枚用いるゲームです。このゲームではスートやジョーカーは考えません。
 * ・このゲームでは以下の順序でカード間に「強さ」が決まっています。
 * <p>
 * <p>
 * <p>
 * プレイヤー 1, 2, …, n の n 人でプレイします。
 * ・まず、52 枚のカードを各プレイヤーに、なるべく同じ枚数ずつ配ります。
 * ・プレイヤー 1 → 2 → … → n → 1 → … という順で、以下のルールで場にカードを出していきます。
 * 　・場にカードが存在しない場合は、好きなカードを 1 枚出します。
 * 　・場にカードが存在する場合は、場の 1 番上に出ているカードよりも強いカードをその上に 1 枚出します。
 * 　　出せるカードがない場合はパスとなります。
 * 　・手札が早くなくなったプレイヤーから順に 1 位、2 位、...、n 位と順位がつきます。
 * 　　順位がついたプレイヤーはその時点でゲームから抜けます。
 * 　・パスが 1 周続き、最後に場にカードを出したプレイヤーの手番まで再度戻ってきた場合は、場に出ているカードを全て除去し、そのプレイヤーが場に新たに好きなカードを出します。最後に場にカードを出したプレイヤーがゲームから抜けている場合は、その次のプレイヤーの手番まで再度戻ってきたときに、場に出ているカードを全て除去して場に新たに好きなカードを出します。
 * <p>
 * あなたはこのゲームの AI を開発しているエンジニアであり、なるべく強い AI を作ろうと思っています。しかしながら、最強の AI を作るのはとても難しいので、まずはプレイ人数が 52 人の場合に最善のプレイを行う AI を作りたいと考えました。
 * 52 人で戦う場合、各プレイヤーに最初に配られるカードはちょうど 1 枚ずつとなり、「出せるときは必ず出す」というプレイが最善となります。
 * <p>
 * 入力例1の図
 * <p>
 * <p>
 * <p>
 * 各プレイヤーに最初に配られるカードの一覧が与えられるので、各プレイヤーが最善のプレイを行った場合の順位をそれぞれ出力するプログラムを作成して下さい。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * c_1 c_2 ... c_52
 * <p>
 * <p>
 * 1 行目に、プレイヤー i (1 ≦ i ≦ 52) に配られるカードの種類 c_i が半角スペース区切りで与えられます。
 * 入力は 1 行となり、入力値最終行の末尾に改行が 1 つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * ・各 c_i (1 ≦ i ≦ 52) は "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" のいずれか
 * ・同じ種類のカードはちょうど 4 枚ずつ含まれる
 * <p>
 * 期待する出力
 * <p>
 * 期待する出力は 52 行からなります。
 * i 行目 (1 ≦ i ≦ 52) には、プレイヤー i の順位を出力して下さい。
 * <p>
 * 出力の最後に改行を一つ入れ、余計な文字、空行を含んではいけません。
 * <p>
 * <p>
 * 入力例１
 * <p>
 * 入力
 * K 6 J 5 3 2 A 4 5 10 K 4 J 3 10 6 9 9 3 7 Q K Q 4 10 2 5 J 4 Q 8 9 A 8 9 3 K 7 8 5 7 8 6 10 Q 6 A 2 J 7 A 2
 * <p>
 * 出力
 * 1
 * 13
 * 14
 * 47
 * 48
 * 2
 * 3
 * 49
 * 50
 * 18
 * 15
 * 51
 * 19
 * 52
 * 29
 * 30
 * 31
 * 42
 * 43
 * 44
 * 20
 * 21
 * 22
 * 45
 * 32
 * 4
 * 5
 * 6
 * 33
 * 7
 * 34
 * 35
 * 8
 * 36
 * 37
 * 38
 * 23
 * 24
 * 25
 * 39
 * 40
 * 41
 * 46
 * 26
 * 27
 * 28
 * 16
 * 9
 * 10
 * 17
 * 11
 * 12
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 6 9 5 4 5 7 7 8 2 2 4 J 5 4 K 2 K 3 10 10 Q A 9 A J 9 4 Q K A 3 2 K 10 8 8 7 3 A 6 Q 7 Q 3 J 8 5 9 6 J 6 10
 * <p>
 * 出力
 * 1
 * 2
 * 31
 * 47
 * 48
 * 32
 * 41
 * 33
 * 3
 * 4
 * 5
 * 6
 * 49
 * 50
 * 7
 * 8
 * 9
 * 51
 * 34
 * 35
 * 26
 * 10
 * 27
 * 16
 * 17
 * 36
 * 37
 * 18
 * 19
 * 20
 * 21
 * 11
 * 12
 * 22
 * 38
 * 42
 * 43
 * 52
 * 13
 * 14
 * 15
 * 46
 * 23
 * 24
 * 25
 * 44
 * 45
 * 39
 * 40
 * 28
 * 29
 * 30
 */
public class _clothes5 {
  private static class Player {
    private int winOrder = -1;
    private Trump trump;

    Player(Trump trump) {
      this.trump = trump;
    }

    Trump getTrump() {
      return trump;
    }

    void setWinOrder(int winOrder) {
      this.winOrder = winOrder;
    }

    boolean isWinner() {
      return winOrder != -1;
    }

    boolean hasLoseHand(Trump upCard) {
      return trump.ordinal() <= upCard.ordinal();
    }
  }

  @SuppressWarnings("unused")
  private enum Trump {
    none("0"), trey("3"), cater("4"), cinque("5"), sice("6"), seven("7"), eight("8"), nine("9"), ten("10"), jack("J"), queen("Q"), king("K"), ace("A"), deuce("2");
    private final String c;

    Trump(final String c) {
      this.c = c;
    }

    public static Trump get(String c) {
      return Arrays.stream(Trump.values()).filter(t -> t.c.equals(c)).findFirst().orElse(none);
    }
  }

  private static final int trumpCount = 52;
  private static Player[] players;

  public static void main(String[] args) {
    // TODO: リファクタリング
    Scanner sc = new Scanner(System.in);
    players = Stream.generate(sc::next).limit(trumpCount).map(Trump::get).map(Player::new).toArray(Player[]::new);
    playTheGame();
    Arrays.stream(players).map(player -> player.winOrder).forEach(System.out::println);
  }

  private static void playTheGame() {
    int winnersCount = 0, winner = -1;
    while (winnersCount < trumpCount) {
      Trump upCard = Trump.none;
      // 最後の勝者の次の人からリスタート(初回は0から)
      for (int i : getNextPlayerIndices(winner)) {
        Player player = players[i];
        if (player.hasLoseHand(upCard)) continue;
        upCard = player.getTrump();
        player.setWinOrder(++winnersCount);
        winner = i;
      }
    }
  }

  private static int[] getNextPlayerIndices(int i) {
    return IntStream.range(0, trumpCount).map(j -> (j + i + 1) % trumpCount).filter(j -> !players[j].isWinner()).toArray();
  }
}

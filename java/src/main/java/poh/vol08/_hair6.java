package poh.vol08;

import java.util.Scanner;
import java.util.stream.Stream;

/**
 * 「おさげ」ゲットチャレンジ！
 * <p>
 * Rank 01 paizaランクC相当問題
 * paizaランクとは？
 * <p>
 * Poh8 question box top
 * あなたはアイドルのCDアルバムを作成しています。
 * <p>
 * CDに入る曲の最大の長さ n 分、収録したい曲数 m と、収録したい優先度順に各曲の秒数 t が入力されます。
 * <p>
 * CDに収録したい曲が収まるかを判定し、収まるならば "OK" と出力し、収まらない場合は何曲目まで収まるかを出力して下さい。
 * <p>
 * 入力例1を図で表すと以下のようになります。
 * <p>
 * <p>
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * n
 * m
 * t_1
 * t_2
 * ...
 * t_m
 * <p>
 * <p>
 * <p>
 * ・全ての入力は整数で与えられます。
 * ・1 行目にCDに入る曲の最大の長さ n (分)が与えられます。
 * ・2 行目に収録したい曲数 m が与えられます。
 * ・3 行目から m 行目に収録したい優先度順に各曲の秒数 t
 * ・入力値最終行の末尾に改行が１つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 10 ≦ n ≦ 70
 * 1 ≦ m ≦ 30
 * 1 ≦ t_i ≦ 600
 * <p>
 * 期待する出力
 * <p>
 * CDに収録したい曲が収まるかを判定し、収まるならば "OK" と出力し、収まらない場合は何曲目まで収まるかを出力して下さい。
 * <p>
 * 出力の最後に改行を一つ入れ、余計な文字、空行を含んではいけません。
 * <p>
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 50
 * 9
 * 543
 * 318
 * 265
 * 422
 * 468
 * 573
 * 141
 * 443
 * 421
 * <p>
 * 出力
 * 7
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 60
 * 8
 * 449
 * 163
 * 517
 * 312
 * 170
 * 355
 * 161
 * 238
 * <p>
 * 出力
 * OK
 */
public class _hair6 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt(), m = sc.nextInt();
    long inRangeSongsCount =
        Stream.iterate(sc.nextInt(), i -> i + sc.nextInt()).limit(m).filter(i -> i <= n * 60).count();
    System.out.println(inRangeSongsCount == m ? "OK" : inRangeSongsCount);
  }
}

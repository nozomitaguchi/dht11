package poh.vol08;

import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 「ゆかた」ゲットチャレンジ！
 * <p>
 * Rank 02 paizaランクB相当問題
 * paizaランクとは？
 * <p>
 * Poh8 question box top
 * アイドルの下積み時代に苦労はつきもの…。節約のために、まずは光熱費を見直しましょう。 冷蔵庫の電気代は意外と見落としがちな家計の負担となっています。実際いくら使っているのか気になったあなたは試しに冷蔵庫の使用状況を観察しながら電気代を概算してみることにしました。あなたは冷蔵庫の電気代は以下のように発生すると考えました。
 * <p>
 * ・冷蔵庫内の温度が設定温度より高いとき冷蔵庫は 1 時間で 1 度温度を下げる「冷却」を行い、これには 1 時間あたり 2 円かかる。
 * ・冷蔵庫内の温度が設定温度に等しいとき冷蔵庫は温度を等しく保つ「保温」を行い、これには 1 時間あたり 1 円かかる。
 * <p>
 * またものの出し入れの際に次のように冷蔵庫内の温度が変化すると考えました。
 * <p>
 * ・ ものを取り出すと冷蔵庫内の温度は外気に触れることでその瞬間に 3 度上がる
 * ・ ものを新たに入れると冷蔵庫内の温度は外気と温かいものに触れることでその瞬間に 5 度上がる
 * <p>
 * 1 日の間（24 時間制で 0 時から次の日の 0 時になるまで）のものの出し入れの様子から予想されるこの日の電気代を求めてください。なおはじめの状態では冷蔵庫内の温度は設定温度に一致しているものとします。
 * <p>
 * 例)
 * <p>
 * 5時にものを取り出す
 * 12時にものを入れる
 * 14時にものを取り出す
 * 21時にものを入れる
 * <p>
 * この日の冷蔵庫内の温度と設定温度の差の推移は以下のようになります。
 * <p>
 * <p>
 * <p>
 * 電気代の概算 → 2 × 14 + 1 × 10 = 38 円
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * <p>
 * N
 * t_1 s_1
 * t_2 s_2
 * ...
 * t_n s_N
 * <p>
 * <p>
 * ・1 行目に冷蔵庫からの出し入れの回数を表す N が与えられます。
 * ・続く N 行のうち i 行目 (1 ≦ i ≦ N) に 24 時間制の時刻で時を表す t_i、ものの出し入れの操作を示す文字列 s_i がこの順に半角スペース区切りで与えられます。
 * 　・s_i について、ものを入れる操作は "in" 、出す操作は "out" で表されます。
 * ・入力は合計で N + 1 行となり、入力値最終行の末尾に改行が１つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 添字の範囲は 1 ≦ i ≦ N とします。
 * ・N は整数
 * ・1 ≦ N ≦ 24
 * ・t_i は整数
 * ・0 ≦ t_i ≦ 23
 * ・i < j のとき t_i < t_j (1 ≦ i < j ≦ N)
 * ・s_i は英字小文字で構成される文字列で "in", "out" のいずれか
 * <p>
 * 期待する出力
 * <p>
 * 予想される電気代の概算を円単位の整数で出力してください。
 * <p>
 * 出力の最後に改行を一つ入れ、余計な文字、空行を含んではいけません。
 * <p>
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 4
 * 5 out
 * 12 in
 * 14 out
 * 21 in
 * <p>
 * 出力
 * 38
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 10
 * 2 out
 * 3 in
 * 7 out
 * 8 in
 * 13 out
 * 17 in
 * 18 out
 * 19 in
 * 20 in
 * 21 in
 * <p>
 * 出力
 * 46
 */
public class _clothes6 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Map<Integer, String> inOuts = IntStream.range(0, sc.nextInt()).boxed()
        .collect(Collectors.toMap(i -> sc.nextInt(), i -> sc.next()));

    ToIntFunction<String> upDegreeByInOut = s -> "out".equals(s) ? 3 : "in".equals(s) ? 5 : 0;
    AtomicInteger degree = new AtomicInteger(4);
    int[] degreesByHour = IntStream.range(0, 24)
        .map(hour -> upDegreeByInOut.applyAsInt(inOuts.get(hour)))
        .map(upDegree -> degree.addAndGet(upDegree - (4 < degree.get() ? 1 : 0)))
        .toArray();

    System.out.println(Arrays.stream(degreesByHour).map(i -> i <= 4 ? 1 : 2).sum());
  }
}

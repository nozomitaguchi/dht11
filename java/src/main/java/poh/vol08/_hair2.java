package poh.vol08;

import java.util.Scanner;
import java.util.stream.Stream;

/**
 * 「ショートヘア」ゲットチャレンジ！
 * <p>
 * Rank 01 paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh8 question box top
 * アイドルたるもの日々の発声練習は重要です。あなたは、発声練習で使うワードを必要な回数だけ表示するシステムを作ることになりました。
 * <p>
 * 整数 N と文字列 S が入力されるので、改行区切りで S を N 回出力してください。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * N
 * S
 * <p>
 * 1 行目に繰り返しの回数を表す整数 N が与えられます。
 * 2 行目に出力する文字列 S が与えられます。
 * 入力は合計で 2 行となり、入力値最終行の末尾に改行が１つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ N ≦ 100
 * S は英字小文字で構成される
 * 1 ≦ (S の長さ) ≦ 20
 * n, m は整数
 * <p>
 * 期待する出力
 * <p>
 * 改行区切りで S を N 回出力してください。
 * <p>
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 3
 * idol
 * <p>
 * 出力
 * idol
 * idol
 * idol
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 10
 * dream
 * <p>
 * 出力
 * dream
 * dream
 * dream
 * dream
 * dream
 * dream
 * dream
 * dream
 * dream
 * dream
 */
public class _hair2 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    String s = sc.next();
    Stream.generate(() -> s).limit(n).forEach(System.out::println);
  }
}

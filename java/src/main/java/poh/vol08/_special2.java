package poh.vol08;

import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * 「水着」ゲットチャレンジ！
 * <p>
 * Rank 01 paizaランクC相当問題
 * paizaランクとは？
 * <p>
 * Poh8 question box top
 * 所属事務所の看板を作ることになったあなたは、看板に文字列を表示するため、各文字の印刷された小看板を別々に発注して組み合わせることにしました。しかし、発注後に表示する文字列に変更があり、小看板を再発注することになりました。
 * <p>
 * あなたは再発注する小看板の数を減らすため、共通する文字の小看板を再利用することを考えました。
 * <p>
 * 例えば 文字列が "paiza" から "pizza" へと変更された場合、共通する a, i, p, z の 4 文字の小看板を再利用することで再発注数を 1 つに減らすことができます。
 * <p>
 * <p>
 * <p>
 * 入力として変更前後の文字列の長さおよび文字列自身が与えられます。
 * この再利用方法で再発注しなければならない最小の小看板の数を出力してください。
 * <p>
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * n m
 * s
 * t
 * <p>
 * 1行目に変更前の文字列の長さ n と 変更後の文字列の長さ m が半角スペース区切りで与えられます。
 * 2行目に変更前の文字列 s が与えられます。
 * 3行目に変更後の文字列 t が与えられます。
 * 入力は合計で 3行 となり、 最終行の末尾に改行が1つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * n, m は整数
 * 1 ≦ n, m ≦ 100000
 * (sの長さ) = n, (tの長さ) = m
 * s, t は半角英字小文字のみで構成される文字列
 * <p>
 * 期待する出力
 * <p>
 * 再発注しなければならない最小の小看板の数を整数で出力してください。
 * <p>
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 5 5
 * pizza
 * paiza
 * <p>
 * 出力
 * 1
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 3 5
 * ant
 * maven
 * <p>
 * 出力
 * 3
 */
public class _special2 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    @SuppressWarnings("unused") int n = sc.nextInt(), m = sc.nextInt();
    Supplier<Map<String, Long>> groupByCounts = () -> Arrays.stream(sc.next().split(""))
        .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
    Map<String, Long> s = groupByCounts.get(), t = groupByCounts.get();
    long sum = t.entrySet().stream()
        .mapToLong(e -> e.getValue() - s.getOrDefault(e.getKey(), 0L))
        .filter(l -> 0 < l)
        .sum();
    System.out.println(sum);
  }
}

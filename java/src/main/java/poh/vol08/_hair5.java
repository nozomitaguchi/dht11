package poh.vol08;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 「ツインテール」ゲットチャレンジ！
 * <p>
 * Rank 01 paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh8 question box top
 * プロデューサーの業務は多岐に渡ります。
 * <p>
 * そこで、あなたは文字を使って業務の進捗状況を見やすく表現することにしました。
 * <p>
 * 進捗状況は全体の長さ s と現在の進捗状況の値 t が改行区切りで与えれます。
 * <p>
 * 表示は全体を「-」を使って表示し、現在の進捗位置を「+」で表します。
 * <p>
 * 例えば以下のような入力の場合
 * <p>
 * 10
 * 3
 * <p>
 * <p>
 * 全体の長さが 10 で進捗位置が 3 なので
 * <p>
 * 「-」10文字で構成された文字列の3文字目を「+」に変え
 * <p>
 * --+-------
 * <p>
 * と出力して下さい
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * s
 * t
 * <p>
 * 入力値最終行の末尾に改行が１つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ s ≦ 100
 * 1 ≦ t ≦ s
 * <p>
 * 期待する出力
 * <p>
 * 表示は全体を「-」を使って表示し、現在の進捗位置を「+」で1行の文字列で出力して下さい。
 * <p>
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 10
 * 3
 * <p>
 * 出力
 * --+-------
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 20
 * 20
 * <p>
 * 出力
 * -------------------+
 */
public class _hair5 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int s = sc.nextInt(), t = sc.nextInt();
    String hyphenLine = IntStream.rangeClosed(1, s)
        .mapToObj(i -> i == t ? "+" : "-")
        .collect(Collectors.joining());
    System.out.println(hyphenLine);
  }
}

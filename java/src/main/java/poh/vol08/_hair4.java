package poh.vol08;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * 「ポニーテール」ゲットチャレンジ！
 * <p>
 * Rank 01 paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh8 question box top
 * アイドルは身体が資本ですから、健康管理には気をつけないといけませんよね。
 * <p>
 * 健康診断のために視力検査のプログラムを作成しましょう。
 * <p>
 * 視力検査では円の1部が欠けた「C」のような図形を表示し、欠けた部分の上下左右を答えるものです。
 * <p>
 * 検査では5回中3回以上の正解ならば合格となります。
 * <p>
 * このプログラムでは5行の入力があり、各行は表示された図形の向き、回答の向きが半角スペース区切りで上下左右を U, D, L, Rとして入力されます。
 * <p>
 * 合格の場合は OK 不合格の場合は NG と出力するプログラムを作成して下さい。
 * <p>
 * <p>
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * d_1 e_1
 * d_2 e_2
 * d_3 e_3
 * d_4 e_4
 * d_5 e_5
 * <p>
 * 入力値最終行の末尾に改行が１つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ i ≦ 5
 * d_i, e_i はそれぞれ半角英字の1文字で U, D, L, R のいずれか
 * <p>
 * 期待する出力
 * <p>
 * 合格の場合は OK 不合格の場合は NG と出力してください。
 * <p>
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * U U
 * D D
 * L L
 * R R
 * L L
 * <p>
 * 出力
 * OK
 * <p>
 * 入力例２
 * <p>
 * 入力
 * U D
 * D L
 * R L
 * D U
 * U U
 * <p>
 * 出力
 * NG
 */
public class _hair4 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long correctCount = IntStream.range(0, 5).filter(i -> sc.next().equals(sc.next())).count();
    System.out.println(3 <= correctCount ? "OK" : "NG");
  }
}

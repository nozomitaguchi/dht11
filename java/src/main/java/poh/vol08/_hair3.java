package poh.vol08;

import java.util.Scanner;

/**
 * 「ロングヘア」ゲットチャレンジ！
 * <p>
 * Rank 01 paizaランクD相当問題
 * paizaランクとは？
 * <p>
 * Poh8 question box top
 * トップアイドルになるには運も必要です。
 * <p>
 * 正の整数 N が入力されるので、N が 7 の倍数であれば "lucky" と、そうでないときは "unlucky" と出力してください。
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * N
 * <p>
 * 入力値最終行の末尾に改行が１つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 1 ≦ N ≦ 100
 * <p>
 * 期待する出力
 * <p>
 * N が 7 の倍数であれば "lucky" と、そうでないときは "unlucky" と出力してください。
 * <p>
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 7
 * <p>
 * 出力
 * lucky
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 12
 * <p>
 * 出力
 * unlucky
 */
public class _hair3 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(sc.nextInt() % 7 == 0 ? "lucky" : "unlucky");
  }
}

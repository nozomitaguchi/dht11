package poh.vol08;

import java.util.Scanner;
import java.util.stream.Stream;

/**
 * 「めがね」ゲットチャレンジ！
 * <p>
 * Rank 01 paizaランクC相当問題
 * paizaランクとは？
 * <p>
 * Poh8 question box top
 * アイドルになったからには、アイドル界のセンターを目指したいものです。
 * N (奇数) 個の異なる数字を大きい順に並び替えた時に真ん中に来る数字を求めてください。 真ん中とは (N + 1)/2 番目のことを指します。
 * <p>
 * <p>
 * <p>
 * 例)
 * <p>
 * 元の数字の並び -> 並び替えた数字の並び -> 真ん中の数字
 * 2, 3, 5, 4, 1 -> 5, 4, 3, 2, 1 -> 3
 * 27, 100, 83, 2, 57, 71, 40 -> 100, 83, 71, 57, 41, 27, 3 -> 57
 * <p>
 * <p>
 * 入力される値
 * <p>
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * <p>
 * N
 * a_1 a_2 ... a_N
 * <p>
 * 1 行目に与えられる数字の個数を表す N が与えられます。
 * 2 行めに数字の並び a_1, a_2, ..., a_N がこの順に半角スペース区切りで与えられます。
 * 入力は合計で 2 行となり、入力最終行の最後に改行が一つ入ります。
 * 条件
 * <p>
 * すべてのテストケースにおいて、以下の条件をみたします。
 * <p>
 * 添字の範囲は 1 ≦ i ≦ N とする。
 * <p>
 * N は奇数
 * a_i は整数
 * 3 ≦ N ≦ 99
 * 1 ≦ a_i ≦ 100
 * a_1, a_2, ..., a_N はすべて異なる
 * 期待する出力
 * <p>
 * 与えられる N 個の数字の並びについて、大きい方から並び替えて真ん中 ((N + 1)/2 番目) に来る数字を一行で出力してください。
 * <p>
 * 出力の最後に改行を一つ入れ、余計な文字、空行を含んではいけません。
 * <p>
 * <p>
 * 入力例１
 * <p>
 * 入力
 * 5
 * 2 3 5 4 1
 * <p>
 * 出力
 * 3
 * <p>
 * 入力例２
 * <p>
 * 入力
 * 7
 * 27 100 83 2 57 71 40
 * <p>
 * 出力
 * 57
 */
public class _eye4 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int[] sortedNums = Stream.generate(sc::nextInt).limit(n).mapToInt(Integer::valueOf).sorted().toArray();
    System.out.println(sortedNums[n / 2]);
  }
}

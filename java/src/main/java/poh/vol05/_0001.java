package poh.vol05;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * レナたんが俺の許嫁！？手紙の暗号を解読せよ！
 * <p>
 * いきなりあらわれた憧れのアイドル「新田レナ」が俺の許嫁！？
 * 海外にいる両親からの手紙を預かっているって言われたけど、手紙の文面は暗号化されてて解読しないと読めないよ！！俺の将来はいったいどうなっちゃうんだ！？
 * 届いた手紙を開いてみたところ、手紙は暗号文で何と書かれているのかよくわかりません。手紙には冒頭にヒントとしてアルファベットで「ODD」と書いてありました。その下に続いて暗号文が以下のような文字列で書かれていました。
 * RieanbaC Biwsu
 * 文字列をよく見ていると「ODD」というヒントの意味に貴方は気付きました。奇数を意味する英単語です。 文字列の奇数番目のみを拾って読んでいくと
 * Rena is
 * という文字列が現れました。まだまだ手紙には続きがあるようですが、以降もこの法則で書かれているようです。 標準入力から文字S列が与えられるので暗号文を解読し、その結果を標準出力で出力してください。
 * ミッション１
 * 手紙の暗号を解読してください。
 * 解答次第でこの後のストーリーが変わります！！
 * 入力される値
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * S
 * S は長さ N の半角アルファベットと半角スペースのみで構成された文字列です。
 * 条件
 * すべてのテストケースにおいて、以下の条件をみたします。
 * 1 ≦ N ≦ 100
 * <p>
 * <p>
 * 期待する出力
 * 暗号文を解読した文字列を出力してください。
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * 入力例1
 * PXaeiTzVap
 * 出力例1
 * Paiza
 * 入力例2
 * abcdefg hijklmn
 * 出力例2
 * aceghjln
 */
public class _0001 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String[] s = sc.nextLine().split("");
    String decrypted = IntStream.range(0, s.length)
        .filter(i -> i % 2 == 0)
        .mapToObj(i -> s[i])
        .collect(Collectors.joining());
    System.out.println(decrypted);
  }
}

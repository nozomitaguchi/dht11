package poh.vol05;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * レナたんのパパが経営する会社の入社試験に挑戦だ！
 * <p>
 * レナたんのパパが経営する会社の入社試験を受ける事になってしまった達也。上手くいけば新しい仕事に就けるし、レナたんとの関係も進行しちゃう！？
 * 貴方はある売上を集計するソフトウェアの開発をしています。月曜日から始まる n 日分の売上データ s 円が改行区切りで入力されます。nは必ず7の倍数です。
 * ミッション2
 * 月曜日から日曜日までの各曜日の合計を改行区切りで出力して下さい。
 * 入力される値
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * n
 * s_1
 * s_2
 * s_3
 * ...
 * s_n
 * 条件
 * すべてのテストケースにおいて、以下の条件をみたします。
 * 1 ≦ n ≦ 210
 * 0 ≦ s_n ≦ 1000000
 * ※日数nは必ず7の倍数です。
 * 期待する出力
 * 月曜日から日曜日までの各曜日の合計を改行区切りで出力して下さい。
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * 入力例1
 * 14
 * 30000
 * 20000
 * 0
 * 50000
 * 0
 * 100000
 * 500000
 * 30000
 * 20000
 * 0
 * 20000
 * 15000
 * 600000
 * 450000
 * 出力例1
 * 60000
 * 40000
 * 0
 * 70000
 * 15000
 * 700000
 * 950000
 * 入力例2
 * 7
 * 1003
 * 2302
 * 421
 * 32124
 * 3
 * 0
 * 3214
 * <p>
 * <p>
 * <p>
 * <p>
 * <p>
 * <p>
 * <p>
 * 出力例2
 * 1003
 * 2302
 * 421
 * 32124
 * 3
 * 0
 * 3214
 */
public class _0002 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Map<Integer, Integer> sumsByWeekDay = IntStream.rangeClosed(1, sc.nextInt()).boxed()
        .collect(Collectors.groupingBy(i -> i % 7 == 0 ? 7 : i % 7, TreeMap::new,
            Collectors.summingInt(i -> sc.nextInt())));
    sumsByWeekDay.values().forEach(System.out::println);
  }
}

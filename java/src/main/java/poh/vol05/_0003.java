package poh.vol05;

import java.util.Random;

/**
 * ミナミとレナ、どちらを選ぶか究極の選択です・・！
 * <p>
 * ミナミを選ぶ方は「MINAMI」、レナたんを選ぶ方は「RENA」と標準出力で出力してください。どちらを選ぶかによって物語の展開が変わります！！
 * ミッション3
 * "MINAMI"か"RENA"を出力してください。
 * 期待する出力
 * 文字列 "MINAMI"か"RENA"を出力してください。
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * 出力例１
 * RENA
 * 出力例2
 * MINAMI
 */
public class _0003 {
  public static void main(String[] args) {
    System.out.println(new Random().nextBoolean() ? "RENA" : "MINAMI");
  }
}

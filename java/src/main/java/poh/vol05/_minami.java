package poh.vol05;

import java.util.Scanner;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 会社を辞めて、一から自分でアプリ開発を頑張るぜ！
 * <p>
 * レナとの婚約を破棄し、会社を辞めた達也は、自宅で開発していたスマートフォンゲームアプリ開発を頑張る事にしました。今開発中のアプリの処理を完成させてください。
 * そのアプリは横が x マス、縦が y マスの格子状の盤面に爆弾が下から敷き詰められ、ユーザーが爆弾をタッチし点火させて爆発させるゲームです。以下の図のように、残った爆弾は下の爆弾が爆発して出来た盤面の空き領域に真っ直ぐ下へ落下していきます。
 * あなたは点火させる処理まで作りましたが爆発した爆弾が落下した後の盤面に爆弾がどのように残るかを計算しなくてはいけません。
 * Figure minami
 * 実際の入力１行目に半角スペース区切りで １行目に半角スペース区切りで 0 が空き領域、 1 が爆弾、 2 が点火された爆弾で表現され半角スペース区切りで入力されます。
 * 実際の図の入力を表すと
 * 4 4
 * 1 0 1 0
 * 2 2 2 0
 * 2 2 1 1
 * 1 1 2 2
 * となり、出力は以下のようになります。
 * 0 0 0 0
 * 0 0 0 0
 * 1 0 1 0
 * 1 1 1 1
 * ミッションX
 * 爆弾が爆発した後の盤面の状態を出力して下さい。
 * 入力される値
 * 入力は以下のフォーマットで与えられる。
 * x y
 * t_0_0 t_1_0 t_2_0 ... t_x_0
 * t_0_1 t_1_1 t_2_1 ... t_x_1
 * ,,,
 * t_0_y t_1_y t_2_y ... t_x_y
 * 条件
 * すべてのテストケースにおいて、以下の条件をみたします。
 * 1 ≦ x ≦ 20
 * 1 ≦ y ≦ 20
 * t is 0 , 1, or 2
 * <p>
 * 期待する出力
 * 爆弾が爆発した後の盤面の状態を出力して下さい。
 * 最後は改行し、余計な文字、空行を含んではならない。
 * 入力例1
 * 3 4
 * 1 0 2
 * 1 2 1
 * 1 2 2
 * 1 2 2
 * <p>
 * 出力例1
 * 1 0 0
 * 1 0 0
 * 1 0 0
 * 1 0 1
 * <p>
 * 入力例2
 * 3 5
 * 0 0 0
 * 2 1 0
 * 1 1 1
 * 1 2 1
 * 2 1 2
 * 出力例2
 * 0 0 0
 * 0 0 0
 * 0 1 0
 * 1 1 1
 * 1 1 1
 */
public class _minami {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int x = sc.nextInt(), y = sc.nextInt();
    int[][] blownUpField = Stream.generate(() ->
        IntStream.range(0, x).map(i -> sc.nextInt()).map(bom -> bom == 2 ? 0 : bom).toArray()
    ).limit(y).toArray(int[][]::new);
    Function<int[][], BiFunction<Integer, Integer, Stream<IntStream>>> changeDirection =
        field -> (a, b) -> IntStream.range(0, a).mapToObj(i -> IntStream.range(0, b).map(j -> field[j][i]));
    // 縦に持ち直してソートすることにより落下を表現する
    int[][] fellDowned = changeDirection.apply(blownUpField).apply(x, y)
        .map(s -> s.sorted().toArray())
        .toArray(int[][]::new);

    changeDirection.apply(fellDowned).apply(y, x)
        .map(s -> s.mapToObj(String::valueOf).collect(Collectors.joining(" ")))
        .forEach(System.out::println);
  }
}

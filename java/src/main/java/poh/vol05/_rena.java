package poh.vol05;

import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 障害が起きている会社のメインシステムを改修だ！！
 * <p>
 * レナとシリコンバレーに来たと思ったら、会社のメインシステムで障害が発生！？　しかもこのシステムを作った人達はもう会社に一人も居ないとの事。この危機を乗り切って俺の実力を全社に見せてやる！！
 * 障害を起こしているのは表を計算するアプリケーションです。表は横 x マス、縦 y マスで構成されており、各マスには数字が入っています。 あなたは範囲選択されたマスを合計するプログラムを書かなくてはいけません。
 * 選択範囲は左上のマスと右上のマスが指定され、そのマスを含んだ矩形の内側の範囲を複数の箇所を選択することが出来ます。矩形が重なるような選択範囲が指定された場合、選択部分は結合され多角形になります。
 * Figure rena
 * 上記データの入力値
 * 4 5 3
 * 948 608 920 216
 * 3 413 306 7
 * 312 173 0 1000
 * 365 726 280 358
 * 26 539 197 753
 * 2 1 3 3
 * 3 3 4 4
 * 1 4 4 5
 * <p>
 * ミッションX
 * 選択範囲に含まれるマスの全てを合計した数字を出力するプログラムを作成してください。
 * 入力される値
 * 入力は以下のフォーマットで与えられる。
 * x y N #表の横幅 x , 表の縦幅 y , 選択範囲の総数 N
 * t_1_1 t_2_1 ... t_x_1 #表の 1 行目の 1 から x 列目
 * t_1_2 t_2_2 ... t_x_2 #表の 2 行目の 1 から x 列目
 * ...
 * t_1_y t_2_y ... t_x_y #表の y 行目の 1 から x 列目
 * x_s_1 y_s_1 x_e_1 y_e_1 #1個目の選択範囲の左上の座標 x_s, y_e 右下の座標 x_e, y_e
 * x_s_2 y_s_2 x_e_2 y_e_2 #2個目の選択範囲の左上の座標 x_s, y_e 右下の座標 x_e, y_e
 * ...
 * x_s_N y_s_N x_e_N y_e_N #N個目の選択範囲の左上の座標 x_s, y_e 右下の座標 x_e, y_e
 * 条件
 * すべてのテストケースにおいて、以下の条件をみたします。
 * 1 ≦ x, y ≦ 20
 * 1 ≦ N ≦ 10
 * 0 ≦ t_i_j ≦ 1000 (1 ≦ i ≦ x ,1 ≦ j ≦ y)
 * 1 ≦ x_s_k, x_e_k ≦ x (1 ≦ k ≦ N)
 * 1 ≦ y_s_k, y_e_k ≦ y (1 ≦ k ≦ N)
 * 期待する出力
 * 区間合計点数を出力せよ。
 * 最後は改行し、余計な文字、空行を含んではならない。
 * 入力例1
 * 4 5 3
 * 948 608 920 216
 * 3 413 306 7
 * 312 173 0 1000
 * 365 726 280 358
 * 26 539 197 753
 * 2 1 3 3
 * 3 3 4 4
 * 1 4 4 5
 * <p>
 * <p>
 * 出力例1
 * 6664
 * 入力例2
 * 10 5 5
 * 119 677 334 254 884 256 588 445 682 206
 * 691 171 875 407 926 225 313 381 811 637
 * 291 452 658 153 335 657 4 189 408 386
 * 97 750 918 118 334 191 635 341 669 594
 * 51 239 413 394 667 113 558 288 757 788
 * 3 2 6 3
 * 9 4 10 4
 * 10 2 10 5
 * 5 1 6 4
 * 2 2 10 5
 * 出力例2
 * 17895
 */
public class _rena {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int x = sc.nextInt(), y = sc.nextInt(), n = sc.nextInt();
    IntFunction<AtomicInteger[]> getLine = i -> IntStream.range(0, x)
        .mapToObj(j -> new AtomicInteger(sc.nextInt()))
        .toArray(AtomicInteger[]::new);

    AtomicInteger[][] field = IntStream.range(0, y)
        .mapToObj(getLine)
        .toArray(AtomicInteger[][]::new);

    Supplier<int[]> getRangeIndices = () -> Stream.generate(sc::nextInt).mapToInt(i -> i - 1).limit(4).toArray();

    Function<int[], IntStream> rangeSelect = range -> IntStream.rangeClosed(range[1], range[3])
        .flatMap(i -> IntStream.rangeClosed(range[0], range[2]).map(j -> field[i][j].getAndSet(0)));
    System.out.println(Stream.generate(getRangeIndices).limit(n).flatMapToInt(rangeSelect).sum());
  }
}

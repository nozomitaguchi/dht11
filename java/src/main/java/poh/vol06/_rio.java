package poh.vol06;

import java.awt.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * ちょうどいい濃度のコーヒーを淹れ、古代コボール王Ⅳ世を撃破せよ！
 * <p>
 * あなたの手元には、コボール遺跡から発掘された古代コーヒーカップとお湯、
 * そしてコボール文明に伝わるレシピで作られたコーヒーの粉末があります。
 * これらを使ってちょうどいい濃度のコーヒーを淹れましょう。
 * あなたは、次の 3 種類の操作を行うことができます。
 * <p>
 * 1.カップにお湯を A グラム入れる
 * 2.カップにコーヒー粉末を B グラム入れて、よくかき混ぜる
 * 3.味見のために、カップの中身を C グラム飲む
 * <p>
 * カップに X グラムのお湯と Y グラムのコーヒー粉末が既に入っているときに上記の操作を行った場合、
 * カップの中のお湯とコーヒー粉末の量はそれぞれ次の表のように変化します。
 * <p>
 * 操作1
 * 《お湯を入れる》	操作2
 * 《コーヒー粉末を入れる》	操作3
 * 《味見》
 * お湯	X+A	X	X-C*X/(X+Y)
 * コーヒー粉末	Y	Y+B	Y-C*Y/(X+Y)
 * 指定された順番でこれらの操作を行ったとき、最後にカップに残ったコーヒーの濃度が何パーセントかを求めてください。
 * ここで、お湯が X グラム、コーヒー粉末が Y グラム入ったコーヒーの濃度は 100*Y/(X+Y) パーセントと計算できます。
 * なお、答えは小数点以下を切り捨てた整数値で出力してください。
 * 次の図は、入力例 2 におけるカップの中身の変化を表しています。
 * Figure rio 1
 * すべての操作が終わった後で、カップには約 62.9 グラムのお湯と約 47.1 グラムのコーヒー粉末が入っているので、
 * コーヒーの濃度は約 42.8 パーセントとなり、小数点以下を切り捨てた 42 が答えとなります。
 * ミッション１
 * 最後にカップに残ったコーヒーの濃度を求めてください。
 * 解答次第で世界の運命が決まります！！
 * 入力される値
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * N
 * t_1 s_1
 * t_2 s_2
 * ...
 * t_N s_N
 * 1 行目には操作を行う回数を表す整数 N が与えられます。 続く N 行には、行う操作の種類と分量が与えられます。 具体的には次のようになります。
 * <p>
 * 《t_i が 1 のとき》i 回目には「カップにお湯を s_i グラム入れる」操作を行います。
 * 《t_i が 2 のとき》i 回目には「カップにコーヒー粉末を s_i グラム入れる」操作を行います。
 * 《t_i が 3 のとき》i 回目には「カップの中身を s_i グラム飲む」操作を行います。
 * 条件
 * すべてのテストケースにおいて、以下の条件をみたします。
 * 1 ≦ N ≦ 10
 * t_i は 1, 2, 3 のいずれか
 * 1 ≦ s_i ≦ 100
 * 各操作が終わった後にカップが空になっていることはない
 * <p>
 * <p>
 * 期待する出力
 * N 回の操作が終わった後のカップに残ったコーヒーの濃度（パーセント）を、小数点以下切り捨てで 1 行に出力してください。
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * 入力例1
 * 2
 * 1 90
 * 2 10
 * <p>
 * <p>
 * 出力例1
 * 10
 * 入力例2
 * 4
 * 1 50
 * 2 55
 * 3 15
 * 1 20
 * 出力例2
 * 42
 */
public class _rio {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    Point[] procedures = Stream.generate(() -> new Point(sc.nextInt(), sc.nextInt())).limit(n).toArray(Point[]::new);

    BigDecimal hotWater = BigDecimal.valueOf(0), beans = BigDecimal.valueOf(0);
    for (Point p : procedures) {
      BigDecimal s = BigDecimal.valueOf(p.y);
      switch (p.x) {
        case 1:
          hotWater = hotWater.add(s);
          break;
        case 2:
          beans = beans.add(s);
          break;
        default: // 直接代入すると、濃度が計算できないので後入れ。
          BigDecimal k = beans.subtract(getRatio(beans, hotWater, s));
          hotWater = hotWater.subtract(getRatio(hotWater, beans, s));
          beans = k;
          break;
      }
    }
    System.out.println(getRatio(beans, hotWater, BigDecimal.valueOf(100)).intValue());
  }

  private static BigDecimal getRatio(BigDecimal a, BigDecimal b, BigDecimal s) {
    return a.divide(a.add(b), MathContext.DECIMAL128).multiply(s);
  }
}

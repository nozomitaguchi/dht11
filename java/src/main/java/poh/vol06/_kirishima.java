package poh.vol06;

import java.util.Scanner;
import java.util.function.IntFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 古代コボールすごろくで古代コボール王Ⅳ世を撃破せよ！
 * <p>
 * 古代コボールすごろくでは、n 個のマスが 1 マス目(スタート)から n マス目(ゴール)まで一列に並んでおり、各マスには 1 つの値が設定されています。
 * あなたは、1 番目のマス（スタート）にいます。 最初に 1 回だけルーレットを回し、その出目の数だけ正の方向（ゴール方向）へ進みます。
 * 移動した先のマスの値が 0 ならば終了で、そうでないならば、さらにその値だけ移動します。
 * マスの値が正の数ならば正の方向へ移動し、負の数ならば負の方向（スタート方向）へ移動します。
 * 移動先のマスでも、値に従って同様のことを繰り返します。
 * また、移動しようとした先のマスが存在しない場合は、移動を行わずにゲームオーバーとします。
 * 次の表は、n = 8 のときの各マスの値の例を表しています。
 * Figure kirishima 1
 * この表について、
 * 出目が 7 ならば、1 番目 -> 8 番目とマスを移動してあがり
 * 出目が 3 ならば、1 番目 -> 4 番目 -> 2 番目 -> 8番目とマスを移動してあがり
 * 出目が 4 ならば、1 番目 -> 5 番目とマスを移動してゲームオーバー
 * 出目が 2 ならば、1 番目 -> 3 番目 -> 1 番目とマスを移動してゲームオーバー
 * 出目が 5 ならば、1 番目 -> 6 番目 -> 7 番目 -> 6 番目 -> ...
 * とマスを移動し続けることになり、あがれないのでゲームオーバー
 * 出目が 10 ならば、1 番目のマスから移動できずにゲームオーバーとなります。
 * <p>
 * 上記パターンのイメージを見る
 * 各マスの値とルーレットの出目が与えられたときに、 n 番目のマスまで移動して終了することができるかどうかを調べるプログラムを作成してください。 各マスの値とルーレットの出目が与えられたときに、 n 番目（ゴール）のマスまで移動して終了することができるかどうかを調べるプログラムを作成してください。
 * ミッション１
 * スゴロクがゴール可能か調べてください。
 * 解答次第で世界の運命が決まります！！
 * 入力される値
 * 入力は標準入力にて以下のフォーマットで与えられます。
 * n
 * t_1 t_2 ... t_n
 * m
 * d_1
 * d_2
 * ...
 * d_m
 * n はマスの数、t_i は i 番目のマスに書かれている数字を表しています。 m は出目が与えられる回数を表しており、d_j は、j 回目に与えられる出目を表しています。
 * 条件
 * すべてのテストケースにおいて、以下の条件をみたします。
 * 2 ≦ n ≦ 100
 * t_1 = t_n = 0 (1 番目と n 番目のマスの値は必ず 0)
 * -100 ≦ t_i ≦ 100 (2 ≦ i ≦ n-1)
 * 1 ≦ m ≦ 100
 * 1 ≦ d_j ≦ 100 (1 ≦ j ≦ m)
 * <p>
 * <p>
 * 期待する出力
 * 各出目に対して、ゴールできる場合には、"Yes" そうでない場合は "No" を 1 行ごとに出力してください。
 * 最後は改行し、余計な文字、空行を含んではいけません。
 * 入力例1
 * 8
 * 0 6 -2 -2 0 1 -1 0
 * 6
 * 7
 * 3
 * 4
 * 2
 * 5
 * 10
 * 出力例1
 * Yes
 * Yes
 * No
 * No
 * No
 * No
 * 入力例2
 * 5
 * 0 1 0 1 0
 * 4
 * 1
 * 1
 * 2
 * 3
 * 出力例2
 * No
 * No
 * No
 * Yes
 */
public class _kirishima {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    Block[] blocks = Stream.generate(sc::nextInt).limit(n).map(Block::new).toArray(Block[]::new);

    Block firstBlock = blocks[0], lastBlock = blocks[n - 1];
    firstBlock.setVisit(true);
    lastBlock.setArriveAtGoal(true);
    // 各ブロックがゴールできるか検証して覚えておく
    IntStream.range(1, n - 1).forEach(i -> verify(i, blocks));

    int m = sc.nextInt();
    IntFunction<String> f = i -> i < n && blocks[i].canArriveAtGoal() ? "Yes" : "No";
    Stream.generate(sc::nextInt).limit(m).map(f::apply).forEach(System.out::println);
  }

  private static boolean verify(int cur, Block[] blocks) {
    Block block = blocks[cur];
    if (block.isVisit()) return block.canArriveAtGoal();
    block.setVisit(true);
    // マス移動0の時、終点
    if (block.getNextTo() == 0) return block.canArriveAtGoal();
    int next = cur + block.getNextTo();
    if (0 < next && next < blocks.length) block.setArriveAtGoal(verify(next, blocks));
    return block.canArriveAtGoal();
  }

  private static class Block {
    private int nextTo;
    private boolean isVisit = false;
    private boolean canArriveAtGoal = false;

    Block(int nextTo) {
      this.nextTo = nextTo;
    }

    boolean isVisit() {
      return isVisit;
    }

    void setVisit(@SuppressWarnings("SameParameterValue") boolean isVisit) {
      this.isVisit = isVisit;
    }

    int getNextTo() {
      return nextTo;
    }

    boolean canArriveAtGoal() {
      return canArriveAtGoal;
    }

    void setArriveAtGoal(boolean canArriveAtGoal) {
      this.canArriveAtGoal = canArriveAtGoal;
    }
  }
}



package daily_coding_problem;

import java.util.Collections;
import java.util.Scanner;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * This problem was asked by Google.
 * Given a stack of N elements, interleave the first half of the stack with the second half reversed using only one other queue. This should be done in-place.
 * Recall that you can only push or pop from a stack, and enqueue or dequeue from a queue.
 * For example, if the stack is [1, 2, 3, 4, 5], it should become [1, 5, 2, 4, 3]. If the stack is [1, 2, 3, 4], it should become [1, 4, 2, 3].
 * Hint: Try working backwards from the end state.
 */
public class _0001 {

  /**
   * stackの場合は, push か pop しか使っちゃだめ.
   *
   * @param args 入力値.
   */
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Stack<String> stack = new Stack<>();
    Stream.of(sc.nextLine().split(", ")).forEach(stack::push);

    Stack<String> result = new Stack<>();
    IntStream.range(0, stack.size()).forEach(i -> {
      Collections.reverse(stack);
      result.push(stack.pop());
    });

    System.out.println(result.stream().collect(Collectors.joining(", ")));
  }

}

package daily_coding_problem;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * This problem was asked by Uber.
 * <p>
 * Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i. Solve it without using division and in O(n) time.
 * <p>
 * For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
 */
public class _0002free {

  /**
   * ルールを無視したバージョン.
   *
   * @param args 入力値.
   */
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int[] array = Stream.of(sc.nextLine().split(", ")).mapToInt(Integer::parseInt).toArray();
    int product = IntStream.of(array).reduce((l, r) -> l * r).orElse(0);
    IntStream filtered = IntStream.of(array).map(i -> product / i);
    System.out.println(filtered.mapToObj(String::valueOf).collect(Collectors.joining(", ")));
  }

}

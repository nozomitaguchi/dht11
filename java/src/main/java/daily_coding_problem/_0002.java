package daily_coding_problem;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This problem was asked by Uber.
 * <p>
 * Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i. Solve it without using division and in O(n) time.
 * <p>
 * For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
 */
public class _0002 {

  /**
   * . 2 3 4 5
   * 1 . 3 4 5
   * 1 2 . 4 5
   * 1 2 3 . 5
   * 1 2 3 4 .
   * <p>
   * (1), (1 x 2), (1 x 2 x 3), (1 x 2 x 3 x 4)
   * は、それぞれ、前の要素に自分を掛けただけで表現できるので
   * 二重ループの解き方に比べて計算量を減らせるはず。
   *
   * @param args 入力値.
   */
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int[] array = Stream.of(sc.nextLine().split(", ")).mapToInt(Integer::parseInt).toArray();
    int length = array.length, s = 1, e = 1;
    // 初期値は1詰め.
    Integer[] result = Stream.generate(() -> 1).limit(length).toArray(Integer[]::new);

    for (int i = 0, j = 1, k = 2; i < length - 1; i++, j++, k++) {
      s *= array[i];
      result[j] *= s;
      e *= array[length - j];
      result[length - k] *= e;
    }

    System.out.println(Stream.of(result).map(String::valueOf).collect(Collectors.joining(", ")));
  }

}

package daily_coding_problem;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * This problem was asked by Google.
 * Given a stack of N elements, interleave the first half of the stack with the second half reversed using only one other queue. This should be done in-place.
 * Recall that you can only push or pop from a stack, and enqueue or dequeue from a queue.
 * For example, if the stack is [1, 2, 3, 4, 5], it should become [1, 5, 2, 4, 3]. If the stack is [1, 2, 3, 4], it should become [1, 4, 2, 3].
 * Hint: Try working backwards from the end state.
 */
public class _0001free {

  /**
   * ルールを無視したバージョン.
   *
   * @param args 入力値.
   */
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String[] array = sc.nextLine().split(", ");
    int len = array.length;
    Stream<Integer> indexes = IntStream.rangeClosed(0, len).boxed();
    Stream<String> sorted = indexes.flatMap(i -> Stream.of(array[i], array[len - 1 - i])).limit(len);
    System.out.println(sorted.collect(Collectors.joining(", ")));
  }

}

package euler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.IntSupplier;

/**
 * Problem 2 「偶数のフィボナッチ数」
 * <p>
 * フィボナッチ数列の項は前の2つの項の和である. 最初の2項を 1, 2 とすれば, 最初の10項は以下の通りである.
 * <p>
 * 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 * 数列の項の値が400万以下の, 偶数値の項の総和を求めよ.
 */
public class _0002 {
  public static void main(String[] args) {
    Integer limit = new Scanner(System.in).nextInt();
    List<Integer> fib = new ArrayList<>(Arrays.asList(0, 1));
    IntSupplier next = () -> fib.get(fib.size() - 2) + fib.get(fib.size() - 1);
    while (next.getAsInt() <= limit) fib.add(next.getAsInt());
    System.out.println(fib.stream().mapToInt(i -> i).filter(i -> i % 2 == 0).sum());
  }
}

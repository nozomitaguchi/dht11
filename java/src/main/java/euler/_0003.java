package euler;

import java.util.Scanner;

/**
 * Problem 3 「最大の素因数」
 * <p>
 * 13195 の素因数は 5, 7, 13, 29 である.
 * <p>
 * 600851475143 の素因数のうち最大のものを求めよ.
 */
public class _0003 {
  public static void main(String[] args) {
    long index = 2, target = new Scanner(System.in).nextLong();
    while (index <= target) if (target % index++ == 0) target /= --index;
    System.out.println(index);
  }
}

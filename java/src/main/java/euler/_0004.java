package euler;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Problem 4 「最大の回文積」
 * <p>
 * 左右どちらから読んでも同じ値になる数を回文数という. 2桁の数の積で表される回文数のうち, 最大のものは 9009 = 91 × 99 である.
 * <p>
 * では, 3桁の数の積で表される回文数の最大値を求めよ.
 */
public class _0004 {
  public static void main(String[] args) {
    // TODO: 小難しくなってしまったので, シンプルに書き直す.
    Scanner sc = new Scanner(System.in);
    int min = sc.nextInt(), max = sc.nextInt();
    IntStream combs = IntStream.rangeClosed(min, max).flatMap(i -> IntStream.rangeClosed(min, max).map(j -> j * i)).distinct();
    System.out.println(combs.filter(_0004::isPalindrome).max().orElse(0));
  }

  /**
   * 回文数か判定する.
   * <p>
   * 同桁の最小回文数で割った余りが [別の回文数 * 10の倍数] or [0] になるかを,
   * 再帰的に調べることによって回文数か判定を行う.
   * <p>
   * ex) 28582 % 10001 = 8580, 858 % 101 = 5, 5 % 5 = 0
   * ex) 90509 % 10001 = 500, 5 % 5 = 0
   *
   * @param num 判定する数字.
   * @return 回文数の場合true, それ以外はfalse.
   */
  private static boolean isPalindrome(int num) {
    // Then num is 99499, minPalindromeOfSameDigit is 10001
    int digit = (int) Math.log10(num);
    int minPalindromeOfSameDigit = (int) Math.pow(10, digit) + 1;
    num %= minPalindromeOfSameDigit;
    return num == 0 || haveSamePalindromeZero(digit, num) && num % 10 == 0 && isPalindrome(deleteUnder0(num));
  }

  /**
   * なくなった桁数と, 次に実行する際に消える桁数の差が一致しているか判断する.
   * (実際はlog10で出るのは桁数-1になる.)
   * <p>
   * ex) 1111 -> 3 - 2 == 2 - 1
   * ex) 90509 -> 4 - 2 == 2 - 0
   * ex) 900010009 -> 8 - 4 == 4 - 0
   *
   * @param before 実行前の桁数.
   * @param num    次に回文判定を実行する加工前の数字.
   * @return 一致している場合true, それ以外はfalse
   */
  private static boolean haveSamePalindromeZero(int before, int num) {
    int digit = (int) Math.log10(num), next = (int) Math.log10(deleteUnder0(num));
    return before - digit == digit - next;
  }

  /**
   * 下n桁の0を除去する.
   *
   * @param num 除去する数字.
   * @return 除去済みの数字.
   */
  private static int deleteUnder0(int num) {
    if (num == 0 || num % 10 != 0) return num;
    return deleteUnder0(num / 10);
  }

}

package euler;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Problem 1 「3と5の倍数」
 * <p>
 * 10未満の自然数のうち, 3 もしくは 5 の倍数になっているものは 3, 5, 6, 9 の4つがあり, これらの合計は 23 になる.
 * <p>
 * 同じようにして, 1000 未満の 3 か 5 の倍数になっている数字の合計を求めよ.
 */
public class _0001 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(IntStream.range(1, sc.nextInt()).filter(i -> i % 3 == 0 || i % 5 == 0).sum());
  }
}

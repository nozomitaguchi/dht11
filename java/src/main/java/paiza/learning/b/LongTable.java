package paiza.learning.b;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class LongTable {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    int n = sc.nextInt(), m = sc.nextInt();

    Stream<SimpleEntry<Integer, Integer>> groups =
        Stream.generate(() -> new SimpleEntry<>(sc.nextInt(), sc.nextInt())).limit(m);

    List<Integer> seated = groups
        .map(group -> positions(n, group))
        .reduce(new ArrayList<>(), LongTable::sitDown);

    System.out.println(seated.size());

  }

  private static List<Integer> positions(int n, SimpleEntry<Integer, Integer> group) {
    return IntStream.range(group.getValue(), group.getKey() + group.getValue())
        .boxed()
        .map(i -> i % n)
        .collect(Collectors.toList());
  }

  private static List<Integer> sitDown(List<Integer> seated, List<Integer> notSeated) {
    boolean hasAlreadySeated = notSeated.stream().anyMatch(seated::contains);
    if (hasAlreadySeated) return seated;
    return Stream.of(seated, notSeated).flatMap(Collection::stream).collect(Collectors.toList());
  }

}

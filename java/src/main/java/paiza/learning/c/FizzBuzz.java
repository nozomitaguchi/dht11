package paiza.learning.c;

import java.util.Scanner;
import java.util.stream.IntStream;

public class FizzBuzz {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    IntStream.rangeClosed(1, sc.nextInt()).boxed().map(FizzBuzz::convert).forEach(System.out::println);
  }

  private static Object convert(int i) {
    if (i % 15 == 0) return "Fizz Buzz";
    if (i % 3 == 0) return "Fizz";
    if (i % 5 == 0) return "Buzz";
    return i;
  }

}

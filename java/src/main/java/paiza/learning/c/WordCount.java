package paiza.learning.c;

import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WordCount {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    String[] words = sc.nextLine().split(" ");

    Map<String, Long> wordCounts = Arrays.stream(words)
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    Arrays.stream(words)
        .distinct()
        .map(word -> word + " " + wordCounts.getOrDefault(word, 0L))
        .forEach(System.out::println);

  }

}

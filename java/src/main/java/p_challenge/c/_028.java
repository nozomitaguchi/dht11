package p_challenge.c;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _028 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int point = IntStream.range(0, sc.nextInt())
        .map(i -> grade(sc.next(), sc.next()))
        .sum();
    System.out.println(point);
  }

  /**
   * 採点する。
   *
   * @param correct 正解
   * @param answer  解答
   * @return 点数
   */
  private static int grade(String correct, String answer) {
    if (correct.equals(answer)) return 2;
    if (correct.length() == answer.length() &&
        countDefCharacters(correct, answer) == 1) return 1;
    return 0;
  }

  /**
   * 単語を比較して相違のある箇所をカウントする。
   * 正解と解答の長さが違う場合、exceptionが発生する可能性がある。
   *
   * @param correct 正解
   * @param answer  解答
   * @return 相違のある箇所の数
   */
  private static long countDefCharacters(String correct, String answer) {
    char[] correctChars = correct.toCharArray();
    char[] answerChars = answer.toCharArray();
    return IntStream.range(0, correctChars.length)
        .filter(i -> correctChars[i] != answerChars[i])
        .count();
  }
}
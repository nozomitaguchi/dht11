package p_challenge.c;

import java.util.Scanner;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

public class _021 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    IntUnaryOperator squaring = i -> (int) Math.pow(i, 2);
    int xc = sc.nextInt(), yc = sc.nextInt(), r1 = squaring.applyAsInt(sc.nextInt()), r2 = squaring.applyAsInt(sc.nextInt());
    IntStream.range(0, sc.nextInt())
        .map(i -> squaring.applyAsInt(sc.nextInt() - xc) + squaring.applyAsInt(sc.nextInt() - yc))
        .forEach(i -> System.out.println(r1 <= i && i <= r2 ? "yes" : "no"));
  }
}
package p_challenge.c;


import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class _040 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Map<String, List<Double>> heights = IntStream.range(0, sc.nextInt()).boxed()
        .collect(Collectors.groupingBy(i -> sc.next(), Collectors.mapping(i -> sc.nextDouble(), Collectors.toList())));
    Stream<Double> leHeights = heights.get("le").stream(), geHeights = heights.get("ge").stream();
    Comparator<Double> c = Comparator.comparing(Double::doubleValue);
    System.out.println(geHeights.max(c).orElse(0d) + " " + leHeights.min(c).orElse(0d));
  }
}
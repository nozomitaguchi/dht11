package p_challenge.c;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _013 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String n = sc.next();
    List<String> hateRooms = IntStream.range(0, sc.nextInt())
        .mapToObj(i -> sc.next())
        .filter(s -> !Arrays.asList(s.split("")).contains(n))
        .collect(Collectors.toList());
    if (hateRooms.isEmpty()) System.out.println("none");
    hateRooms.forEach(System.out::println);
  }
}
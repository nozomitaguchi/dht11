package p_challenge.c;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

public class _015 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(IntStream.range(0, sc.nextInt()).map(i -> (int) (getPointRate(sc.next()) * sc.nextDouble())).sum());
  }

  /**
   * ポイント還元率を取得する。
   *
   * @param date 日付
   * @return ポイント還元率
   */
  private static double getPointRate(String date) {
    List<String> d = Arrays.asList(date.split(""));
    return d.contains("3") ? 0.03 : d.contains("5") ? 0.05 : 0.01;
  }
}
package p_challenge.c;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _030 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int h = sc.nextInt(), w = sc.nextInt();
    IntStream.range(0, h)
        .mapToObj(i -> IntStream.range(0, w)
            .mapToObj(j -> sc.nextInt() < 128 ? "0" : "1")
            .collect(Collectors.joining(" ")))
        .forEach(System.out::println);
  }
}
package p_challenge.c;

import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.IntStream;

public class _005 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Predicate<String[]> f = ar -> ar.length == 4 && Arrays.stream(ar).allMatch(s -> !"".equals(s) && Double.parseDouble(s) <= 255);
    IntStream.range(0, sc.nextInt())
        .mapToObj(i -> f.test(sc.next().split("[.]")) ? "True" : "False")
        .forEach(System.out::println);
  }
}
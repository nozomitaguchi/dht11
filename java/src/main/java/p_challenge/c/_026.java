package p_challenge.c;

import java.util.Comparator;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.IntStream;

public class _026 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt(), s = sc.nextInt(), p = sc.nextInt();
    Optional<Object> bestCarrot = IntStream.rangeClosed(1, n)
        .mapToObj(i -> new int[]{i, sc.nextInt(), sc.nextInt()})
        .filter(ar -> (s - p) <= ar[2] && ar[2] <= (s + p))
        .max(Comparator.comparingInt(ar -> ar[1]))
        .map(ar -> ar[0]);
    System.out.println(bestCarrot.orElse("not found"));
  }
}
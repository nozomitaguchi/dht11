package p_challenge.c;

import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _043 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Map<Integer, Long> counts = IntStream.range(0, sc.nextInt()).map(i -> sc.nextInt()).boxed().collect(Collectors.groupingBy(s -> s, Collectors.counting()));
    Long max = counts.values().stream().mapToLong(i -> i).max().orElse(0L);
    String result = counts.entrySet().stream().filter(e -> Objects.equals(e.getValue(), max)).map(Map.Entry::getKey).sorted(Comparator.naturalOrder()).map(Object::toString).collect(Collectors.joining(" "));
    System.out.println(result);
  }
}
package p_challenge.c;

import java.util.Scanner;

public class _016 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String result = sc.next();
    String[][] strings = {{"A", "4"}, {"E", "3"}, {"G", "6"}, {"I", "1"}, {"O", "0"}, {"S", "5"}, {"Z", "2"}};
    for (String[] string : strings) result = result.replace(string[0], string[1]);
    System.out.println(result);
  }
}
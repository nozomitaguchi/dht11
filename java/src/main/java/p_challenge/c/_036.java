package p_challenge.c;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class _036 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Integer[] fst = {sc.nextInt(), sc.nextInt()},
        snd = {sc.nextInt(), sc.nextInt()},
        contestantTimes = {0, sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()},
        finalistTimes = {0, sc.nextInt(), sc.nextInt()};
    Integer[] finalist = {race(contestantTimes, fst), race(contestantTimes, snd)};
    boolean isYoungWinner = race(finalistTimes, new Integer[]{1, 2}) == 1;
    Arrays.stream(finalist)
        .sorted(isYoungWinner ? Comparator.naturalOrder() : Comparator.reverseOrder())
        .forEach(System.out::println);
  }

  /**
   * レースを行う．
   *
   * @param times      タイム
   * @param contestant 出場者
   * @return 勝者
   */
  private static int race(Integer[] times, Integer[] contestant) {
    return times[contestant[0]] < times[contestant[1]] ? contestant[0] : contestant[1];
  }
}
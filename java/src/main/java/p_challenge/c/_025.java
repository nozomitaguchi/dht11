package p_challenge.c;

import java.util.HashMap;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class _025 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int m = sc.nextInt(), n = sc.nextInt();
    HashMap<Integer, Integer> sheetsPerHour = Stream.generate(() -> new int[]{sc.nextInt(), sc.nextInt(), sc.nextInt()}).limit(n)
        .collect(Collectors.toMap(ar -> ar[0], ar -> ar[2], (r, l) -> r + l, HashMap::new));
    System.out.println(sheetsPerHour.values().stream().reduce(0, (acc, i) -> acc + (int) Math.ceil((double) i / m)));
  }
}
package p_challenge.c;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _038 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    // m: 機械の数, n: お菓子の数
    int m = sc.nextInt(), n = sc.nextInt();
    // key: 容器の数, value: 機械番号
    Map<Integer, Integer> machines = IntStream.rangeClosed(1, m).boxed()
        .collect(Collectors.toMap(i -> sc.nextInt(), Function.identity()));
    // お菓子の余りが一番少なく、最大の容器数の機械を探す。
    Optional<Integer> best = machines.keySet().stream()
        .sorted(Comparator.reverseOrder())
        .min(Comparator.comparing(i -> n % i));
    System.out.println(machines.get(best.orElse(0)));
  }
}
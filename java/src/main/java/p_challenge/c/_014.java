package p_challenge.c;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _014 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt(), diameter = sc.nextInt() * 2;
    IntStream.rangeClosed(1, n)
        .filter(i -> diameter <= IntStream.range(0, 3).map(j -> sc.nextInt()).min().orElse(0))
        .forEach(System.out::println);
  }
}
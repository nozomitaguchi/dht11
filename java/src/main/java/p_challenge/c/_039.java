package p_challenge.c;

import java.util.Arrays;
import java.util.Scanner;

public class _039 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int count = Arrays.stream(sc.nextLine().split(""))
        .mapToInt(s -> "<".equals(s) ? 10 : "/".equals(s) ? 1 : 0)
        .sum();
    System.out.println(count);
  }
}
package p_challenge.c;

import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _032 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Map<Integer, List<Integer>> prices = IntStream.range(0, sc.nextInt()).boxed()
        .collect(Collectors.groupingBy(i -> sc.nextInt(), Collectors.mapping(i -> sc.nextInt(), Collectors.toList())));
    IntUnaryOperator pointRate = (int i) -> i == 0 ? 5 : i == 1 ? 3 : i == 2 ? 2 : 1;
    int points = prices.entrySet().stream()
        .mapToInt(e -> pointRate.applyAsInt(e.getKey()) * (e.getValue().stream().mapToInt(i -> i).sum() / 100))
        .sum();
    System.out.println(points);
  }
}
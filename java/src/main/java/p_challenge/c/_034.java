package p_challenge.c;

import java.util.Scanner;

@SuppressWarnings("unused")
public class _034 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String a = sc.next(), op = sc.next(), b = sc.next(), eq = sc.next(), c = sc.next();
    int sign = "+".equals(op) ? 1 : -1;
    int x = "x".equals(a) ? i(c) - i(b) * sign : "x".equals(b) ? (i(c) - i(a)) * sign : i(a) + i(b) * sign;
    System.out.println(x);
  }

  private static int i(String s) {
    return Integer.parseInt(s);
  }
}
package p_challenge.c;

import java.util.Scanner;

public class _024 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int[] nums = {0, 0};
    for (int i = 0, n = sc.nextInt(); i < n; i++) {
      switch (sc.next()) {
        case "SET":
          nums[sc.nextInt() - 1] = sc.nextInt();
          break;
        case "ADD":
          nums[1] = nums[0] + sc.nextInt();
          break;
        case "SUB":
          nums[1] = nums[0] - sc.nextInt();
      }
    }
    System.out.println(nums[0] + " " + nums[1]);
  }
}
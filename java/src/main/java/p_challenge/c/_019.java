package p_challenge.c;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _019 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    // TODO: 素因数分解での解き方にも挑戦してみる。
    IntStream.range(0, sc.nextInt())
        .map(i -> sc.nextInt())
        .mapToObj(i -> IntStream.range(1, i).filter(j -> i % j == 0).sum() - i)
        .map(i -> i == 0 ? "perfect" : Math.abs(i) == 1 ? "nearly" : "neither")
        .forEach(System.out::println);
  }
}
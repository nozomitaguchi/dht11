package p_challenge.c;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _022 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int[][] ar = IntStream.range(0, n)
        .mapToObj(i -> new int[]{sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()}).toArray(int[][]::new);
    System.out.println(ar[0][0] + " " + ar[n - 1][1] + " "
        + IntStream.range(0, n).map(i -> ar[i][2]).max().orElse(0) + " "
        + IntStream.range(0, n).map(i -> ar[i][3]).min().orElse(0));
  }
}
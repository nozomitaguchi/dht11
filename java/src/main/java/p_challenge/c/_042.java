package p_challenge.c;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _042 {
  /*
   * 勝者をmapで取得して, 表を出力する.
   */
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    // key: 1, value: [2, 3] の時, 1が 2と3に勝ったことを表す
    Map<Integer, List<Integer>> winners = IntStream.range(0, n * (n - 1) / 2).boxed()
        .collect(Collectors.groupingBy(i -> sc.nextInt(), Collectors.mapping(i -> sc.nextInt(), Collectors.toList())));

    IntStream.rangeClosed(1, n).mapToObj(i -> IntStream.rangeClosed(1, n).mapToObj(j ->
        i == j ? "-" : winners.getOrDefault(i, new ArrayList<>()).contains(j) ? "W" : "L")
    ).map(s -> s.collect(Collectors.joining(" "))).forEach(System.out::println);

  }
}
package p_challenge.c;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class _035 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long count = IntStream.range(0, sc.nextInt())
        .filter(i -> {
          String faculty = sc.next();
          int[] records = IntStream.range(0, 5).map(j -> sc.nextInt()).toArray();
          return 350 <= Arrays.stream(records).sum() &&
              160 <= ("s".equals(faculty) ? records[1] + records[2] : records[3] + records[4]);
        }).count();
    System.out.println(count);
  }
}
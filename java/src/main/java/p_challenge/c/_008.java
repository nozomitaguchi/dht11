package p_challenge.c;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _008 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String[] tags = sc.nextLine().split(" ");
    // 最短マッチで取得
    Matcher matcher = Pattern.compile(String.join(".*?", tags)).matcher(sc.nextLine());
    while (matcher.find()) {
      String value = matcher.group().replace(tags[0], "").replace(tags[1], "");
      System.out.println(value.isEmpty() ? "<blank>" : value);
    }
  }
}
package p_challenge.c;

import java.util.Scanner;

public class _037 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String[] date = sc.next().split("/"), time = sc.next().split(":");
    int day = Integer.parseInt(date[1]), hour = Integer.parseInt(time[0]);
    System.out.println(date[0] + "/" + String.format("%02d", (day + hour / 24)) +
        " " + String.format("%02d", (hour % 24)) + ":" + time[1]);
  }
}
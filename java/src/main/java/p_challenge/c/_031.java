package p_challenge.c;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _031 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    // key: 国名, value: 時差
    Map<String, Integer> timeDifferences = IntStream.range(0, sc.nextInt()).boxed()
        .collect(Collectors.toMap(i -> sc.next(), i -> sc.nextInt(), (u, v) -> v, LinkedHashMap::new));
    int diff = timeDifferences.get(sc.next());
    String[] postTime = sc.next().split(":");
    IntUnaryOperator getLocalTime = (int time) -> {
      int timeDifference = time - diff;
      // 前日になることを考慮して24を加算後、余りを求める。
      return (Integer.parseInt(postTime[0]) + timeDifference + 24) % 24;
    };
    timeDifferences.values().stream()
        .map(v -> String.format("%02d", getLocalTime.applyAsInt(v)) + ":" + postTime[1])
        .forEach(System.out::println);
  }
}
package p_challenge.c;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _029 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int m = sc.nextInt(), n = sc.nextInt();
    List<Integer[]> rainyPercents = IntStream.range(0, m)
        .mapToObj(i -> new Integer[]{sc.nextInt(), sc.nextInt()})
        .collect(Collectors.toList());
    IntUnaryOperator nDaysTotal = i -> IntStream.range(0, n).map(j -> rainyPercents.get(i + j)[1]).sum();
    // key: n日間の合計降水確率, value: 開始日, 同じ降水確率の場合は先勝ち
    Map<Integer, Integer> totalPercents = IntStream.rangeClosed(0, rainyPercents.size() - n).boxed()
        .collect(Collectors.toMap(nDaysTotal::applyAsInt, i -> rainyPercents.get(i)[0], (u, r) -> u, HashMap::new));
    Integer min = totalPercents.keySet().stream().min(Comparator.naturalOrder()).orElse(0);
    Integer bestStartDay = totalPercents.get(min);
    System.out.println(bestStartDay + " " + (bestStartDay + n - 1));
  }
}
package p_challenge.c;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class _023 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    List<Integer> winningNums = Stream.generate(sc::nextInt).limit(6).collect(Collectors.toList());
    LongStream.range(0, sc.nextInt())
        .map(i -> IntStream.range(0, 6)
            .filter(j -> winningNums.contains(sc.nextInt()))
            .count()
        ).forEach(System.out::println);
  }
}
package p_challenge.c;

import java.util.Scanner;
import java.util.function.BiPredicate;
import java.util.stream.IntStream;

public class _010 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    double a = sc.nextDouble(), b = sc.nextDouble(), R = sc.nextDouble();
    BiPredicate<Double, Double> isSilentPlace = (x, y) -> Math.pow(x - a, 2) + Math.pow(y - b, 2) >= Math.pow(R, 2);
    IntStream.range(0, sc.nextInt())
        .mapToObj(i -> isSilentPlace.test(sc.nextDouble(), sc.nextDouble()) ? "silent" : "noisy")
        .forEach(System.out::println);
  }
}
package p_challenge.c;

import java.util.Comparator;
import java.util.Scanner;
import java.util.stream.IntStream;

public class _006 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    // n: アイテム数, m = 参加人数, k = 上位者出力人数
    int n = sc.nextInt(), m = sc.nextInt(), k = sc.nextInt();
    double[] itemRates = IntStream.range(0, n).mapToDouble(i -> sc.nextDouble()).toArray();
    IntStream.range(0, m)
        .mapToDouble(i -> IntStream.range(0, n).mapToDouble(j -> sc.nextDouble() * itemRates[j]).sum())
        .boxed()
        .sorted(Comparator.reverseOrder())
        .limit(k)
        .forEach(score -> System.out.println(Math.round(score)));
  }
}
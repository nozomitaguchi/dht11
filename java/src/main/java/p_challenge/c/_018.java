package p_challenge.c;

import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _018 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Function<Integer, Map<String, Integer>> f = i ->
        IntStream.range(0, i).boxed().collect(Collectors.toMap(j -> sc.next(), j -> sc.nextInt()));
    Map<String, Integer> recipe = f.apply(sc.nextInt()), pieceInHand = f.apply(sc.nextInt());
    int count = recipe.entrySet().stream()
        .mapToInt(e -> pieceInHand.getOrDefault(e.getKey(), 0) / e.getValue()).min().orElse(0);
    System.out.println(count);
  }
}
package p_challenge.c;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _017 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int a = sc.nextInt(), b = sc.nextInt();
    IntStream.range(0, sc.nextInt()).forEach(i -> {
      int A = sc.nextInt(), B = sc.nextInt();
      System.out.println(a < A || (a == A && b > B) ? "Low" : "High");
    });
  }
}
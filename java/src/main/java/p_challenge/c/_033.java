package p_challenge.c;

import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.IntStream;

public class _033 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int pitchCount = sc.nextInt();
    Function<String, String> judgeLastPitch = lastPitch -> "ball".equals(lastPitch) ? "fourball!" : "out!";
    IntStream.rangeClosed(1, pitchCount)
        .mapToObj(i -> i == pitchCount ? judgeLastPitch.apply(sc.next()) : sc.next() + "!")
        .forEach(System.out::println);
  }
}
package p_challenge.c;


import java.util.Comparator;
import java.util.Scanner;
import java.util.stream.IntStream;

public class _041 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    IntStream.range(0, sc.nextInt())
        .mapToObj(i -> new int[]{sc.nextInt(), sc.nextInt(), sc.nextInt()})
        .sorted(Comparator.<int[]>comparingInt(ar -> ar[0]).thenComparingInt(ar -> ar[1]).thenComparingInt(ar -> ar[2]).reversed())
        .forEach(ar -> System.out.println(ar[0] + " " + ar[1] + " " + ar[2]));
  }
}
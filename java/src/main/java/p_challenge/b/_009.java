package p_challenge.b;

import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class _009 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    LinkedHashMap<String, Integer> table = IntStream.range(0, n).boxed()
        .collect(Collectors.toMap(i -> sc.next(), i -> sc.nextInt(), (l, r) -> l, LinkedHashMap::new));

    AtomicInteger count = new AtomicInteger();
    Integer[] shows = table.values().toArray(new Integer[0]);
    IntUnaryOperator getStart = time -> {
      int nextStart = time + shows[count.getAndIncrement()] + 10,
          nextEnd = nextStart + shows[count.get()];
      // 昼休憩は通常より50分長い．
      return nextStart + (12 * 60 < nextEnd && nextEnd < 13 * 60 ? 50 : 0);
    };
    Integer[] starts = Stream.iterate(10 * 60, getStart::applyAsInt).limit(shows.length).toArray(Integer[]::new);

    String[] names = table.keySet().toArray(new String[0]);
    Function<Integer, String> getTime = i -> String.format("%02d", i / 60) + ":" + String.format("%02d", i % 60);
    IntStream.range(0, n)
        .mapToObj(i -> getTime.apply(starts[i]) + " - " + getTime.apply(starts[i] + shows[i]) + " " + names[i])
        .forEach(System.out::println);
  }
}
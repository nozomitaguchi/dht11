package p_challenge.b;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class _031 {
  public static void main(String[] args) throws Exception {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    int size = Integer.parseInt(br.readLine());
    char[] board = br.readLine().toCharArray();
    char[] reversedBoard = reverse(size, board);
    System.out.println(countB(reversedBoard));
  }

  /**
   * コインの裏表を挟まれているものがなくなるまで反転する。
   * <p>
   * ex)
   * wwbwbbwb
   * ↓
   * wwwbwwbb
   * ↓
   * wwwwbbbb
   *
   * @param size  コインの総数
   * @param board コインが並べられているボード
   * @return 反転し終えたボード
   */
  private static char[] reverse(int size, char[] board) {
    char[] result = new char[size];
    char target = board[0];
    int point = 0, count = 0;
    for (int i = 1; i < size; i++) {
      char coin = board[i];
      if (target == coin) {
        continue;
      }
      for (int j = point; j < i; j++) {
        result[j] = count == 0 ? target : coin;
      }
      target = coin;
      point = i;
      count++;
    }
    for (int i = point; i < size; i++) {
      result[i] = target;
    }
    if (count > 1) {
      return reverse(size, result);
    }
    return result;
  }

  /**
   * ボードに含まれる黒コイン(b)の数を数える。
   *
   * @param board コインが並べられているボード
   * @return 黒コイン(b)の数
   */
  private static int countB(char[] board) {
    int count = 0;
    for (char coin : board) {
      if (coin == 'b') {
        count++;
      }
    }
    return count;
  }
}
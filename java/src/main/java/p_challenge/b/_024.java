package p_challenge.b;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _024 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    double r = sc.nextDouble();
    long sum = IntStream.rangeClosed(0, (int) r + 1).mapToLong(y ->
        IntStream.rangeClosed(0, (int) r + 1).filter(x ->
            Math.sqrt(x * x + y * y) < r).count()
    ).sum();
    System.out.println(sum * 4);
  }
}

package p_challenge.b;

import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class _015 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    Supplier<Integer[]> getDigitalNumber = () ->
        IntStream.range(0, 7).mapToObj(i -> sc.nextInt()).toArray(Integer[]::new);
    Integer[] a = getDigitalNumber.get(), b = getDigitalNumber.get();

    Integer[][] DigitalNumbers = {
        {1, 1, 1, 1, 1, 1, 0}, // 0
        {0, 1, 1, 0, 0, 0, 0}, // 1
        {1, 1, 0, 1, 1, 0, 1}, // 2
        {1, 1, 1, 1, 0, 0, 1}, // 3
        {0, 1, 1, 0, 0, 1, 1}, // 4
        {1, 0, 1, 1, 0, 1, 1}, // 5
        {1, 0, 1, 1, 1, 1, 1}, // 6
        {1, 1, 1, 0, 0, 1, 0}, // 7
        {1, 1, 1, 1, 1, 1, 1}, // 8
        {1, 1, 1, 1, 0, 1, 1}  // 9
    };

    Function<IntUnaryOperator, Function<Integer[], Integer[]>> rotate = s -> n ->
        IntStream.rangeClosed(0, 6).map(s).map(i -> n[i]).boxed().toArray(Integer[]::new);

    Function<Integer[], Integer[]>
        normal = rotate.apply(i -> i),
        lineSymmetry = rotate.apply(i -> i == 0 || i == 6 ? i : 6 - i),
        pointSymmetry = rotate.apply(i -> i < 3 ? i + 3 : i == 6 ? i : i - 3);

    Predicate<Integer[]> anyMatchRotateNumbers = n ->
        Arrays.stream(DigitalNumbers).anyMatch(num -> Arrays.equals(n, num));

    Stream.of(normal, lineSymmetry, pointSymmetry)
        .map(f -> anyMatchRotateNumbers.test(f.apply(a)) && anyMatchRotateNumbers.test(f.apply(b)))
        .map(anyMatchBoth -> anyMatchBoth ? "Yes" : "No")
        .forEach(System.out::println);
  }
}
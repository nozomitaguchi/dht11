package p_challenge.b;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _013 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    int a = sc.nextInt(), b = sc.nextInt(), c = sc.nextInt();
    int max = IntStream.range(0, sc.nextInt())
        .map(i -> sc.nextInt() * 60 + sc.nextInt())
        .filter(i -> i + b + c < 9 * 60)
        .max().orElse(0);
    System.out.println((String.format("%02d", (max - a) / 60) + ":" + String.format("%02d", (max - a) % 60)));
  }
}
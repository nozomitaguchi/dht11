package p_challenge.b;

import java.util.Scanner;
import java.util.function.IntSupplier;
import java.util.stream.IntStream;

public class _008 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    IntSupplier profits = () -> (IntStream.range(0, n).map(j -> sc.nextInt()).sum());
    System.out.println(IntStream.range(0, sc.nextInt()).map(i -> profits.getAsInt()).filter(i -> 0 < i).sum());
  }
}
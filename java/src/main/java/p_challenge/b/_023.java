package p_challenge.b;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

public class _023 {
  private static final String SELF = "0", PLUS = "1", MINUS = "2", BLANK = "";

  public static void main(String[] args) throws Exception {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String line = br.readLine();
    char[] nums = line.toCharArray();
    HashSet<String> set = new HashSet<>();
    int length = nums.length;
    HashMap<String, ArrayList<String>> map = getMap();
    for (int i = 0; i < length; i++) {
      String base = String.valueOf(nums[i]);
      ArrayList<String> selfList = map.get(base + SELF);
      if (selfList != null) {
        for (String j : selfList) {
          StringBuilder result = new StringBuilder(line);
          result.replace(i, i + 1, j);
          set.add(result.toString());
        }
      }
      ArrayList<String> minusList = map.get(base + MINUS) == null ? new ArrayList<>() : map.get(base + MINUS);
      for (String j : minusList) {
        for (int k = 0; k < length; k++) {
          if (i == k) {
            continue;
          }
          ArrayList<String> plusList = map.get(nums[k] + PLUS) == null ? new ArrayList<>() : map.get(nums[k] + PLUS);
          for (String p : plusList) {
            StringBuilder result = new StringBuilder(line);
            result.replace(i, i + 1, j);
            result.replace(k, k + 1, p);
            set.add(result.toString());
          }
        }
      }
    }
    if (set.size() == 0) {
      System.out.println("none");
      return;
    }
    for (String s : new TreeSet<>(set)) {
      System.out.println(s);
    }
  }

  public static HashMap<String, ArrayList<String>> getMap() {
    final boolean[][] digitalLights = {
        {true, true, true, true, true, true, false},
        {false, true, true, false, false, false, false},
        {true, true, false, true, true, false, true},
        {true, true, true, true, false, false, true},
        {false, true, true, false, false, true, true},
        {true, false, true, true, false, true, true},
        {true, false, true, true, true, true, true},
        {true, true, true, false, false, false, false},
        {true, true, true, true, true, true, true},
        {true, true, true, true, false, true, true}};
    final int loop = digitalLights.length;
    HashMap<String, ArrayList<String>> map = new HashMap<>();
    for (int i = 0; i < loop; i++) {
      final boolean[] base = digitalLights[i];
      for (int j = 0; j < loop; j++) {
        final boolean[] com = digitalLights[j];
        int comLoop = com.length, p = 0, m = 0;
        for (int k = 0; k < comLoop; k++) {
          if (base[k] != com[k]) {
            if (com[k]) {
              p++;
            } else {
              m++;
            }
          }
        }
        String mode = p == 1 && m == 1 ? SELF : p == 1 && m == 0 ? PLUS : p == 0 && m == 1 ? MINUS : BLANK;
        if (BLANK.equals(mode)) {
          continue;
        }
        String key = i + mode;
        ArrayList<String> list = map.get(key) == null ? new ArrayList<>() : map.get(key);
        list.add(String.valueOf(j));
        map.put(key, list);
      }
    }
    return map;
  }
}

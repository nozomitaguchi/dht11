package p_challenge.b;

import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class _012 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    LongStream.range(0, sc.nextInt()).map(i -> {
      long num = Long.parseLong(sc.next().substring(0, 15));
      AtomicInteger count = new AtomicInteger();
      long sum = IntStream.range(0, 15).mapToLong(j -> {
        long l = num / (long) Math.pow(10, count.get()) % 10;
        return (count.incrementAndGet() % 2 == 0) ? l : (l * 2 < 10) ? l * 2 : (l * 2 / 10 + l * 2 % 10);
      }).sum();
      return (10 - sum % 10) % 10;
    }).forEach(System.out::println);
  }
}
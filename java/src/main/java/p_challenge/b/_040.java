package p_challenge.b;

import java.util.Scanner;

public class _040 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    String T = sc.nextLine().trim(), S = sc.nextLine();
    int[] destinations = getDestinations(n, T.toCharArray());
    System.out.println(n == 0 ? S : decrypt(S.toCharArray(), destinations));
  }

  /**
   * 小文字アルファベットの順番を取得．
   *
   * @param value アルファベット
   * @return aの場合0, bの場合1, ... zの場合25,
   */
  private static int getAlphabetNumber(char value) {
    return Character.getNumericValue(value) - 10;
  }

  /**
   * アルファベット順に移動先が格納された配列を取得する．
   * x[0] == 3 の場合，a の復号化後の文字は d であることを示している．
   *
   * @param count 復号化を行う回数
   * @param T     アルファベット順の復号化後の文字
   * @return アルファベット順に移動先が格納された配列
   */
  private static int[] getDestinations(int count, char[] T) {
    int[] base = new int[26];
    for (int i = 0; i < T.length; i++) {
      base[getAlphabetNumber(T[i])] = i;
    }
    int[] destinations = new int[26];
    for (int i = 0; i < base.length; i++) {
      destinations[i] = getDestination(count, base, base[i]);
    }
    return destinations;
  }

  /**
   * 移動先が格納された配列を取得する．
   *
   * @param count          復号化回数
   * @param base           復号化する基盤になる配列
   * @param DestinationNum 移動先の番号
   * @return 移動先が格納された配列
   */
  private static int getDestination(int count, int[] base, int DestinationNum) {
    if (--count <= 0) return DestinationNum;
    return getDestination(count, base, base[DestinationNum]);
  }

  /**
   * 復号化する．
   *
   * @param S            復号化対象
   * @param destinations アルファベット順に移動先が格納された配列
   * @return 復号化済み文字列
   */
  private static String decrypt(char[] S, int[] destinations) {
    char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
    StringBuilder result = new StringBuilder();
    for (char c : S) {
      int num = getAlphabetNumber(c);
      result.append(num < 0 ? " " : alphabet[destinations[num]]);
    }
    return result.toString();
  }

}
package p_challenge.b;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _014 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    int x = sc.nextInt(), y = sc.nextInt(), z = sc.nextInt();
    Supplier<String> f = () -> {
      String[][] lines = IntStream.range(0, x).mapToObj(i -> sc.next().split("")).toArray(String[][]::new);
      @SuppressWarnings("unused") String sectionLine = sc.next();
      return IntStream.range(0, y).mapToObj(i ->
          IntStream.range(0, x).anyMatch(j -> "#".equals(lines[j][i])) ? "#" : ".").collect(Collectors.joining());
    };
    List<String> blocks = IntStream.range(0, z).mapToObj(i -> f.get()).collect(Collectors.toList());
    Collections.reverse(blocks);
    blocks.forEach(System.out::println);
  }
}
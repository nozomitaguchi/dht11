package p_challenge.b;

import java.util.List;
import java.util.Scanner;
import java.util.function.IntPredicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _010 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    boolean isAOffence = "A".equals(sc.next());
    int direction = isAOffence ? 1 : -1, u = sc.nextInt();
    // 攻めがBなら、ポジションを裏返す。
    Supplier<List<Integer>> positions =
        () -> IntStream.range(0, 11).map(i -> sc.nextInt() * direction).boxed().collect(Collectors.toList());
    List<Integer> a = positions.get(), b = positions.get(), offence = isAOffence ? a : b, defense = isAOffence ? b : a;

    int passer = offence.get(u - 1), defenseLine = defense.stream().sorted().toArray(Integer[]::new)[9];
    IntPredicate isOffside = i -> 55 * direction <= i && passer < i && defenseLine < i;
    List<Integer> offsideMembers = offence.stream().filter(isOffside::test)
        .map(i -> offence.indexOf(i) + 1).sorted().collect(Collectors.toList());
    if (offsideMembers.isEmpty()) System.out.println("None");
    offsideMembers.forEach(System.out::println);
  }
}
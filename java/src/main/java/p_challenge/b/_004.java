package p_challenge.b;

import java.time.temporal.ValueRange;
import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;

public class _004 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Function<String, ValueRange> range = octet -> {
      if (octet.equals("*")) {
        return ValueRange.of(0, 255);
      } else if (octet.startsWith("[")) {
        String[] ar = octet.substring(1, octet.length() - 1).split("-");
        return ValueRange.of(Integer.parseInt(ar[0]), Integer.parseInt(ar[1]));
      }
      return ValueRange.of(Integer.parseInt(octet), Integer.parseInt(octet));
    };
    ValueRange[] ipRanges = Arrays.stream(sc.nextLine().split("\\.")).map(range).toArray(ValueRange[]::new);
    Predicate<String[]> inRange = ip -> IntStream.range(0, 4).allMatch(i -> ipRanges[i].isValidValue(Integer.parseInt(ip[i])));

    IntStream.range(0, Integer.parseInt(sc.nextLine()))
        .mapToObj(i -> sc.nextLine().split(" "))
        .filter(log -> inRange.test(log[0].split("\\.")))
        .map(log -> log[0] + " " + log[3].substring(1) + " " + log[6])
        .forEach(System.out::println);
  }
}
package p_challenge.b;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.IntStream;

public class _021 {
  private static List<Character>
      s_o_x = Arrays.asList('s', 'o', 'x'),
      c_s = Arrays.asList('c', 's'),
      a_i_u_e_o = Arrays.asList('a', 'i', 'u', 'e', 'o');

  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    Function<String, String> f = word -> {
      int length = word.length();
      char[] chars = word.toCharArray();
      char last = chars[length - 1], prev = chars[length - 2];
      if (s_o_x.contains(last) || ('h' == last && c_s.contains(prev))) {
        return word + "es";
      } else if ('f' == last) {
        return word.substring(0, length - 1) + "ves";
      } else if ('f' == prev && 'e' == last) {
        return word.substring(0, length - 2) + "ves";
      } else if (!a_i_u_e_o.contains(prev) && 'y' == last) {
        return word.substring(0, length - 1) + "ies";
      }
      return word + "s";
    };
    IntStream.range(0, sc.nextInt()).mapToObj(i -> f.apply(sc.next())).forEach(System.out::println);
  }
}
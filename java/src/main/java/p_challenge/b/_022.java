package p_challenge.b;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _022 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    int m = sc.nextInt(), n = sc.nextInt(), k = sc.nextInt();
    // 存在しない0を含む
    int[] supporters = IntStream.rangeClosed(0, m).map(i -> 0).toArray();
    IntStream.range(0, k).forEach(i -> {
      int orator = sc.nextInt(), advocate = supporters[orator];
      for (int j = 0; j <= m; j++) {
        if (orator == j || supporters[j] == 0) continue;
        supporters[j] = supporters[j] - 1;
        supporters[orator] = ++advocate;
      }
      // 無関心なやつがいるときは ++
      if (n >= i) supporters[orator] = ++advocate;
    });
    int max = IntStream.of(supporters).max().orElse(0);
    IntStream.rangeClosed(0, m).filter(i -> supporters[i] == max).forEach(System.out::println);
  }
}
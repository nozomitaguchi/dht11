package p_challenge.b;

import java.math.BigDecimal;
import java.util.Scanner;

public class _006 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int oY = sc.nextInt(), s = sc.nextInt(), theta = sc.nextInt(), x = sc.nextInt(), y = sc.nextInt(), a = sc.nextInt();
    double g = 9.8, radian = Math.toRadians(theta), r = a / 2d;
    double h = oY + x * Math.tan(radian) - (g * Math.pow(x, 2)) / (2 * Math.pow(s, 2) * Math.pow(Math.cos(radian), 2));
    double point = Math.abs(y - h);
    System.out.println(point > r ? "Miss" : "Hit " + new BigDecimal(point).setScale(1, BigDecimal.ROUND_HALF_UP));
  }
}
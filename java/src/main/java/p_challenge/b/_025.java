package p_challenge.b;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class _025 {
  private static boolean[] bushes;
  private static int n;

  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    n = sc.nextInt();
    int m = sc.nextInt(), k = sc.nextInt();
    // 茂み( true の場合は、うさぎがいることを表す。)
    bushes = new boolean[n];
    // m番のうざぎがどこにいるか。
    int[] places = new int[m];
    IntStream.range(0, m).forEach(i -> {
      int r = sc.nextInt() - 1;
      bushes[r] = true;
      places[i] = r;
    });
    IntStream.range(0, k).forEach(i -> IntStream.range(0, m).forEach(j -> {
      bushes[places[j]] = false;
      int l = getLandingPoint((places[j] + 1) % n);
      bushes[l] = true;
      places[j] = l;
    }));
    Arrays.stream(places).forEach(i -> System.out.println(i + 1));
  }

  /**
   * ウザギがいない飛び先を取得する。
   *
   * @param point 飛び先候補の番号
   * @return 飛び先
   */
  private static int getLandingPoint(int point) {
    return bushes[point] ? getLandingPoint((point + 1) % n) : point;
  }
}
package p_challenge.b;

import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _016 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    int w = sc.nextInt(), h = sc.nextInt(), n = sc.nextInt(), x = sc.nextInt(), y = sc.nextInt();
    Map<String, Integer> totalDistances = IntStream.range(0, n).boxed()
        .collect(Collectors.groupingBy(i -> sc.next(), Collectors.summingInt(i -> sc.nextInt())));
    int r = totalDistances.getOrDefault("R", 0) % w,
        l = totalDistances.getOrDefault("L", 0) % w - w,
        u = totalDistances.getOrDefault("U", 0) % h,
        d = totalDistances.getOrDefault("D", 0) % h - h;
    System.out.println((x + r - l) % w + " " + (y + u - d) % h);
  }
}

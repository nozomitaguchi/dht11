package p_challenge.b;

import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _019 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt(), k = sc.nextInt(), m = n / k;
    // 予め横方向はsumしておく。
    Supplier<int[]> f = () ->
        IntStream.range(0, m).map(i -> IntStream.range(0, k).map(j -> sc.nextInt()).sum()).toArray();
    int[][] image = IntStream.range(0, n).mapToObj(i -> f.get()).toArray(int[][]::new);
    IntStream.range(0, m).mapToObj(a ->
        IntStream.range(0, m).mapToObj(b ->
            // 縦方向をsumして平均値を取得。
            String.valueOf(IntStream.range(0, k).map(c -> image[a * k + c][b]).sum() / (k * k))
        ).collect(Collectors.joining(" "))
    ).forEach(System.out::println);
  }
}
package p_challenge.b;

import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class _017 {
  public static void main(String[] args) throws Exception {
    String[] cards = new Scanner(System.in).next().split("");
    Map<String, Long> counts = Arrays.stream(cards).collect(Collectors.groupingBy(s -> s, Collectors.counting()));
    boolean isTwoPair = counts.values().stream().allMatch(i -> i == 2) && counts.keySet().stream().noneMatch("*"::equals);
    int level = (int) (counts.values().stream().mapToLong(i -> i - 1).sum() + counts.getOrDefault("*", 0L));
    System.out.println(isTwoPair ? "TwoPair" : new String[]{"NoPair", "OnePair", "ThreeCard", "FourCard"}[level]);
  }
}

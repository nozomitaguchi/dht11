package p_challenge.b;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _026 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    // TODO: 90点しか取れない。
    int[] coinKinds = {500, 100, 50, 10};
    Supplier<Map<Integer, Integer>> getCashes = () -> new LinkedHashMap<Integer, Integer>() {{
      Arrays.stream(coinKinds).forEach(i -> put(i, sc.nextInt()));
    }};

    Predicate<Map<Integer, Integer>> isManyCoins = coins -> 100 <= coins.get(50) * 50 + coins.get(10) * 10;

    BiFunction<Integer, Integer, Integer> sum = (r, l) -> r + l, subtract = (r, l) -> r - l;
    Function<BiFunction<Integer, Integer, Integer>, BinaryOperator<Map<Integer, Integer>>> f =
        howMerge -> (base, set) -> new LinkedHashMap<Integer, Integer>(base) {{
          set.forEach((key, value) -> this.merge(key, value, howMerge));
        }};
    BinaryOperator<Map<Integer, Integer>> mergeCoins = f.apply(sum), subtractCoins = f.apply(subtract);

    ToIntFunction<Map<Integer, Integer>> summing = coins ->
        coins.entrySet().stream().mapToInt(e -> e.getKey() * e.getValue()).sum();

    BiFunction<Map<Integer, Integer>, Integer, Map<Integer, Integer>> getRepayCoins = (coins, repayment) -> {

      Map<Integer, Integer> map = new LinkedHashMap<>();
      for (Map.Entry<Integer, Integer> e : coins.entrySet()) {
        int coinKind = e.getKey();
        int min = Math.min(e.getValue(), repayment / coinKind);
        repayment -= min * coinKind;
        map.put(coinKind, min);
        if (repayment != 0) continue;
        if (isManyCoins.test(map)) return new LinkedHashMap<>();
        return map;
      }
      return new LinkedHashMap<>();
    };

    Map<Integer, Integer> cashes = getCashes.get();
    IntStream.range(0, sc.nextInt()).forEach(i -> {
      int payment = sc.nextInt();
      Map<Integer, Integer> coins = getCashes.get();
      int repayment = summing.applyAsInt(coins) - payment;
      Map<Integer, Integer> repayCoins = getRepayCoins.apply(cashes, repayment);
      if (repayCoins.isEmpty()) {
        System.out.println("impossible");
      } else {
        System.out.println(repayCoins.values().stream().map(String::valueOf).collect(Collectors.joining(" ")));
        Map<Integer, Integer> map = mergeCoins.apply(coins, subtractCoins.apply(cashes, repayCoins));
        cashes.clear();
        cashes.putAll(map);
      }
    });
  }
}
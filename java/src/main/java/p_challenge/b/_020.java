package p_challenge.b;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class _020 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    // TODO: ダサいのでリファクタする。
    List<String> pages = new ArrayList<>();
    AtomicInteger count = new AtomicInteger();
    IntStream.range(0, Integer.parseInt(sc.nextLine()))
        .mapToObj(i -> sc.nextLine().replaceFirst("go to ", ""))
        .map(page -> {
          if ("use the back button".equals(page)) return pages.get(count.decrementAndGet() - 1);
          pages.add(count.getAndIncrement(), page);
          return page;
        })
        .forEach(System.out::println);
  }
}
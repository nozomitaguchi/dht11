package p_challenge.b;

import java.util.Scanner;

public class _011 {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    int onePageNum = sc.nextInt() * 2,
        m = sc.nextInt(),
        page = m / onePageNum + (m % onePageNum == 0 ? 0 : 1),
        pocket = m % onePageNum == 0 ? onePageNum : m % onePageNum;
    System.out.println(page * onePageNum + 1 - pocket);
  }
}
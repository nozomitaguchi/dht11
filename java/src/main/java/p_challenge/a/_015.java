package p_challenge.a;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class _015 {
  public static void main(String[] args) throws Exception {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    char[][] firstState = new char[8][8];
    for (int i = 0; i < 8; i++) {
      firstState[i] = br.readLine().toCharArray();
    }

    char[][][][] parts = new char[4][4][4][4];
    for (int i = 0; i < 4; i++) {
      char[][] baseBlock = new char[4][4];
      for (int j = 0; j < 4; j++) {
        baseBlock[j] = br.readLine().toCharArray();
      }
      parts[i] = getRotateBlocks(baseBlock);
    }

    List<ArrayList<ArrayList<Integer[]>>> patternsList = new ArrayList<>();
    for (char[][][] blocks : parts) {
      ArrayList<ArrayList<Integer[]>> patterns = new ArrayList<>();
      for (char[][] block : blocks) {
        patterns.add(getPattern(block));
      }
      patternsList.add(patterns);
    }

    ArrayList<char[][]> states = new ArrayList<>();
    states.add(firstState);

    for (int c = 0; c < 4; c++) {
      ArrayList<ArrayList<Integer[]>> patterns = patternsList.get(c);
      ArrayList<char[][]> nextStates = new ArrayList<>();
      for (char[][] state : states) {
        for (int y = 0; y < 8; y++) {
          for (int x = 0; x < 8; x++) {
            if (state[y][x] != '#') {
              continue;
            }
            for (ArrayList<Integer[]> pattern : patterns) {
              boolean isMatch = true;
              for (Integer[] p : pattern) {
                int Y = y + p[0], X = x + p[1];
                if ((Y < 0 || 8 <= Y || X < 0 || 8 <= X) || state[Y][X] != '#') {
                  isMatch = false;
                }
              }
              if (isMatch) {
                if (c == 3) {
                  System.out.println("Yes");
                  return;
                }
                char[][] nextState = new char[state.length][];
                for (int i = 0; i < state.length; i++) {
                  nextState[i] = state[i].clone();
                }
                for (Integer[] p : pattern) {
                  int Y = y + p[0], X = x + p[1];
                  nextState[Y][X] = '.';
                }
                nextStates.add(nextState);
              }
            }
          }
        }
      }
      states = nextStates;
    }
    System.out.println("No");
  }

  private static ArrayList<Integer[]> getPattern(char[][] block) {
    ArrayList<Integer[]> list = new ArrayList<>();
    int Y = -1, X = -1;
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++) {
        if (block[i][j] != '#') {
          continue;
        }
        if (Y == -1) {
          Y = i;
          X = j;
        }
        block[i][j] = '.';
        list.add(new Integer[]{i - Y, j - X});
      }
    }
    return list;
  }

  private static char[][][] getRotateBlocks(char[][] baseBlock) {
    char[][][] blocks = new char[4][4][4];
    blocks[0] = baseBlock;
    for (int i = 0; i < 3; i++) {
      blocks[i + 1] = rotate(blocks[i]);
    }
    return blocks;
  }

  private static char[][] rotate(char[][] block) {
    char[][] rotatedBlock = new char[4][4];
    for (int y = 0; y < 4; y++) {
      for (int x = 0; x < 4; x++) {
        rotatedBlock[3 - x][y] = block[y][x];
      }
    }
    return rotatedBlock;
  }
}
package p_challenge.a;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class _006 {
  private static final int[][] direction = {{0, 1}, {-1, 0}, {0, -1}, {1, 0}};
  private static boolean[][] feild = new boolean[500][500];
  private static int turn = 0, step = 0;

  public static void main(String[] args) throws Exception {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    final int N = Integer.parseInt(br.readLine());
    ArrayList<int[]> points = new ArrayList<>();
    for (int i = 0; i < N; i++) {
      final String[] x_y = br.readLine().split(" ");
      final int y = Integer.parseInt(x_y[1]) + 250, x = Integer.parseInt(x_y[0]) + 250;
      points.add(new int[]{y, x});
      feild[y][x] = true;
    }
    advanceTheTime(points);
    System.out.println(step);
  }

  private static void advanceTheTime(ArrayList<int[]> points) {
    if (points.size() == 0)
      return;
    final int[] motion = direction[turn % 4];
    final int straightStep = turn / 2 + 1;
    for (int i = 0; i < straightStep; i++) {
      ArrayList<int[]> next = new ArrayList<>();
      for (int[] point : points) {
        final int y = point[0] + motion[0], x = point[1] + motion[1];
        if (feild[y][x])
          continue;
        feild[y][x] = true;
        next.add(new int[]{y, x});
      }
      if (next.size() == 0)
        return;
      points = next;
      step++;
    }
    turn++;
    advanceTheTime(points);
  }
}
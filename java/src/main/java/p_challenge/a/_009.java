package p_challenge.a;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class _009 {
  public static void main(String[] args) throws Exception {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    final String[] H_W = br.readLine().split(" ");
    final int H = Integer.parseInt(H_W[0]), W = Integer.parseInt(H_W[1]);
    char[][] box = new char[H][W];
    for (int i = 0; i < H; i++) {
      box[i] = br.readLine().toCharArray();
    }
    final char SLASH = '/', BQ = '\\';
    final int R = 0, T = 1, L = 2, B = 3;
    int a = 0, x = -1, y = 0, count = 0;
    while (true) {
      switch (a) {
        case R:
          x++;
          break;
        case T:
          y--;
          break;
        case L:
          x--;
          break;
        case B:
          y++;
          break;
      }
      if (x < 0 || W <= x || y < 0 || H <= y) {
        break;
      }
      count++;
      char point = box[y][x];
      if (SLASH == point) {
        a = a == R ? T : a == T ? R : a == L ? B : L;
      } else if (BQ == point) {
        a = a == B ? R : a == R ? B : a == T ? L : T;
      }
    }
    System.out.println(count);
  }
}
package p_challenge.a;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class _003 {
  private static int[][] squares = new int[8][8];
  private static int p, e, x, y;

  public static void main(String[] args) throws Exception {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    final int N = Integer.parseInt(br.readLine());
    squares[3][3] = 2;
    squares[4][3] = 1;
    squares[3][4] = 1;
    squares[4][4] = 2;
    for (int i = 0; i < N; i++) {
      final String[] p_x_y = br.readLine().split(" ");
      p = "B".equals(p_x_y[0]) ? 1 : 2;
      e = p == 1 ? 2 : 1;
      x = Integer.parseInt(p_x_y[1]) - 1;
      y = Integer.parseInt(p_x_y[2]) - 1;
      squares[y][x] = p;
      int[] loop = {-1, 0, 1};
      for (int xs : loop) {
        for (int ys : loop) {
          if (xs == 0 && ys == 0) {
            continue;
          }
          int count = getReverseNum(xs, ys, 1);
          for (int j = 1; j <= count; j++) {
            squares[y + ys * j][x + xs * j] = p;
          }
        }
      }
    }
    int B = 0, W = 0;
    for (int[] line : squares) {
      for (int square : line) {
        if (square == 1) {
          B++;
        } else if (square == 2) {
          W++;
        }
      }
    }
    String winner = B == W ? "Draw!" : B < W ? "The white won!" : "The black won!";
    System.out.println(String.format("%02d", B) + "-" + String.format("%02d", W) + " " + winner);
  }

  private static int getReverseNum(int xs, int ys, int count) {
    int X = x + xs * count, Y = y + ys * count;
    if (X < 0 || 8 <= X || Y < 0 || 8 <= Y) {
      return 0;
    }
    int square = squares[Y][X];
    if (e == square) {
      return getReverseNum(xs, ys, ++count);
    } else if (p == square) {
      return count - 1;
    }
    return 0;
  }
}
package p_challenge.a;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class _005 {
  public static void main(String[] args) throws Exception {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String[] a_b_n = br.readLine().split(" ");
    final int NOMAL = 0, SPARE = 1, STRIKE = 2, RESTRIKE = 3;
    int a = Integer.parseInt(a_b_n[0]), b = Integer.parseInt(a_b_n[1]), n = Integer.parseInt(a_b_n[2]), status = 0, roundCount = 0;
    boolean is1st = true;
    List<Integer> pitches = Arrays.stream(br.readLine().replaceAll("G", "0").split(" ")).map(Integer::parseInt).collect(Collectors.toList());
    int[] score = new int[a + 2];
    for (int i = 0; i < n; i++) {
      int pitch = pitches.get(i);
      score[roundCount] += pitch;
      if (status == RESTRIKE) {
        score[roundCount - 2] += pitch;
        status = STRIKE;
      }
      if (status == STRIKE || (is1st && status == SPARE)) {
        score[roundCount - 1] += pitch;
      }
      if (is1st && b != score[roundCount]) {
        is1st = false;
        continue;
      }
      if (is1st && b == score[roundCount]) {
        status = status >= STRIKE ? RESTRIKE : STRIKE;
      } else if (b == score[roundCount]) {
        status = SPARE;
      } else {
        status = NOMAL;
      }
      roundCount++;
      is1st = true;
    }
    System.out.println(Arrays.stream(score).sum());
  }
}
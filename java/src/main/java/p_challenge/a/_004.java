package p_challenge.a;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.TreeMap;

public class _004 {
  public static void main(String[] args) throws Exception {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    final String[] l_n_m = br.readLine().split(" ");
    final int l = Integer.parseInt(l_n_m[0]), n = Integer.parseInt(l_n_m[1]), m = Integer.parseInt(l_n_m[2]);
    TreeMap<Integer, TreeMap<Integer, Integer>> map = new TreeMap<>();
    for (int i = 0; i < m; i++) {
      final String[] a_b_c = br.readLine().split(" ");
      final int a = Integer.parseInt(a_b_c[0]), b = Integer.parseInt(a_b_c[1]), c = Integer.parseInt(a_b_c[2]);
      TreeMap<Integer, Integer> right = map.get(a) != null ? map.get(a) : new TreeMap<>();
      right.put(b, c);
      map.put(a, right);
      TreeMap<Integer, Integer> left = map.get(a + 1) != null ? map.get(a + 1) : new TreeMap<>();
      left.put(c, b * -1);
      map.put(a + 1, left);
    }
    int p = 1;
    int count = l;
    while (true) {
      if (count <= 0) {
        break;
      }
      TreeMap<Integer, Integer> jumpMap = map.get(p);
      if (jumpMap == null) {
        break;
      }
      Integer toI = jumpMap.get(count);
      if (toI == null) {
        count--;
        continue;
      }
      p = toI < 0 ? p - 1 : p + 1;
      count = Math.abs(toI) - 1;
    }
    System.out.println(p);
  }
}
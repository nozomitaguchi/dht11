package p_challenge.d;

import java.math.BigDecimal;
import java.util.Scanner;
import java.util.stream.IntStream;

public class _069 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    double average = IntStream.range(0, 7).mapToDouble(i -> sc.nextDouble()).sum() / 7.0;
    BigDecimal roundedAverage = new BigDecimal(average).setScale(1, BigDecimal.ROUND_HALF_UP);
    System.out.println(roundedAverage);
  }
}
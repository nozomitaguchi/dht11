package p_challenge.d;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _004 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String names = IntStream.range(0, sc.nextInt())
        .mapToObj(i -> sc.next())
        .collect(Collectors.joining(","));
    System.out.println("Hello " + names + ".");
  }
}
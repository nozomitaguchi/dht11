package p_challenge.d;

import java.util.Arrays;
import java.util.Scanner;

public class _051 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long count = Arrays.stream(sc.nextLine().split(" ")).filter("W"::equals).count();
    System.out.println(5 <= count ? "OK" : "NG");
  }
}
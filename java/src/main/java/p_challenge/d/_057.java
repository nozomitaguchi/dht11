package p_challenge.d;

import java.util.Scanner;

public class _057 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = Integer.parseInt(sc.nextLine());
    String[] presents = sc.nextLine().split(" ");
    System.out.println(presents[n - 1]);
  }
}
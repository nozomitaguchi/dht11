package p_challenge.d;

import java.util.Scanner;

public class _076 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String w = sc.next(), s = sc.next();
    System.out.println(s.contains(w) ? "NG" : s);
  }
}
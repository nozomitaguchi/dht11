package p_challenge.d;

import java.util.Scanner;

public class _002 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int a = sc.nextInt(), b = sc.nextInt();
    System.out.println(a == b ? "eq" : Math.max(a, b));
  }
}
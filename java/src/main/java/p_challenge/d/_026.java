package p_challenge.d;

import java.util.Scanner;
import java.util.stream.Stream;

public class _026 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long count = Stream.generate(sc::next).limit(7).filter("no"::equals).count();
    System.out.println(count);
  }
}
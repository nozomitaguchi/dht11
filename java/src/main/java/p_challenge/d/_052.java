package p_challenge.d;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _052 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(IntStream.rangeClosed(1, sc.nextInt()).sum());
  }
}
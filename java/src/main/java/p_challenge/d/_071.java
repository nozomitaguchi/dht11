package p_challenge.d;

import java.util.Scanner;

public class _071 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    boolean isHighTemperature = 25 <= sc.nextInt(), isLowHumidity = sc.nextInt() <= 40,
        isDayForLaundry = (isHighTemperature || isLowHumidity) && !(isHighTemperature && isLowHumidity);
    System.out.println(isDayForLaundry ? "Yes" : "No");
  }
}
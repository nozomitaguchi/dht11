package p_challenge.d;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _078 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int total = IntStream.generate(sc::nextInt).limit(7).sum();
    System.out.println(sc.nextInt() * 7 <= total ? "pass" : "failure");
  }
}
package p_challenge.d;

import java.util.Scanner;

public class _056 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    double x = Math.pow(sc.nextDouble(), 3) - Math.pow(sc.nextDouble(), 3);
    System.out.println((int) x);
  }
}
package p_challenge.d;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _005 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int m = sc.nextInt(), n = sc.nextInt();
    String collect = IntStream.range(0, 10)
        .mapToObj(i -> String.valueOf(m + n * i))
        .collect(Collectors.joining(" "));
    System.out.println(collect);
  }
}
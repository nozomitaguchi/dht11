package p_challenge.d;

import java.util.Scanner;

public class _054 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int length = sc.nextLine().length();
    System.out.println(length >= 11 ? "OK" : 11 - length);
  }
}
package p_challenge.d;

import java.util.Scanner;
import java.util.stream.Stream;

public class _091 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Stream<Integer> pollens = Stream.generate(sc::nextInt).limit(10);
    System.out.println(pollens.filter(i -> i <= 2).count());
  }
}
package p_challenge.d;

import java.util.Scanner;

public class _039 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int a = sc.nextInt(), b = sc.nextInt(), c = sc.nextInt();
    System.out.println(a == b && b == c ? "YES" : "NO");
  }
}
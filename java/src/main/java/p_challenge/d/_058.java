package p_challenge.d;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class _058 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(Stream.of("A", "B", "A").flatMap(s -> Stream.generate(() -> s).limit(sc.nextInt())).collect(Collectors.joining()));
  }
}
package p_challenge.d;

import java.util.Arrays;
import java.util.Scanner;

public class _017 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int[] n = {sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()};
    System.out.println(Arrays.stream(n).max().orElse(0));
    System.out.println(Arrays.stream(n).min().orElse(0));
  }
}
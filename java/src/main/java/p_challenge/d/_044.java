package p_challenge.d;

import java.util.Scanner;

public class _044 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String name = sc.next();
    System.out.println("Hi, " + ("F".equals(sc.next()) ? "Ms. " : "Mr. ") + name);
  }
}
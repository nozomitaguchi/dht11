package p_challenge.d;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _040 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(IntStream.range(0, 7).filter(i -> sc.nextInt() <= 30).count());
  }
}
package p_challenge.d;

import java.util.Scanner;

public class _008 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(sc.nextInt() % 2 == 0 ? "even" : "odd");
  }
}
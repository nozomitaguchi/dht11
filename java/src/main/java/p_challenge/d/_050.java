package p_challenge.d;

import java.util.Scanner;
import java.util.function.IntFunction;

public class _050 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    IntFunction<Integer> f = i -> (5 < i) ? 5 : i;
    System.out.println(f.apply(sc.nextInt()) + f.apply(sc.nextInt()));
  }
}
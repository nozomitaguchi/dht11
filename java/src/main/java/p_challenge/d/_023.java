package p_challenge.d;

import java.util.Arrays;
import java.util.Scanner;

public class _023 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long count = Arrays.stream(sc.next().split("")).filter("A"::equals).count();
    System.out.println(count);
  }
}
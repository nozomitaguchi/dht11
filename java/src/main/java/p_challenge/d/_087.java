package p_challenge.d;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class _087 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Stream<String> chars = Stream.generate(sc::next).limit(sc.nextInt());
    System.out.println(chars.collect(Collectors.joining()));
  }
}
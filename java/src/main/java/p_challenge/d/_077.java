package p_challenge.d;

import java.util.Scanner;

public class _077 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int i = sc.nextInt() * sc.nextInt();
    System.out.println(i < 10000 ? i : "NG");
  }
}
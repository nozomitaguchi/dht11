package p_challenge.d;

import java.util.Scanner;
import java.util.stream.Stream;

public class _075 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int sum = Stream.generate(sc::nextInt).limit(4).mapToInt(i -> i).sum();
    System.out.println(1 + 2 + 3 + 4 + 5 - sum);
  }
}
package p_challenge.d;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _048 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    IntStream.of(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()).reduce((i, n) -> {
      System.out.println(n - i);
      return n;
    });
  }
}
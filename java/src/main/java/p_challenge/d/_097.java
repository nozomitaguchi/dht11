package p_challenge.d;

import java.util.Scanner;
import java.util.stream.Stream;

public class _097 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long wetDays = Stream.generate(sc::nextInt).limit(7).filter(i -> i == 1).count();
    System.out.println(5 <= wetDays ? "yes" : "no");
  }
}
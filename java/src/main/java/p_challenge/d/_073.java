package p_challenge.d;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class _073 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    List<String> strList = Arrays.asList(sc.next().split(""));
    Collections.reverse(strList);
    System.out.println(strList.stream().collect(Collectors.joining()));
  }
}
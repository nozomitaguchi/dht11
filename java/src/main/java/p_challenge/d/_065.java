package p_challenge.d;

import java.util.Scanner;

public class _065 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int head = sc.nextInt() / 100;
    System.out.println(head == 2 ? "ok" : head == 4 ? "error" : "unknown");
  }
}
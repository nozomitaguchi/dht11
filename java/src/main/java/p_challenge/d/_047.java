package p_challenge.d;

import java.util.Scanner;
import java.util.stream.Stream;

public class _047 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Stream.of("Gold", "Silver", "Bronze").forEach(medal -> System.out.println(medal + " " + sc.next()));
  }
}
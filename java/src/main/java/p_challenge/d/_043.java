package p_challenge.d;

import java.util.Scanner;

public class _043 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int i = sc.nextInt();
    System.out.println(i <= 30 ? "sunny" : 71 <= i ? "rainy" : "cloudy");
  }
}
package p_challenge.d;

import java.util.Scanner;

public class _037 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    double m = sc.nextDouble(), n = sc.nextDouble();
    System.out.println((int) Math.ceil(n / m));
  }
}
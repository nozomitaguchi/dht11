package p_challenge.d;

import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class _062 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Integer[] ar = Stream.iterate(0, i -> i + sc.nextInt()).limit(4).toArray(Integer[]::new);
    IntStream.range(1, 4).forEach(i -> System.out.println("ABCDEFGHIJ".substring(ar[i - 1], ar[i])));
  }
}
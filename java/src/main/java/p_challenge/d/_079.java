package p_challenge.d;

import java.util.Arrays;
import java.util.Scanner;

public class _079 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String[] chars = sc.next().split("");
    boolean allMatch = Arrays.stream(chars).allMatch(c -> c.equals(chars[0]));
    System.out.println(allMatch ? "NG" : "OK");
  }
}
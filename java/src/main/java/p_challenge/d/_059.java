package p_challenge.d;

import java.util.Scanner;

public class _059 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String line = sc.nextLine();
    System.out.println(line.equals("J J") ? "J Q" : line);
  }
}
package p_challenge.d;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class _007 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(Stream.generate(() -> "*").limit(sc.nextInt()).collect(Collectors.joining()));
  }
}
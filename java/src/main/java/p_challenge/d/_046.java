package p_challenge.d;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _046 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println(IntStream.of(sc.nextInt(), sc.nextInt(), sc.nextInt()).max().orElse(0));
  }
}
package p_challenge.d;

import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Predicate;

public class _053 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Predicate<String> isFavorite = (String sweets) -> Arrays.asList("candy", "chocolate").contains(sweets);
    System.out.println(isFavorite.test(sc.next()) ? "Thanks!" : "No!");
  }
}
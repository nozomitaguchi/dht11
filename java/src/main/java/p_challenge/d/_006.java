package p_challenge.d;

import java.util.Scanner;

public class _006 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long n = sc.nextLong();
    String s = sc.next();
    System.out.println(n * ("km".equals(s) ? 1000000 : "m".equals(s) ? 1000 : 10));
  }
}
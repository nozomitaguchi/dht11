package p_challenge.d;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _015 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    IntStream.range(0, n).forEach(i -> System.out.println(n - i));
  }
}
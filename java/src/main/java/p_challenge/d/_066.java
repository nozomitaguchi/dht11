package p_challenge.d;

import java.util.Scanner;

public class _066 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int m = sc.nextInt(), n = sc.nextInt();
    System.out.println(m > n ? "No" : n - m);
  }
}
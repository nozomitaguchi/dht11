package p_challenge.d;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class _003 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    String collect = IntStream.rangeClosed(1, 9)
        .mapToObj(i -> String.valueOf(n * i))
        .collect(Collectors.joining(" "));
    System.out.println(collect);
  }
}
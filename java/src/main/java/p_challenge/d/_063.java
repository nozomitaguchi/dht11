package p_challenge.d;

import java.util.Arrays;
import java.util.Scanner;

public class _063 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String[] t = sc.nextLine().split(" ");
    int a = sc.nextInt();
    long count = Arrays.stream(t)
        .mapToInt(Integer::parseInt)
        .filter(t_n -> t_n < a)
        .count();
    System.out.println(count + 1);
  }
}
package p_challenge.d;

import java.util.Scanner;

public class _034 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int i = 21 % sc.nextInt();
    System.out.println(i == 0 ? 3 : i);
  }
}
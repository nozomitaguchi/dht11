package p_challenge.d;

import java.util.Arrays;
import java.util.Scanner;

public class _068 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    long sunDayCount = Arrays.stream(sc.next().split("")).filter("S"::equals).count();
    System.out.println(sunDayCount + " " + (n - sunDayCount));
  }
}
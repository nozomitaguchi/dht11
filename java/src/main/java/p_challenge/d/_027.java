package p_challenge.d;

import java.util.Scanner;
import java.util.stream.IntStream;

public class _027 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int sum = IntStream.rangeClosed(1, sc.nextInt()).reduce((n, m) -> n + m).orElse(0);
    System.out.println(sum);
  }
}
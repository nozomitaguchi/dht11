package p_challenge.s;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class _007 {
  public static void main(String[] args) throws Exception {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    char[] line = br.readLine().toCharArray();
    List<Character> numberList = Arrays.asList('1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
    List<Character> alphabetList = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    Map<Character, Long> countMap = new HashMap<>();
    for (Character a : alphabetList) {
      countMap.put(a, 0L);
    }
    Map<Long, Long> magnifications = new HashMap<>();
    magnifications.put(0L, 1L);
    long layer = 0L;
    StringBuilder num = new StringBuilder();
    for (char c : line) {
      if (alphabetList.contains(c)) {
        long magnification = "".equals(num.toString()) ? 1L : Long.parseLong(num.toString());
        num = new StringBuilder();
        magnifications.put(layer, magnification);
        long disitNum = 1L;
        for (long i = 0; i <= layer; i++) {
          disitNum *= magnifications.get(i);
        }
        countMap.put(c, countMap.get(c) + disitNum);
        magnifications.put(layer, 1L);
      } else if (numberList.contains(c)) {
        num.append(c);
      } else if ('(' == c) {
        long magnification = "".equals(num.toString()) ? 1L : Long.parseLong(num.toString());
        num = new StringBuilder();
        magnifications.put(layer++, magnification);
      } else if (')' == c) {
        magnifications.put(--layer, 1L);
      }
    }
    Map<Character, Long> sortedCountMap = new TreeMap<>(countMap);
    for (Entry<Character, Long> r : sortedCountMap.entrySet()) {
      System.out.println(r.getKey() + " " + r.getValue());
    }
  }
}

package p_challenge.s;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class _002 {
  public static void main(String[] args) throws Exception {

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String[] mapInfo = br.readLine().split(" ");

    //経路情報の作成 スタートは0 道とゴールは-1 障害物は-2
    int height = Integer.parseInt(mapInfo[0]);
    int width = Integer.parseInt(mapInfo[1]);
    int[][] mapArray = new int[width][height];

    int[][] start = new int[1][2];
    int[][] goal = new int[1][2];

    for (int i = 0; i < width; i++) {
      mapInfo = br.readLine().split(" ");
      for (int j = 0; j < height; j++) {
        switch (mapInfo[j]) {
          case "s":
            mapInfo[j] = "0";
            start[0][0] = i;
            start[0][1] = j;
            break;
          case "g":
            mapInfo[j] = "-1";
            goal[0][0] = i;
            goal[0][1] = j;
            break;
          case "0":
            mapInfo[j] = "-1";
            break;
          case "1":
            mapInfo[j] = "-2";
            break;
        }

        mapArray[i][j] = Integer.parseInt(mapInfo[j]);
      }
    }

    //経路の重みづけ
    mapArray = getMap(mapArray, start);

    if (mapArray[goal[0][0]][goal[0][1]] == -1) {
      System.out.println("Fail");
    } else {
      System.out.println(mapArray[goal[0][0]][goal[0][1]]);
    }

  }

  private static int[][] getMap(int[][] mapArray, int[][] start) {

    int historyNum = 0;

    List
        <int[]> historyList = new ArrayList<>();
    int[] tmpInt = new int[2];
    tmpInt[0] = start[0][0];
    tmpInt[1] = start[0][1];
    historyList.add(tmpInt);

    List<int[]> tmpList = new ArrayList<>();

    while (true) {

      for (int i = historyNum; i < historyList.size(); i++) {
        int[] tmp = historyList.get(i);
        directionSearch(mapArray, tmp[0], tmp[1], tmpList);
      }

      if (tmpList.size() == 0) {
        return mapArray;
      }

      historyNum = historyList.size();

      historyList.addAll(tmpList);
      tmpList = new ArrayList<>();

    }
  }

  private static void directionSearch(int[][] mapArray, int i, int j, List<int[]> tmpList) {

    //↑方向の検索
    try {
      if (mapArray[i - 1][j] != -2 && mapArray[i - 1][j] == -1) {
        mapArray[i - 1][j] = mapArray[i][j] + 1;
        int[] tmpInt = new int[2];
        tmpInt[0] = i - 1;
        tmpInt[1] = j;
        tmpList.add(tmpInt);
      }
    } catch (ArrayIndexOutOfBoundsException ignored) {
    }

    //→方向の検索
    try {
      if (mapArray[i][j + 1] != -2 && mapArray[i][j + 1] == -1) {
        mapArray[i][j + 1] = mapArray[i][j] + 1;
        int[] tmpInt = new int[2];
        tmpInt[0] = i;
        tmpInt[1] = j + 1;
        tmpList.add(tmpInt);
      }
    } catch (ArrayIndexOutOfBoundsException ignored) {
    }

    //↓方向の検索
    try {
      if (mapArray[i + 1][j] != -2 && mapArray[i + 1][j] == -1) {
        mapArray[i + 1][j] = mapArray[i][j] + 1;
        int[] tmpInt = new int[2];
        tmpInt[0] = i + 1;
        tmpInt[1] = j;
        tmpList.add(tmpInt);
      }
    } catch (ArrayIndexOutOfBoundsException ignored) {
    }

    //←方向の検索
    try {
      if (mapArray[i][j - 1] != -2 && mapArray[i][j - 1] == -1) {
        mapArray[i][j - 1] = mapArray[i][j] + 1;
        int[] tmpInt = new int[2];
        tmpInt[0] = i;
        tmpInt[1] = j - 1;
        tmpList.add(tmpInt);
      }
    } catch (ArrayIndexOutOfBoundsException ignored) {
    }

  }
}

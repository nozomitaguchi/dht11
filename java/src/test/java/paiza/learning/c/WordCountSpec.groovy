package paiza.learning.c

import spock.lang.Unroll
import utils.BaseSpecification

class WordCountSpec extends BaseSpecification {

  @Unroll('WordCount : #testCase')
  def 'WordCount'() {
    given:
      setIn(input)
    when:
      WordCount.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          'red green blue blue green blue',
          'Nagato Yukikaze Akagi Kitakami Nagato Akagi Akagi Kitakami']
      expected << [
          ['red 1', 'green 2', 'blue 3'],
          ['Nagato 2', 'Yukikaze 1', 'Akagi 3', 'Kitakami 2']]
  }

}

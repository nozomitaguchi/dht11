package paiza.learning.c

import spock.lang.Unroll
import utils.BaseSpecification

class FizzBuzzSpec extends BaseSpecification {

  @Unroll('WordCount : #testCase')
  def 'WordCount'() {
    given:
      setIn(input)
    when:
      FizzBuzz.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << ['5', '20']
      expected << [
          [1, 2, 'Fizz', 4, 'Buzz'],
          [1, 2, 'Fizz', 4, 'Buzz', 'Fizz', 7, 8, 'Fizz', 'Buzz', 11, 'Fizz', 13, 14, 'Fizz Buzz', 16, 17, 'Fizz', 19, 'Buzz']]
  }

}

package paiza.learning.b


import spock.lang.Unroll
import utils.BaseSpecification

class LongTableSpec extends BaseSpecification {
  @Unroll('WordCount : #testCase')
  def 'WordCount'() {
    given:
      setIn(input)
    when:
      LongTable.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          '6 3\n3 2\n1 6\n2 5',
          '12 6\n4 6\n4 8\n4 10\n4 12\n4 2\n4 4']
      expected << [[4], [12]]
  }
}

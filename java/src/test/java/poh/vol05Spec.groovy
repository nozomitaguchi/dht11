package poh

import utils.BaseSpecification
import poh.vol05._0001
import poh.vol05._0002
import poh.vol05._minami
import poh.vol05._rena
import spock.lang.Unroll

class vol05Spec extends BaseSpecification {
  @Unroll('vol05_0001 : #testCase')
  def 'vol05_0001'() {
    given:
    setIn(input)
    when:
    _0001.main()
    then:
    results == expected
    where:
    testCase | input             || expected
    'input1' | 'PXaeiTzVap'      || ['Paiza']
    'input2' | 'abcdefg hijklmn' || ['aceghjln']
  }

  @Unroll('vol05_0002 : #testCase')
  def 'vol05_0002'() {
    given:
    setIn(input)
    when:
    _0002.main()
    then:
    results == expected
    where:
    testCase | input                                                                                          || expected
    'input1' | '14\n30000\n20000\n0\n50000\n0\n100000\n500000\n30000\n20000\n0\n20000\n15000\n600000\n450000' || [60000, 40000, 0, 70000, 15000, 700000, 950000]
    'input2' | '7\n1003\n2302\n421\n32124\n3\n0\n3214'                                                        || [1003, 2302, 421, 32124, 3, 0, 3214]
  }

  @Unroll('vol05_minami : #testCase')
  def 'vol05_minami'() {
    given:
    setIn(input)
    when:
    _minami.main()
    then:
    results == expected
    where:
    testCase | input                                    || expected
    'input1' | '3 4\n1 0 2\n1 2 1\n1 2 2\n1 2 2'        || ['1 0 0', '1 0 0', '1 0 0', '1 0 1']
    'input2' | '3 5\n0 0 0\n2 1 0\n1 1 1\n1 2 1\n2 1 2' || ['0 0 0', '0 0 0', '0 1 0', '1 1 1', '1 1 1']
  }

  @Unroll('vol05_rena : #testCase')
  def 'vol05_rena'() {
    given:
    setIn(input)
    when:
    _rena.main()
    then:
    results == expected
    where:
    testCase | input                                                                                                                                                                                                                                                              || expected
    'input1' | '4 5 3\n948 608 920 216\n3 413 306 7\n312 173 0 1000\n365 726 280 358\n26 539 197 753\n2 1 3 3\n3 3 4 4\n1 4 4 5 '                                                                                                                                                 || [6664]
    'input2' | '10 5 5\n119 677 334 254 884 256 588 445 682 206\n691 171 875 407 926 225 313 381 811 637\n291 452 658 153 335 657 4 189 408 386\n97 750 918 118 334 191 635 341 669 594\n51 239 413 394 667 113 558 288 757 788\n3 2 6 3\n9 4 10 4\n10 2 10 5\n5 1 6 4\n2 2 10 5' || [17895]
  }
}

package poh

import utils.BaseSpecification
import poh.vol04._0001
import poh.vol04._0002
import poh.vol04._0003
import spock.lang.Unroll

class vol04Spec extends BaseSpecification {

  @Unroll('vol04_0001 : #testCase')
  def 'vol04_0001'() {
    given:
      setIn(input)
    when:
      _0001.main()
    then:
      results == expected
    where:
      testCase | input            || expected
      'input1' | '4\n5\n10\n2\n3' || [20]
      'input2' | '3\n100\n23\n12' || [135]
  }

  @Unroll('vol04_0002 : #testCase')
  def 'vol04_0002'() {
    given:
      setIn(input)
    when:
      _0002.main()
    then:
      results == expected
    where:
      testCase | input                                     || expected
      'input1' | '4\n10 5 100\n4 0 300\n2 2 200\n2 3 100'  || [1700]
      'input2' | '3\n100 99 2034\n300 233 303\n20 12 1022' || [30511]
  }

  @Unroll('vol04_0003 : #testCase')
  def 'vol04_0003'() {
    given:
      setIn(input)
    when:
      _0003.main()
    then:
      results == expected
    where:
      testCase | input                                                   || expected
      'input1' | '3 7\n4\n5\n1\n10\n3\n4\n1'                             || [17]
      'input2' | '6 12\n0\n123\n222\n21\n1\n332\n22\n99\n3\n444\n24\n10' || [924]
  }
}

package poh

import utils.BaseSpecification
import poh.vol01._0001
import spock.lang.Unroll

class vol01Spec extends BaseSpecification {

  @Unroll('vol01_0001 : #testCase')
  def 'vol01_0001'() {
    given:
      setIn(input)
    when:
      _0001.main()
    then:
      results == expected
    where:
      testCase | input                                             || expected
      'input1' | '4 3\n8000\n4000\n9000\n6000\n3000\n14000\n10000' || [0, 14000, 10000]
      'input2' | '5 2\n4000\n3000\n1000\n2000\n5000\n10000\n3000'  || [9000, 3000]
      'input3' | '4 1\n1000\n3000\n7000\n8000\n10000'              || [10000]
  }
}

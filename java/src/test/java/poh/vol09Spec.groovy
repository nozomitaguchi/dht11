package poh

import utils.BaseSpecification
import poh.vol09.*
import spock.lang.Unroll

class vol09Spec extends BaseSpecification {
  
  @Unroll('vol09_0001 : #testCase')
  def 'vol09_0001'() {
    given:
      setIn(input)
    when:
      _0001.main()
    then:
      results == expected
    where:
      testCase | input        || expected
      'input1' | '10\n25\n1'  || [36]
      'input2' | '1\n99\n100' || [200]
  }

  @Unroll('vol09_0003 : #testCase')
  def 'vol09_0003'() {
    given:
      setIn(input)
    when:
      _0003.main()
    then:
      results == expected
    where:
      testCase | input      || expected
      'input1' | 'helpme'   || ['SOS']
      'input2' | 'thankyou' || ['thankyou']
  }

  @Unroll('vol09_0004 : #testCase')
  def 'vol09_0004'() {
    given:
      setIn(input)
    when:
      _0004.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '9 9 +' || [18]
      'input2' | '4 3 -' || [1]
  }

  @Unroll('vol09_0005 : #testCase')
  def 'vol09_0005'() {
    given:
      setIn(input)
    when:
      _0005.main()
    then:
      results == expected
    where:
      testCase | input                               || expected
      'input1' | '8 1 4 7 2 3 9 5 6 0\nencode\n257'  || ['435']
      'input2' | '8 3 7 1 2 5 6 0 9 4\ndecode\n0728' || ['7240']
  }

  @Unroll('vol09_0006 : #testCase')
  def 'vol09_0006'() {
    given:
      setIn(input)
    when:
      _0006.main()
    then:
      results == expected
    where:
      testCase | input                     || expected
      'input1' | '3\n1 2 3\n4 6 8'         || ['5 6 7', '7 8 9', '9 10 11']
      'input2' | '5\n8 7 6 5 4\n1 2 3 4 5' || ['9 8 7 6 5', '10 9 8 7 6', '11 10 9 8 7', '12 11 10 9 8', '13 12 11 10 9']
  }

  @Unroll('vol09_0007 : #testCase')
  def 'vol09_0007'() {
    given:
      setIn(input)
    when:
      _0007.main()
    then:
      results == expected
    where:
      testCase | input                                                                      || expected
      'input1' | '2\n20 5 10\n45 6 40\n2\n1 6\n2 12'                                        || [110, 460]
      'input2' | '4\n30 2 10\n55 5 40\n100 1 2\n25 10 25\n6\n1 1\n2 1\n3 3\n1 3\n2 9\n4 52' || [30, 55, 294, 80, 455, 1175]
  }

  @Unroll('vol09_0008 : #testCase')
  def 'vol09_0008'() {
    given:
      setIn(input)
    when:
      _0008.main()
    then:
      results == expected
    where:
      testCase | input                                                  || expected
      'input1' | '/home/paiza ../test'                                  || ['/home/test']
      'input2' | '/ ..'                                                 || ['/']
      'input3' | '/root/paiza io/../../paiza/io/../../../../home/paiza' || ['/home/paiza']
  }

  @Unroll('vol09_0009 : #testCase')
  def 'vol09_0009'() {
    given:
      setIn(input)
    when:
      _0009.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '247'   || [300]
      'input2' | '31'    || [31]
      'input3' | '54445' || [100000]
  }
}

package poh

import utils.BaseSpecification
import poh.vol06._kirishima
import poh.vol06._rio
import poh.vol06._tsubame
import spock.lang.Unroll

class vol06Spec extends BaseSpecification {

  @Unroll('vol06_kirishima : #testCase')
  def 'vol06_kirishima'() {
    given:
      setIn(input)
    when:
      _kirishima.main()
    then:
      results == expected
    where:
      testCase | input                                         || expected
      'input1' | '8\n0 6 -2 -2 0 1 -1 0\n6\n7\n3\n4\n2\n5\n10' || ['Yes', 'Yes', 'No', 'No', 'No', 'No']
      'input2' | '5\n0 1 0 1 0\n4\n1\n1\n2\n3'                 || ['No', 'No', 'No', 'Yes']
  }

  @Unroll('vol06_rio : #testCase')
  def 'vol06_rio'() {
    given:
      setIn(input)
    when:
      _rio.main()
    then:
      results == expected
    where:
      testCase | input                       || expected
      'input1' | '2\n1 90\n2 10'             || [10]
      'input2' | '4\n1 50\n2 55\n3 15\n1 20' || [42]
  }

  @Unroll('vol06_tsubame : #testCase')
  def 'vol06_tsubame'() {
    given:
      setIn(input)
    when:
      _tsubame.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '27'  || [36]
      'input2' | '97'  || [113]
  }

}

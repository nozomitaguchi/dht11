package poh

import utils.BaseSpecification
import poh.vol08.*
import spock.lang.Unroll

class vol08Spec extends BaseSpecification {

  @Unroll('vol08_hair2 : #testCase')
  def 'vol08_hair2'() {
    given:
      setIn(input)
    when:
      _hair2.main()
    then:
      results == expected
    where:
      testCase | input       || expected
      'input1' | '3\nidol'   || ['idol', 'idol', 'idol']
      'input2' | '10\ndream' || ['dream', 'dream', 'dream', 'dream', 'dream', 'dream', 'dream', 'dream', 'dream', 'dream']
  }

  @Unroll('vol08_hair3 : #testCase')
  def 'vol08_hair3'() {
    given:
      setIn(input)
    when:
      _hair3.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '7'   || ['lucky']
      'input2' | '12'  || ['unlucky']
  }

  @Unroll('vol08_hair4 : #testCase')
  def 'vol08_hair4'() {
    given:
      setIn(input)
    when:
      _hair4.main()
    then:
      results == expected
    where:
      testCase | input                     || expected
      'input1' | 'U U\nD D\nL L\nR R\nL L' || ['OK']
      'input2' | 'U D\nD L\nR L\nD U\nU U' || ['NG']
  }

  @Unroll('vol08_hair6 : #testCase')
  def 'vol08_hair6'() {
    given:
      setIn(input)
    when:
      _hair6.main()
    then:
      results == expected
    where:
      testCase | input                                                || expected
      'input1' | '50\n9\n543\n318\n265\n422\n468\n573\n141\n443\n421' || [7]
      'input2' | '60\n8\n449\n163\n517\n312\n170\n355\n161\n238'      || ['OK']
  }

  @Unroll('vol08_hair5 : #testCase')
  def 'vol08_hair5'() {
    given:
      setIn(input)
    when:
      _hair5.main()
    then:
      results == expected
    where:
      testCase | input    || expected
      'input1' | '10\n3'  || ['--+-------']
      'input2' | '20\n20' || ['-------------------+']
  }

  @Unroll('vol08_eye2 : #testCase')
  def 'vol08_eye2'() {
    given:
      setIn(input)
    when:
      _eye2.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '5 2' || ['OK']
      'input2' | '3 4' || ['NG']
  }

  @Unroll('vol08_eye3 : #testCase')
  def 'vol08_eye3'() {
    given:
      setIn(input)
    when:
      _eye3.main()
    then:
      results == expected
    where:
      testCase | input  || expected
      'input1' | '850'  || [8]
      'input2' | '1230' || [22]
  }

  @Unroll('vol08_eye4 : #testCase')
  def 'vol08_eye4'() {
    given:
      setIn(input)
    when:
      _eye4.main()
    then:
      results == expected
    where:
      testCase | input                     || expected
      'input1' | '5\n2 3 5 4 1'            || [3]
      'input2' | '7\n27 100 83 2 57 71 40' || [57]
  }

  @Unroll('vol08_clothes2 : #testCase')
  def 'vol08_clothes2'() {
    given:
      setIn(input)
    when:
      _clothes2.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '3 6' || ['ok']
      'input2' | '2 7' || ['ng']
  }

  @Unroll('vol08_clothes3 : #testCase')
  def 'vol08_clothes3'() {
    given:
      setIn(input)
    when:
      _clothes3.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '5 2'   || [3]
      'input2' | '10 10' || [0]
  }

  @Unroll('vol08_clothes5 : #testCase')
  def 'vol08_clothes5'() {
    given:
      setIn(input)
    when:
      _clothes5.main()
    then:
      results == expected
    where:
      testCase | input                                                                                                         || expected
      'input1' | 'K 6 J 5 3 2 A 4 5 10 K 4 J 3 10 6 9 9 3 7 Q K Q 4 10 2 5 J 4 Q 8 9 A 8 9 3 K 7 8 5 7 8 6 10 Q 6 A 2 J 7 A 2' || [1, 13, 14, 47, 48, 2, 3, 49, 50, 18, 15, 51, 19, 52, 29, 30, 31, 42, 43, 44, 20, 21, 22, 45, 32, 4, 5, 6, 33, 7, 34, 35, 8, 36, 37, 38, 23, 24, 25, 39, 40, 41, 46, 26, 27, 28, 16, 9, 10, 17, 11, 12]
      'input2' | '6 9 5 4 5 7 7 8 2 2 4 J 5 4 K 2 K 3 10 10 Q A 9 A J 9 4 Q K A 3 2 K 10 8 8 7 3 A 6 Q 7 Q 3 J 8 5 9 6 J 6 10' || [1, 2, 31, 47, 48, 32, 41, 33, 3, 4, 5, 6, 49, 50, 7, 8, 9, 51, 34, 35, 26, 10, 27, 16, 17, 36, 37, 18, 19, 20, 21, 11, 12, 22, 38, 42, 43, 52, 13, 14, 15, 46, 23, 24, 25, 44, 45, 39, 40, 28, 29, 30]
  }

  @Unroll('vol08_clothes6 : #testCase')
  def 'vol08_clothes6'() {
    given:
      setIn(input)
    when:
      _clothes6.main()
    then:
      results == expected
    where:
      testCase | input                                                                      || expected
      'input1' | '4\n5 out\n12 in\n14 out\n21 in'                                           || [38]
      'input2' | '10\n2 out\n3 in\n7 out\n8 in\n13 out\n17 in\n18 out\n19 in\n20 in\n21 in' || [46]
  }

  @Unroll('vol08_special2 : #testCase')
  def 'vol08_special2'() {
    given:
      setIn(input)
    when:
      _special2.main()
    then:
      results == expected
    where:
      testCase | input               || expected
      'input1' | '5 5\npizza\npaiza' || [1]
      'input2' | '3 5\nant\nmaven'   || [3]
  }

  @Unroll('vol08_special5 : #testCase')
  def 'vol08_special5'() {
    given:
      setIn(input)
    when:
      _special5.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '2\n9'  || [3]
      'input2' | '1\n15' || [8]
  }

  @Unroll('vol08_special6 : #testCase')
  def 'vol08_special6'() {
    given:
      setIn(input)
    when:
      _special6.main()
    then:
      results == expected
    where:
      testCase | input               || expected
      'input1' | '100 110\n20 200'   || [12000]
      'input2' | '980 200\n200 1000' || [201000]
  }
}

package poh

import utils.BaseSpecification
import poh.vol10.*
import spock.lang.Unroll

class vol10Spec extends BaseSpecification {

  @Unroll('vol10_1002 : #testCase')
  def 'vol10_1002'() {
    given:
      setIn(input)
    when:
      _1002.main()
    then:
      results == expected
    where:
      testCase | input       || expected
      'input1' | '5\nLLRLR'  || ['4 2 1 3 5']
      'input2' | '6\nLRLRLR' || ['5 3 1 2 4 6']
  }

  @Unroll('vol10_1003 : #testCase')
  def 'vol10_1003'() {
    given:
      setIn(input)
    when:
      _1003.main()
    then:
      results == expected
    where:
      testCase | input           || expected
      'input1' | 'logicsummoner' || ['logicsummoner']
      'input2' | '123paiza'      || ['123paiza']
  }

  @Unroll('vol10_1004 : #testCase')
  def 'vol10_1004'() {
    given:
      setIn(input)
    when:
      _1004.main()
    then:
      results == expected
    where:
      testCase | input                       || expected
      'input1' | 'paiza\npaiza'              || ['OK']
      'input2' | 'pepepepepepepe\nmoxyomoto' || ['NG']
  }

  @Unroll('vol10_2001 : #testCase')
  def 'vol10_2001'() {
    given:
      setIn(input)
    when:
      _2001.main()
    then:
      results == expected
    where:
      testCase | input      || expected
      'input1' | 'bbbwwwww' || ['3 5']
      'input2' | 'wwwwwbbb' || ['0 5 3']
  }

  @Unroll('vol10_2002 : #testCase')
  def 'vol10_2002'() {
    given:
      setIn(input)
    when:
      _2002.main()
    then:
      results == expected
    where:
      testCase | input                                                                    || expected
      'input1' | '3\n6 1 8\n7 5 3\n2 0 0'                                                 || ['6 1 8', '7 5 3', '2 9 4']
      'input2' | '5\n1 15 8 24 0\n19 3 21 12 10\n13 0 20 6 4\n25 9 2 18 11\n7 16 14 5 23' || ['1 15 8 24 17', '19 3 21 12 10', '13 22 20 6 4', '25 9 2 18 11', '7 16 14 5 23']
  }

  @Unroll('vol10_3001 : #testCase')
  def 'vol10_3001'() {
    given:
      setIn(input)
    when:
      _3001.main()
    then:
      results == expected
    where:
      testCase | input                                      || expected
      'input1' | 'Attack\nAttack\nDefense\nDefense\nAttack' || [300]
      'input2' | 'Attack\nAttack\nAttack\nAttack\nAttack'   || [500]
  }

  @Unroll('vol10_3002 : #testCase')
  def 'vol10_3002'() {
    given:
      setIn(input)
    when:
      _3002.main()
    then:
      results == expected
    where:
      testCase | input                                                   || expected
      'input1' | '4 5000 3000 4000\n-1000 -1500 500 -5000'               || [6000]
      'input2' | '10 50 20 30\n-10 -10 -10 100 -10 -10 -10 -20 -100 -10' || [50]
  }

  @Unroll('vol10_3003 : #testCase')
  def 'vol10_3003'() {
    given:
      setIn(input)
    when:
      _3003.main()
    then:
      results == expected
    where:
      testCase | input                               || expected
      'input1' | '011100110111\n10'                  || [2]
      'input2' | '11110011010111011101111111010\n20' || ['No']
  }

  @Unroll('vol10_4001 : #testCase')
  def 'vol10_4001'() {
    given:
      setIn(input)
    when:
      _4001.main()
    then:
      results == expected
    where:
      testCase | input                || expected
      'input1' | '100 1000\n120 1100' || [1100]
      'input2' | '1 10\n200 500'      || [500]
  }

  @Unroll('vol10_4002 : #testCase')
  def 'vol10_4002'() {
    given:
      setIn(input)
    when:
      _4002.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '250 4' || [1000]
      'input2' | '124 3' || [372]
  }

  @Unroll('vol10_4003 : #testCase')
  def 'vol10_4003'() {
    given:
      setIn(input)
    when:
      _4003.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '49'  || [99]
      'input2' | '80'  || [100]
  }

  @Unroll('vol10_4004 : #testCase')
  def 'vol10_4004'() {
    given:
      setIn(input)
    when:
      _4004.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          '4\n' +
              '0 paiza\n' +
              '0 onlin\n' +
              '0 ehack\n' +
              '0 athon',
          '8\n' +
              '0 thequickbr\n' +
              '0 ownfoxjump\n' +
              '0 soverthela\n' +
              '0 zydog\n' +
              '1 jackdawslo\n' +
              '1 vemybigsph\n' +
              '1 inxofquart\n' +
              '1 z']
      expected << [['0 paizaonlinehackathon'],
                   ['0 thequickbrownfoxjumpsoverthelazydog', '1 jackdawslovemybigsphinxofquartz']]
  }
}

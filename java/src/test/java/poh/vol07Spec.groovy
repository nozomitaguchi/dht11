package poh

import utils.BaseSpecification
import poh.vol07.*
import spock.lang.Unroll

class vol07Spec extends BaseSpecification {

  @Unroll('vol07_0002 : #testCase')
  def 'vol07_0002'() {
    given:
      setIn(input)
    when:
      _0002.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '3'   || ['AnnAnnAnn']
      'input2' | '10'  || ['AnnAnnAnnAnnAnnAnnAnnAnnAnnAnn']
  }

  @Unroll('vol07_0004 : #testCase')
  def 'vol07_0004'() {
    given:
      setIn(input)
    when:
      _0004.main()
    then:
      results == expected
    where:
      testCase | input     || expected
      'input1' | '9\n23'   || [32]
      'input2' | '99\n100' || [199]
  }

  @Unroll('vol07_0005 : #testCase')
  def 'vol07_0005'() {
    given:
      setIn(input)
    when:
      _0005.main()
    then:
      results == expected
    where:
      testCase | input                   || expected
      'input1' | 'yes\nyes\nno\nyes\nno' || ['yes']
      'input2' | 'no\nyes\nno\nno\nyes'  || ['no']
  }

  @Unroll('vol07_0006 : #testCase')
  def 'vol07_0006'() {
    given:
      setIn(input)
    when:
      _0006.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '5'   || ['5', '4', '3', '2', '1', '0!!']
      'input2' | '10'  || ['10', '9', '8', '7', '6', '5', '4', '3', '2', '1', '0!!']
  }

  @Unroll('vol07_0007 : #testCase')
  def 'vol07_0007'() {
    given:
      setIn(input)
    when:
      _0007.main()
    then:
      results == expected
    where:
      testCase | input              || expected
      'input1' | '200 250\n180 200' || [2]
      'input2' | '250 200\n120 150' || [1]
  }

  @Unroll('vol07_0009 : #testCase')
  def 'vol07_0009'() {
    given:
      setIn(input)
    when:
      _0009.main()
    then:
      results == expected
    where:
      testCase | input                         || expected
      'input1' | '3\npaiza\nonline\nhackathon' || ['paiza_online_hackathon']
      'input2' | '2\nis\nAndroid'              || ['is_Android']
  }

  @Unroll('vol07_0010 : #testCase')
  def 'vol07_0010'() {
    given:
      setIn(input)
    when:
      _0010.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '4'   || [24]
      'input2' | '3'   || [6]
  }

  @Unroll('vol07_0011 : #testCase')
  def 'vol07_0011'() {
    given:
      setIn(input)
    when:
      _0011.main()
    then:
      results == expected
    where:
      testCase | input                                                           || expected
      'input1' | '4\n0 0 1 0\n0 1 1 0\n0 1 0 1\n1 1 1 0\n3\n0 1 1\n0 1 0\n1 1 1' || ['1 0']
      'input2' | '4\n0 0 0 0\n0 0 1 1\n0 0 1 1\n0 0 0 0\n2\n1 1\n1 1'            || ['1 2']
  }


  @Unroll('vol07_0012 : #testCase')
  def 'vol07_0012'() {
    given:
      setIn(input)
    when:
      _0012.main()
    then:
      results == expected
    where:
      testCase | input                                    || expected
      'input1' | '10 10 10 2\n0 3\n0 8'                   || [200]
      'input2' | '20 40 10 5\n1 34\n1 17\n0 7\n1 6\n0 11' || [240]
  }


  @Unroll('vol07_0013 : #testCase')
  def 'vol07_0013'() {
    given:
      setIn(input)
    when:
      _0013.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '15'  || [307674368]
      'input2' | '10'  || [36288]
  }

  @Unroll('vol07_0016 : #testCase')
  def 'vol07_0016'() {
    given:
      setIn(input)
    when:
      _0016.main()
    then:
      results == expected
    where:
      testCase | input                       || expected
      'input1' | '5\n3\n1 3 4\n3\n2 3 5'     || ['2 5']
      'input2' | '8\n5\n1 3 4 5 6\n3\n1 5 6' || ['None']
  }

  @Unroll('vol07_0017 : #testCase')
  def 'vol07_0017'() {
    given:
      setIn(input)
    when:
      _0017.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '3\n10' || ['RRRWWWRRRW']
      'input2' | '5\n4'  || ['RRRR']
  }

  @Unroll('vol07_0018 : #testCase')
  def 'vol07_0018'() {
    given:
      setIn(input)
    when:
      _0018.main()
    then:
      results == expected
    where:
      testCase | input        || expected
      'input1' | 'catcatcat'  || [3]
      'input2' | 'acatbccatd' || [2]
  }

  @Unroll('vol07_0019 : #testCase')
  def 'vol07_0019'() {
    given:
      setIn(input)
    when:
      _0019.main()
    then:
      results == expected
    where:
      testCase | input    || expected
      'input1' | 'ctacct' || [1, 0, 2, 1]
      'input2' | 'cccc'   || [0, 0, 4, 4]
  }

  @Unroll('vol07_0020 : #testCase')
  def 'vol07_0020'() {
    given:
      setIn(input)
    when:
      _0020.main()
    then:
      results == expected
    where:
      testCase | input        || expected
      'input1' | '2\n30\n210' || ['00:50', '23:50']
      'input2' | '2\n0\n177'  || ['01:00', '00:01']
  }
}

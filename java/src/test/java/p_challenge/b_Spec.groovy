package p_challenge

import p_challenge.b.*
import spock.lang.Unroll
import utils.BaseSpecification

class b_Spec extends BaseSpecification {

  @Unroll('_004 : #testCase')
  def '_004'() {
    given:
    setIn(input)
    when:
    _004.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2']
    input << [
        '192.168.186.70\n' +
            '3\n' +
            '192.168.110.238 - - [10/Jul/2013:18:40:43 +0900] "GET /top.html HTTP/1.1" 404 8922 "http://gi-no.jp" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36"\n' +
            '192.168.186.70 - - [10/Jul/2013:18:52:12 +0900] "GET /top.html HTTP/1.1" 404 3628 "http://facebook.com" "Mozilla/5.0 (Windows NT 5.1; rv:22.0) Gecko/20100101 Firefox/22.0"\n' +
            '192.168.105.56 - - [10/Jul/2013:20:13:52 +0900] "GET /top.html HTTP/1.1" 200 1863 "http://paiza.jp" "Mozilla/5.0 (iPad; CPU OS 6_0_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A523 Safari/8536.25"',
        '192.168.[0-100].*\n' +
            '3\n' +
            '192.168.99.112 - - [10/Jul/2013:13:53:15 +0900] "GET /top.html HTTP/1.1" 404 1426 "http://facebook.com" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36"\n' +
            '192.168.81.20 - - [10/Jul/2013:15:06:33 +0900] "GET /hogehoge.html HTTP/1.1" 404 4374 "http://paiza.jp" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36"\n' +
            '192.168.223.58 - - [10/Jul/2013:21:32:01 +0900] "GET /hoge.html HTTP/1.1" 304 6601 "http://paiza.jp" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:22.0) Gecko/20100101 Firefox/22.0"'
    ]
    expected << [['192.168.186.70 10/Jul/2013:18:52:12 /top.html'], ['192.168.99.112 10/Jul/2013:13:53:15 /top.html', '192.168.81.20 10/Jul/2013:15:06:33 /hogehoge.html']]
  }

  @Unroll('_006 : #testCase')
  def '_006'() {
    given:
    setIn(input)
    when:
    _006.main()
    then:
    results == expected
    where:
    testCase | input                || expected
    'input1' | '10 10 10\n10 10 10' || ['Hit 3.3']
    'input2' | '10 15 45\n10 10 10' || ['Miss']
  }

  @Unroll('_008 : #testCase')
  def '_008'() {
    given:
    setIn(input)
    when:
    _008.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2', 'input3']
    input << [
        '3 5\n' +
            '3 0 -2\n' +
            '1 5 1\n' +
            '-4 1 0\n' +
            '-1 -2 3\n' +
            '0 0 2',
        '4 7\n' +
            '12 0 -19 -12\n' +
            '-7 -5 7 -17\n' +
            '7 -17 -10 10\n' +
            '-17 -4 -6 -12\n' +
            '-4 -9 -7 8\n' +
            '2 -15 -7 -7\n' +
            '0 -15 1 10',
        '5 30\n' +
            '-70 -35 19 90 -8\n' +
            '-69 4 -54 5 -50\n' +
            '63 -81 -17 16 -28\n' +
            '93 -33 -21 78 11\n' +
            '6 -92 -90 -39 29\n' +
            '-61 -38 85 -56 47\n' +
            '-62 -74 61 4 -57\n' +
            '7 -58 19 96 99\n' +
            '89 -24 -78 -75 -6\n' +
            '97 11 -68 68 71\n' +
            '72 8 83 42 55\n' +
            '50 -6 11 59 -36\n' +
            '52 -97 -45 1 -69\n' +
            '14 9 -80 -78 38\n' +
            '-54 -33 78 -28 -6\n' +
            '15 91 67 20 -20\n' +
            '9 58 -16 95 -88\n' +
            '59 45 -11 59 84\n' +
            '99 -43 77 79 -37\n' +
            '-58 -80 -77 38 -91\n' +
            '-49 12 -47 -67 31\n' +
            '-20 62 -70 34 38\n' +
            '-90 -46 5 91 -63\n' +
            '5 79 80 -44 -61\n' +
            '60 68 29 -65 -36\n' +
            '-38 -83 -40 43 96\n' +
            '-20 -98 -71 6 85\n' +
            '95 15 36 -82 41\n' +
            '-74 -99 26 96 -70\n' +
            '-83 94 51 -36 -32'
    ]
    expected << [[10], [0], [1714]]
  }

  @Unroll('_009 : #testCase')
  def '_009'() {
    given:
    setIn(input)
    when:
    _009.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2', 'input3']
    input << [
        '3\n' +
            'jobs 45\n' +
            'gates 60\n' +
            'larry 15',
        '4\n' +
            'r 51\n' +
            'yshwgfdci 12\n' +
            'elqbndnbye 54\n' +
            'lg 47',
        '2\n' +
            'vkdjh 21\n' +
            'yoboqmetph 37'
    ]
    expected << [['10:00 - 10:45 jobs',
                  '10:55 - 11:55 gates',
                  '12:55 - 13:10 larry'],
                 ['10:00 - 10:51 r',
                  '11:01 - 11:13 yshwgfdci',
                  '12:13 - 13:07 elqbndnbye',
                  '13:17 - 14:04 lg'],
                 ['10:00 - 10:21 vkdjh',
                  '10:31 - 11:08 yoboqmetph']]
  }

  @Unroll('_010 : #testCase')
  def '_010'() {
    given:
    setIn(input)
    when:
    _010.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2', 'input3', 'input4', 'input5']
    input << [
        'A 3\n' +
            '18 44 69 31 90 72 48 29 55 37 78\n' +
            '103 85 39 55 51 8 80 37 21 65 54',
        'A 3\n' +
            '18 41 63 30 84 95 67 29 71 48 91\n' +
            '96 77 40 67 49 75 76 31 19 60 47',
        'B 7\n' +
            '86 36 55 88 10 82 53 107 83 22 15\n' +
            '69 38 48 73 21 50 27 1 41 24 103',
        'B 10\n' +
            '69 41 96 89 53 42 83 95 48 4 25\n' +
            '100 71 90 59 86 97 84 85 79 81 98',
        'A 3\n' +
            '18 44 69 31 70 72 48 29 55 37 78\n' +
            '103 85 39 55 51 8 80 37 21 65 54'
    ]
    expected << [[5], [5, 6, 11], [8], ['None'], ['None']]
  }

  @Unroll('_011 : #testCase')
  def '_011'() {
    given:
    setIn(input)
    when:
    _011.main()
    then:
    results == expected
    where:
    testCase | input      || expected
    'input1' | '3 1'      || [6]
    'input2' | '3 6'      || [1]
    'input3' | '157 1518' || [1309]
  }

  @Unroll('_012 : #testCase')
  def '_012'() {
    given:
    setIn(input)
    when:
    _012.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2', 'input3']
    input << [
        '1\n' +
            '846087729128569X',
        '4\n' +
            '628381026148991X\n' +
            '511070105176715X\n' +
            '273492510450813X\n' +
            '670891979616350X',
        '5\n' +
            '091180422478189X\n' +
            '774123801013511X\n' +
            '973736969204716X\n' +
            '793180803472918X\n' +
            '358682935182058X'
    ]
    expected << [[7], [5, 9, 7, 2], [1, 4, 0, 1, 2]]
  }

  @Unroll('_013 : #testCase')
  def '_013'() {
    given:
    setIn(input)
    when:
    _013.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2', 'input3']
    input << [
        '30 30 10\n' +
            '3\n' +
            '6 0\n' +
            '7 0\n' +
            '8 0',
        '25 30 30\n' +
            '2\n' +
            '7 20\n' +
            '8 0',
        '10 10 10\n' +
            '6\n' +
            '8 5\n' +
            '8 15\n' +
            '8 25\n' +
            '8 35\n' +
            '8 45\n' +
            '8 55'
    ]
    expected << [['07:30'], ['06:55'], ['08:25']]
  }

  @Unroll('_014 : #testCase')
  def '_014'() {
    given:
    setIn(input)
    when:
    _014.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2']
    input << [
        '3 3 3\n' +
            '###\n' +
            '##.\n' +
            '#..\n' +
            '--\n' +
            '##.\n' +
            '##.\n' +
            '...\n' +
            '--\n' +
            '##.\n' +
            '##.\n' +
            '...\n' +
            '--',
        '5 5 5\n' +
            '#####\n' +
            '#####\n' +
            '#####\n' +
            '#####\n' +
            '#####\n' +
            '--\n' +
            '#####\n' +
            '#...#\n' +
            '#...#\n' +
            '#...#\n' +
            '#####\n' +
            '--\n' +
            '#####\n' +
            '#...#\n' +
            '#...#\n' +
            '#...#\n' +
            '#####\n' +
            '--\n' +
            '#####\n' +
            '#...#\n' +
            '#...#\n' +
            '#...#\n' +
            '#####\n' +
            '--\n' +
            '#####\n' +
            '#####\n' +
            '#####\n' +
            '#####\n' +
            '#####\n' +
            '--'
    ]
    expected << [['##.',
                  '##.',
                  '###'],
                 ['#####',
                  '#####',
                  '#####',
                  '#####',
                  '#####']]
  }

  @Unroll('_015 : #testCase')
  def '_015'() {
    given:
    setIn(input)
    when:
    _015.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2']
    input << [
        '1 1 1 1 0 1 1\n' +
            '0 1 1 0 0 0 0',
        '0 0 0 0 1 1 0\n' +
            '1 1 0 1 1 1 1']
    expected << [['Yes', 'No', 'No'], ['No', 'Yes', 'No']]
  }

  @Unroll('_016 : #testCase')
  def '_016'() {
    given:
    setIn(input)
    when:
    _016.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2']
    input << [
        '7 6 1\n' +
            '3 4\n' +
            'U 3',
        '7 6 1\n' +
            '3 4\n' +
            'L 5']
    expected << [['3 1'], ['5 4']]
  }

  @Unroll('_017 : #testCase')
  def '_017'() {
    given:
    setIn(input)
    when:
    _017.main()
    then:
    results == expected
    where:
    testCase | input  || expected
    'input1' | 'ABCD' || ['NoPair']
    'input2' | 'ABAB' || ['TwoPair']
    'input3' | '*ZZD' || ['ThreeCard']
  }

  @Unroll('_019 : #testCase')
  def '_019'() {
    given:
    setIn(input)
    when:
    _019.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2']
    input << [
        '6 3\n' +
            '1 2 3 4 5 6\n' +
            '1 2 3 4 5 6\n' +
            '1 2 3 4 5 6\n' +
            '4 5 6 1 2 3\n' +
            '4 5 6 1 2 3\n' +
            '4 5 6 1 2 3',
        '6 2\n' +
            '76 251 15 224 89 129\n' +
            '90 129 102 161 14 92\n' +
            '78 180 218 236 47 25\n' +
            '96 126 138 37 59 202\n' +
            '43 213 30 105 29 195\n' +
            '132 19 14 166 106 16'
    ]
    expected << [['2 5',
                  '5 2'],
                 ['136 125 81',
                  '120 157 83',
                  '101 78 86']]
  }

  @Unroll('_020 : #testCase')
  def '_020'() {
    given:
    setIn(input)
    when:
    _020.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2', 'input3']
    input << [
        '5\n' +
            'go to blank page\n' +
            'go to bja n\n' +
            'go to va\n' +
            'use the back button\n' +
            'use the back button',
        '7\n' +
            'go to blank page\n' +
            'go to nkah\n' +
            'use the back button\n' +
            'go to gi\n' +
            'go to in\n' +
            'go to nkah\n' +
            'use the back button',
        '5\n' +
            'go to blank page\n' +
            'go to paiza\n' +
            'go to paiza\n' +
            'use the back button\n' +
            'use the back button'
    ]
    expected << [['blank page',
                  'bja n',
                  'va',
                  'bja n',
                  'blank page'],
                 ['blank page',
                  'nkah',
                  'blank page',
                  'gi',
                  'in',
                  'nkah',
                  'in'],
                 ['blank page',
                  'paiza',
                  'paiza',
                  'paiza',
                  'blank page']]
  }

  @Unroll('_021 : #testCase')
  def '_021'() {
    given:
    setIn(input)
    when:
    _021.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2', 'input3']
    input << [
        '3\n' +
            'dog\n' +
            'cat\n' +
            'pig',
        '7\n' +
            'box\n' +
            'photo\n' +
            'axis\n' +
            'dish\n' +
            'church\n' +
            'leaf\n' +
            'knife',
        '2\n' +
            'study\n' +
            'play'
    ]
    expected << [['dogs',
                  'cats',
                  'pigs'],
                 ['boxes',
                  'photoes',
                  'axises',
                  'dishes',
                  'churches',
                  'leaves',
                  'knives'],
                 ['studies',
                  'plays']]
  }

  @Unroll('_022 : #testCase')
  def '_022'() {
    given:
    setIn(input)
    when:
    _022.main()
    then:
    results == expected
    where:
    testCase | input                 || expected
    'input1' | '3 3 4\n1\n1\n2\n3'   || [3]
    'input2' | '2 100 4\n2\n2\n2\n1' || [1, 2]
  }

  @Unroll('_023 : #testCase')
  def '_023'() {
    given:
    setIn(input)
    when:
    _023.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2', 'input3']
    input << ['16', '01234567', '888']
    expected << [['10',
                  '19',
                  '75'],
                 ['01224567',
                  '01234367',
                  '01234507',
                  '01234581',
                  '01234597',
                  '01234657',
                  '01234661',
                  '01234957',
                  '01234961',
                  '01254567',
                  '01294557',
                  '01294561',
                  '01334567',
                  '07234557',
                  '07234561',
                  '61234567',
                  '81234557',
                  '81234561',
                  '91234567'],
                 ['none']]
  }

  @Unroll('_024 : #testCase')
  def '_024'() {
    given:
    setIn(input)
    when:
    _024.main()
    then:
    results == expected
    where:
    testCase | input       || expected
    'input1' | '1'         || [4]
    'input2' | '1.7'       || [16]
    'input3' | '100.02651' || [31852]
  }

  @Unroll('_025 : #testCase')
  def '_025'() {
    given:
    setIn(input)
    when:
    _025.main()
    then:
    results == expected
    where:
    testCase | input            || expected
    'input1' | '5 2 2\n1\n2'    || [5, 1]
    'input2' | '6 3 6\n1\n3\n5' || [1, 3, 5]
  }

  @Unroll('_026 : #testCase')
  def '_026'() {
    given:
    setIn(input)
    when:
    _026.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2']
    input << [
        '1 4 1 20\n' +
            '3\n' +
            '130 1 0 0 0\n' +
            '150 0 2 0 0\n' +
            '100 1 0 0 0',
        '5 7 8 9\n' +
            '4\n' +
            '110 0 2 0 0\n' +
            '120 1 0 0 0\n' +
            '130 1 0 0 0\n' +
            '180 0 2 0 2']
    expected << [['0 3 1 2',
                  '0 0 0 5',
                  'impossible'],
                 ['0 0 1 4',
                  '0 3 1 3',
                  '0 3 1 2',
                  'impossible']]
  }

  @Unroll('_027 : #testCase')
  def '_027'() {
    given:
    setIn(input)
    when:
    _027.main()
    then:
    results == expected
    where:
    testCase << ['input1', 'input2']
    input << [
        '2 2 2\n' +
            '1 2\n' +
            '2 1\n' +
            '4\n' +
            '1 1 2 1\n' +
            '1 1 1 2\n' +
            '1 1 2 2\n' +
            '1 2 2 1',
        '2 5 3\n' +
            '5 8 8 6 3\n' +
            '3 6 3 3 5\n' +
            '8\n' +
            '1 4 2 2\n' +
            '1 3 2 1\n' +
            '2 4 2 3\n' +
            '1 3 1 5\n' +
            '2 5 1 1\n' +
            '2 1 1 2\n' +
            '1 5 2 1\n' +
            '1 2 1 3']
    expected << [[4,
                  0],
                 [6,
                  2,
                  2]]
  }

  @Unroll('_031 : #testCase')
  def '_031'() {
    given:
    setIn(input)
    when:
    _031.main()
    then:
    results == expected
    where:
    testCase | input         || expected
    'input1' | '3\nbwb'      || [3]
    'input2' | '8\nwwbwbbwb' || [4]
  }

  @Unroll('_040 : #testCase')
  def '_040'() {
    given:
    setIn(input)
    when:
    _040.main()
    then:
    results == expected
    where:
    testCase | input                                                  || expected
    'input1' | '1 qwertyuiopasdfghjklzxcvbnm\nhqomq gfsoft iqeaqzigf' || ['paiza online hackathon']
    'input2' | '100 poiuytrewqlkjhgfdsamnbvcxz\nsnn xufu ngebmv qwtg' || ['cpp java python ruby']
  }
}
package p_challenge

import p_challenge.a.*
import spock.lang.Unroll
import utils.BaseSpecification

class a_Spec extends BaseSpecification {

  @Unroll('_003 : #testCase')
  def '_003'() {
    given:
      setIn(input)
    when:
      _003.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          '60\n' +
              'B 6 5\n' +
              'W 4 6\n' +
              'B 3 4\n' +
              'W 4 3\n' +
              'B 3 5\n' +
              'W 6 6\n' +
              'B 5 6\n' +
              'W 2 6\n' +
              'B 2 5\n' +
              'W 1 6\n' +
              'B 2 4\n' +
              'W 7 6\n' +
              'B 7 5\n' +
              'W 6 4\n' +
              'B 5 3\n' +
              'W 6 3\n' +
              'B 6 2\n' +
              'W 4 2\n' +
              'B 3 2\n' +
              'W 2 3\n' +
              'B 3 6\n' +
              'W 3 7\n' +
              'B 4 7\n' +
              'W 5 7\n' +
              'B 7 7\n' +
              'W 6 7\n' +
              'B 5 8\n' +
              'W 4 8\n' +
              'B 3 8\n' +
              'W 2 8\n' +
              'B 2 7\n' +
              'W 1 7\n' +
              'B 1 8\n' +
              'W 7 8\n' +
              'B 6 8\n' +
              'W 8 7\n' +
              'B 8 6\n' +
              'W 8 4\n' +
              'B 8 5\n' +
              'W 8 8\n' +
              'B 8 3\n' +
              'W 5 2\n' +
              'B 3 3\n' +
              'W 7 4\n' +
              'B 7 3\n' +
              'W 7 2\n' +
              'B 8 2\n' +
              'W 7 1\n' +
              'B 8 1\n' +
              'W 2 2\n' +
              'B 1 5\n' +
              'W 1 4\n' +
              'B 1 3\n' +
              'W 1 2\n' +
              'B 6 1\n' +
              'W 5 1\n' +
              'B 4 1\n' +
              'W 3 1\n' +
              'B 2 1\n' +
              'W 1 1',
          '14\n' +
              'B 4 3\n' +
              'W 5 3\n' +
              'B 6 3\n' +
              'W 3 3\n' +
              'B 3 4\n' +
              'W 3 5\n' +
              'B 4 6\n' +
              'W 7 3\n' +
              'B 6 5\n' +
              'W 7 5\n' +
              'B 5 6\n' +
              'W 5 7\n' +
              'B 6 4\n' +
              'W 7 4']
      expected << [['38-26 The black won!'],
                   ['00-18 The white won!']]
  }

  @Unroll('_004 : #testCase')
  def '_004'() {
    given:
      setIn(input)
    when:
      _004.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          '7 4 5\n' +
              '1 3 1\n' +
              '3 2 2\n' +
              '2 3 5\n' +
              '3 4 4\n' +
              '1 6 6',
          '5 5 8\n' +
              '3 3 4\n' +
              '1 3 2\n' +
              '4 2 2\n' +
              '2 1 2\n' +
              '2 4 4\n' +
              '3 1 1\n' +
              '1 4 3\n' +
              '4 3 4']
      expected << [[3], [1]]
  }

  @Unroll('_005 : #testCase')
  def '_005'() {
    given:
      setIn(input)
    when:
      _005.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          '10 10 18\n' +
              '3 4 G 1 8 2 6 2 10 2 7 G 10 10 10 9 1 3',
          '15 5 24\n' +
              '5 5 5 4 G 1 G 5 3 2 1 4 4 G G 1 5 5 5 2 1 5 3 1']
      expected << [[145], [124]]
  }

  @Unroll('_006 : #testCase')
  def '_006'() {
    given:
      setIn(input)
    when:
      _006.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          '3\n' +
              '-1 2\n' +
              '3 -1\n' +
              '-2 -2',
          '10\n' +
              '19 8\n' +
              '68 40\n' +
              '-48 4\n' +
              '60 29\n' +
              '22 90\n' +
              '-76 -35\n' +
              '60 93\n' +
              '-26 22\n' +
              '45 73\n' +
              '-61 59']
      expected << [[28], [4660]]
  }

  @Unroll('_009 : #testCase')
  def '_009'() {
    given:
      setIn(input)
    when:
      _009.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2', 'input3']
      input << [
          '3 5\n' +
              '__\\_/\n' +
              '___/_\n' +
              '\\/\\_/',
          '4 4\n' +
              '___\\\n' +
              '_\\__\n' +
              '____\n' +
              '_\\_/',
          '3 2\n' +
              '_\\\n' +
              '//\n' +
              '\\/']
      expected << [[9], [12], [7]]
  }

  @Unroll('_015 : #testCase')
  def '_015'() {
    given:
      setIn(input)
    when:
      _015.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2', 'input3', 'input4']
      input << [
          '........\n' +
              '...#....\n' +
              '..###...\n' +
              '..#.##..\n' +
              '..###...\n' +
              '........\n' +
              '........\n' +
              '........\n' +
              '....\n' +
              '.##.\n' +
              '..##\n' +
              '....\n' +
              '....\n' +
              '..#.\n' +
              '....\n' +
              '....\n' +
              '....\n' +
              '....\n' +
              '..##\n' +
              '....\n' +
              '..#.\n' +
              '.##.\n' +
              '....\n' +
              '....',
          '........\n' +
              '........\n' +
              '#.......\n' +
              '###.....\n' +
              '.###....\n' +
              '###.....\n' +
              '........\n' +
              '........\n' +
              '....\n' +
              '.##.\n' +
              '..##\n' +
              '....\n' +
              '....\n' +
              '..#.\n' +
              '....\n' +
              '....\n' +
              '....\n' +
              '....\n' +
              '..##\n' +
              '....\n' +
              '..#.\n' +
              '.##.\n' +
              '....\n' +
              '....',
          '..#.....\n' +
              '..##.#..\n' +
              '...#.##.\n' +
              '...###..\n' +
              '........\n' +
              '........\n' +
              '........\n' +
              '........\n' +
              '....\n' +
              '.##.\n' +
              '..##\n' +
              '....\n' +
              '....\n' +
              '..#.\n' +
              '....\n' +
              '....\n' +
              '....\n' +
              '....\n' +
              '..##\n' +
              '....\n' +
              '..#.\n' +
              '.##.\n' +
              '....\n' +
              '....',
          '....#...\n' +
              '...###..\n' +
              '...#.#..\n' +
              '.####...\n' +
              '........\n' +
              '........\n' +
              '........\n' +
              '........\n' +
              '....\n' +
              '.##.\n' +
              '..##\n' +
              '....\n' +
              '....\n' +
              '..#.\n' +
              '....\n' +
              '....\n' +
              '....\n' +
              '....\n' +
              '..##\n' +
              '....\n' +
              '..#.\n' +
              '.##.\n' +
              '....\n' +
              '....']
      expected << [['Yes'], ['Yes'], ['No'], ['No']]
  }
}
package p_challenge

import p_challenge.s.*
import spock.lang.Unroll
import utils.BaseSpecification

class s_Spec extends BaseSpecification {

  @Unroll('_002 : #testCase')
  def '_002'() {
    given:
      setIn(input)
    when:
      _002.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          '4 5\n' +
              '0 s 0 1\n' +
              '0 0 1 0\n' +
              '0 1 1 0\n' +
              '0 0 1 g\n' +
              '0 0 0 0',
          '4 4\n' +
              '0 s 0 1\n' +
              '1 0 0 0\n' +
              '0 1 1 1\n' +
              '0 0 0 g']
      expected << [[9], ['Fail']]
  }

  @Unroll('_007 : #testCase')
  def '_007'() {
    given:
      setIn(input)
    when:
      _007.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          'abcdefg10h12(ij2(3k))l9mnop4(3(2(6(qq)r)s5tu)7v5w)x15(yz)',
          '10000(10000(10000(2000(ab)500(dz)c200h)2mu3000(fpr)))']
      expected << [['a 1',
                    'b 1',
                    'c 1',
                    'd 1',
                    'e 1',
                    'f 1',
                    'g 1',
                    'h 10',
                    'i 12',
                    'j 12',
                    'k 72',
                    'l 1',
                    'm 9',
                    'n 1',
                    'o 1',
                    'p 1',
                    'q 288',
                    'r 24',
                    's 12',
                    't 60',
                    'u 12',
                    'v 28',
                    'w 20',
                    'x 1',
                    'y 15',
                    'z 15'],
                   ['a 2000000000000000',
                    'b 2000000000000000',
                    'c 1000000000000',
                    'd 500000000000000',
                    'e 0',
                    'f 300000000000',
                    'g 0',
                    'h 200000000000000',
                    'i 0',
                    'j 0',
                    'k 0',
                    'l 0',
                    'm 200000000',
                    'n 0',
                    'o 0',
                    'p 300000000000',
                    'q 0',
                    'r 300000000000',
                    's 0',
                    't 0',
                    'u 100000000',
                    'v 0',
                    'w 0',
                    'x 0',
                    'y 0',
                    'z 500000000000000']]
  }

}
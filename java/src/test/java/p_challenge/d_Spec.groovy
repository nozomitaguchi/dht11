package p_challenge

import p_challenge.d.*
import spock.lang.Unroll
import utils.BaseSpecification

class d_Spec extends BaseSpecification {

  @Unroll('_002 : #testCase')
  def '_002'() {
    given:
      setIn(input)
    when:
      _002.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '10 20' || [20]
      'input2' | '3 3'   || ['eq']
  }

  @Unroll('_003 : #testCase')
  def '_003'() {
    given:
      setIn(input)
    when:
      _003.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '4'   || ['4 8 12 16 20 24 28 32 36']
      'input2' | '99'  || ['99 198 297 396 495 594 693 792 891']
  }

  @Unroll('_004 : #testCase')
  def '_004'() {
    given:
      setIn(input)
    when:
      _004.main()
    then:
      results == expected
    where:
      testCase | input                               || expected
      'input1' | '2\nPaiza\nGino'                    || ['Hello Paiza,Gino.']
      'input2' | '5\nAlice\nBob\nCarol\nDave\nEllen' || ['Hello Alice,Bob,Carol,Dave,Ellen.']
  }

  @Unroll('_005 : #testCase')
  def '_005'() {
    given:
      setIn(input)
    when:
      _005.main()
    then:
      results == expected
    where:
      testCase | input  || expected
      'input1' | '3 3'  || ['3 6 9 12 15 18 21 24 27 30']
      'input2' | '5 10' || ['5 15 25 35 45 55 65 75 85 95']
      'input3' | '1 3'  || ['1 4 7 10 13 16 19 22 25 28']
  }

  @Unroll('_006 : #testCase')
  def '_006'() {
    given:
      setIn(input)
    when:
      _006.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '54 km' || [54000000]
      'input2' | '2 cm'  || [20]
      'input3' | '12 m'  || [12000]
  }

  @Unroll('_007 : #testCase')
  def '_007'() {
    given:
      setIn(input)
    when:
      _007.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '4'   || ['****']
      'input2' | '6'   || ['******']
      'input3' | '2'   || ['**']
  }

  @Unroll('_008 : #testCase')
  def '_008'() {
    given:
      setIn(input)
    when:
      _008.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '4'   || ['even']
      'input2' | '5'   || ['odd']
      'input3' | '2'   || ['even']
  }

  @Unroll('_009 : #testCase')
  def '_009'() {
    given:
      setIn(input)
    when:
      _009.main()
    then:
      results == expected
    where:
      testCase | input       || expected
      'input1' | '1999 2000' || [1]
      'input2' | '794 1192'  || [398]
  }

  @Unroll('_010 : #testCase')
  def '_010'() {
    given:
      setIn(input)
    when:
      _010.main()
    then:
      results == expected
    where:
      testCase | input                       || expected
      'input1' | 'paiza\nexample.com'        || ['paiza@example.com']
      'input2' | 'paiza.tarou2015\npaiza.jp' || ['paiza.tarou2015@paiza.jp']
  }

  @Unroll('_011 : #testCase')
  def '_011'() {
    given:
      setIn(input)
    when:
      _011.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | 'C'   || [3]
      'input2' | 'X'   || [24]
  }

  @Unroll('_012 : #testCase')
  def '_012'() {
    given:
      setIn(input)
    when:
      _012.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '-1'  || [1]
      'input2' | '15'  || [15]
  }

  @Unroll('_013 : #testCase')
  def '_013'() {
    given:
      setIn(input)
    when:
      _013.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '10 3'  || ['3 1']
      'input2' | '12 14' || ['0 12']
  }

  @Unroll('_014 : #testCase')
  def '_014'() {
    given:
      setIn(input)
    when:
      _014.main()
    then:
      results == expected
    where:
      testCase | input                        || expected
      'input1' | 'paiza'                      || ['PAIZA']
      'input2' | 'abcdefghijklmnopqrstuvwxyz' || ['ABCDEFGHIJKLMNOPQRSTUVWXYZ']
  }

  @Unroll('_015 : #testCase')
  def '_015'() {
    given:
      setIn(input)
    when:
      _015.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '3'   || [3, 2, 1]
      'input2' | '10'  || [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
  }

  @Unroll('_016 : #testCase')
  def '_016'() {
    given:
      setIn(input)
    when:
      _016.main()
    then:
      results == expected
    where:
      testCase | input         || expected
      'input1' | 'aabbccdd\n5' || ['aabbc']
      'input2' | 'oiuytrer\n8' || ['oiuytrer']
  }

  @Unroll('_017 : #testCase')
  def '_017'() {
    given:
      setIn(input)
    when:
      _017.main()
    then:
      results == expected
    where:
      testCase | input             || expected
      'input1' | '9\n12\n3\n6\n10' || [12, 3]
      'input2' | '1\n1\n2\n2\n3'   || [3, 1]
  }

  @Unroll('_019 : #testCase')
  def '_019'() {
    given:
      setIn(input)
    when:
      _019.main()
    then:
      results == expected
    where:
      testCase | input          || expected
      'input1' | 'paiza 3'      || ['i']
      'input2' | 'abcdefghij 5' || ['e']
  }

  @Unroll('_021 : #testCase')
  def '_021'() {
    given:
      setIn(input)
    when:
      _021.main()
    then:
      results == expected
    where:
      testCase | input          || expected
      'input1' | 'paiza\npaiza' || ['Yes']
      'input2' | 'gino\npaiza'  || ['No']
  }

  @Unroll('_022 : #testCase')
  def '_022'() {
    given:
      setIn(input)
    when:
      _022.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '4'   || [96]
      'input2' | '1'   || [6]
  }

  @Unroll('_023 : #testCase')
  def '_023'() {
    given:
      setIn(input)
    when:
      _023.main()
    then:
      results == expected
    where:
      testCase | input     || expected
      'input1' | 'PAIZA'   || [2]
      'input2' | 'aAaAaAa' || [3]
  }

  @Unroll('_024 : #testCase')
  def '_024'() {
    given:
      setIn(input)
    when:
      _024.main()
    then:
      results == expected
    where:
      testCase | input    || expected
      'input1' | '60\n90' || [30]
      'input2' | '45\n45' || [90]
  }

  @Unroll('_025 : #testCase')
  def '_025'() {
    given:
      setIn(input)
    when:
      _025.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '98'  || ['098']
      'input2' | '2'   || ['002']
  }

  @Unroll('_026 : #testCase')
  def '_026'() {
    given:
      setIn(input)
    when:
      _026.main()
    then:
      results == expected
    where:
      testCase | input                             || expected
      'input1' | 'yes\nyes\nyes\nyes\nno\nno\nyes' || [2]
      'input2' | 'yes\nno\nno\nno\nno\nno\nyes'    || [5]
  }

  @Unroll('_027 : #testCase')
  def '_027'() {
    given:
      setIn(input)
    when:
      _027.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '10'  || [55]
      'input2' | '99'  || [4950]
  }

  @Unroll('_028 : #testCase')
  def '_028'() {
    given:
      setIn(input)
    when:
      _028.main()
    then:
      results == expected
    where:
      testCase | input  || expected
      'input1' | '100'  || [3]
      'input2' | '4304' || [4]
  }

  @Unroll('_029 : #testCase')
  def '_029'() {
    given:
      setIn(input)
    when:
      _029.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '3'   || [4]
      'input2' | '1'   || [6]
  }

  @Unroll('_031 : #testCase')
  def '_031'() {
    given:
      setIn(input)
    when:
      _031.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '16'  || [960]
      'input2' | '3'   || [180]
  }

  @Unroll('_032 : #testCase')
  def '_032'() {
    given:
      setIn(input)
    when:
      _032.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '70'  || [30]
      'input2' | '25'  || [75]
  }

  @Unroll('_033 : #testCase')
  def '_033'() {
    given:
      setIn(input)
    when:
      _033.main()
    then:
      results == expected
    where:
      testCase | input                || expected
      'input1' | 'Midorikawa Tsubame' || ['M.T']
      'input2' | 'Paiza Tarou'        || ['P.T']
  }

  @Unroll('_034 : #testCase')
  def '_034'() {
    given:
      setIn(input)
    when:
      _034.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '4'   || [1]
      'input2' | '3'   || [3]
  }

  @Unroll('_035 : #testCase')
  def '_035'() {
    given:
      setIn(input)
    when:
      _035.main()
    then:
      results == expected
    where:
      testCase | input       || expected
      'input1' | '2016 2 12' || ['2016/2/12']
      'input2' | '2000 1 1'  || ['2000/1/1']
  }

  @Unroll('_036 : #testCase')
  def '_036'() {
    given:
      setIn(input)
    when:
      _036.main()
    then:
      results == expected
    where:
      testCase | input               || expected
      'input1' | 'paizaatcodeattest' || ['paiza@code@test']
      'input2' | 'atatatpaizaatatat' || ['@@@paiza@@@']
  }

  @Unroll('_037 : #testCase')
  def '_037'() {
    given:
      setIn(input)
    when:
      _037.main()
    then:
      results == expected
    where:
      testCase | input    || expected
      'input1' | '7\n30'  || [5]
      'input2' | '3\n100' || [34]
  }

  @Unroll('_038 : #testCase')
  def '_038'() {
    given:
      setIn(input)
    when:
      _038.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '4'   || [6]
      'input2' | '10'  || [45]
  }

  @Unroll('_039 : #testCase')
  def '_039'() {
    given:
      setIn(input)
    when:
      _039.main()
    then:
      results == expected
    where:
      testCase | input        || expected
      'input1' | '14\n14\n14' || ['YES']
      'input2' | '5\n6\n5'    || ['NO']
  }

  @Unroll('_040 : #testCase')
  def '_040'() {
    given:
      setIn(input)
    when:
      _040.main()
    then:
      results == expected
    where:
      testCase | input                        || expected
      'input1' | '13\n0\n15\n30\n89\n100\n31' || [4]
      'input2' | '0\n14\n18\n22\n0\n2\n4'     || [7]
  }

  @Unroll('_041 : #testCase')
  def '_041'() {
    given:
      setIn(input)
    when:
      _041.main()
    then:
      results == expected
    where:
      testCase | input      || expected
      'input1' | '400 5 85' || ['OK']
      'input2' | '600 4 70' || ['NG']
  }

  @Unroll('_042 : #testCase')
  def '_042'() {
    given:
      setIn(input)
    when:
      _042.main()
    then:
      results == expected
    where:
      testCase | input           || expected
      'input1' | '1 2\n3 4'      || [-2]
      'input2' | '-50 10\n25 -5' || [0]
  }

  @Unroll('_043 : #testCase')
  def '_043'() {
    given:
      setIn(input)
    when:
      _043.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '31'  || ['cloudy']
      'input2' | '2'   || ['sunny']
  }

  @Unroll('_044 : #testCase')
  def '_044'() {
    given:
      setIn(input)
    when:
      _044.main()
    then:
      results == expected
    where:
      testCase | input     || expected
      'input1' | 'Noda F'  || ['Hi, Ms. Noda']
      'input2' | 'Paiza M' || ['Hi, Mr. Paiza']
  }

  @Unroll('_045 : #testCase')
  def '_045'() {
    given:
      setIn(input)
    when:
      _045.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '5'   || ['A']
      'input2' | '3'   || ['C']
  }

  @Unroll('_046 : #testCase')
  def '_046'() {
    given:
      setIn(input)
    when:
      _046.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '1 3 5' || [5]
      'input2' | '2 5 5' || [5]
  }

  @Unroll('_047 : #testCase')
  def '_047'() {
    given:
      setIn(input)
    when:
      _047.main()
    then:
      results == expected
    where:
      testCase | input                          || expected
      'input1' | 'Ono\nOrujov\nShavdatuashvili' || ['Gold Ono', 'Silver Orujov', 'Bronze Shavdatuashvili']
      'input2' | 'Ogino\nKalisz\nSeto'          || ['Gold Ogino', 'Silver Kalisz', 'Bronze Seto']
  }

  @Unroll('_048 : #testCase')
  def '_048'() {
    given:
      setIn(input)
    when:
      _048.main()
    then:
      results == expected
    where:
      testCase | input              || expected
      'input1' | '5\n8\n19\n25\n31' || [3, 11, 6, 6]
      'input2' | '2\n3\n7\n9\n28'   || [1, 4, 2, 19]
  }

  @Unroll('_049 : #testCase')
  def '_049'() {
    given:
      setIn(input)
    when:
      _049.main()
    then:
      results == expected
    where:
      testCase | input         || expected
      'input1' | 'korakunoaki' || ['koraku']
      'input2' | 'dokusyo'     || ['dokusyo']
      'input3' | 'minorinoaki' || ['minori']
      'input4' | 'yakiniku'    || ['yakiniku']
  }

  @Unroll('_050 : #testCase')
  def '_050'() {
    given:
      setIn(input)
    when:
      _050.main()
    then:
      results == expected
    where:
      testCase | input     || expected
      'input1' | '3 50'    || [8]
      'input2' | '100 500' || [10]
      'input3' | '2 4'     || [6]
  }

  @Unroll('_051 : #testCase')
  def '_051'() {
    given:
      setIn(input)
    when:
      _051.main()
    then:
      results == expected
    where:
      testCase | input                 || expected
      'input1' | 'W W W W W S S S S S' || ['OK']
      'input2' | 'S S W W S W S S S S' || ['NG']
      'input3' | 'W S S S S S S S S W' || ['NG']
  }

  @Unroll('_052 : #testCase')
  def '_052'() {
    given:
      setIn(input)
    when:
      _052.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '4'   || [10]
      'input2' | '10'  || [55]
      'input3' | '50'  || [1275]
  }

  @Unroll('_053 : #testCase')
  def '_053'() {
    given:
      setIn(input)
    when:
      _053.main()
    then:
      results == expected
    where:
      testCase | input       || expected
      'input1' | 'chocolate' || ['Thanks!']
      'input2' | 'candy'     || ['Thanks!']
      'input3' | 'juice'     || ['No!']
  }

  @Unroll('_054 : #testCase')
  def '_054'() {
    given:
      setIn(input)
    when:
      _054.main()
    then:
      results == expected
    where:
      testCase | input                || expected
      'input1' | '11111111111'        || ['OK']
      'input2' | '1111'               || [7]
      'input3' | '111111111111111111' || ['OK']
  }

  @Unroll('_055 : #testCase')
  def '_055'() {
    given:
      setIn(input)
    when:
      _055.main()
    then:
      results == expected
    where:
      testCase | input          || expected
      'input1' | 'a decade'     || ['Best in a decade']
      'input2' | 'the world'    || ['Best in the world']
      'input3' | 'history ever' || ['Best in history ever']
  }

  @Unroll('_056 : #testCase')
  def '_056'() {
    given:
      setIn(input)
    when:
      _056.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '5 2' || [117]
      'input2' | '6 4' || [152]
      'input3' | '2 1' || [7]
  }

  @Unroll('_057 : #testCase')
  def '_057'() {
    given:
      setIn(input)
    when:
      _057.main()
    then:
      results == expected
    where:
      testCase | input                            || expected
      'input1' | '3\ncartoy plamodel gameconsole' || ['gameconsole']
      'input2' | '2\ngloves muffler sweater'      || ['muffler']
      'input3' | '1\npizza steak sushi'           || ['pizza']
  }

  @Unroll('_058 : #testCase')
  def '_058'() {
    given:
      setIn(input)
    when:
      _058.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '1 1 1' || ['ABA']
      'input2' | '5 7 5' || ['AAAAABBBBBBBAAAAA']
  }

  @Unroll('_059 : #testCase')
  def '_059'() {
    given:
      setIn(input)
    when:
      _059.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | 'K Q' || ['K Q']
      'input2' | 'J J' || ['J Q']
  }

  @Unroll('_060 : #testCase')
  def '_060'() {
    given:
      setIn(input)
    when:
      _060.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '3 4'   || [-1]
      'input2' | '80 44' || [36]
  }

  @Unroll('_061 : #testCase')
  def '_061'() {
    given:
      setIn(input)
    when:
      _061.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '3'   || [9]
      'input2' | '0'   || [1]
      'input3' | '52'  || [156]
  }

  @Unroll('_062 : #testCase')
  def '_062'() {
    given:
      setIn(input)
    when:
      _062.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '2 3 5' || ['AB', 'CDE', 'FGHIJ']
      'input2' | '1 1 8' || ['A', 'B', 'CDEFGHIJ']
      'input3' | '3 4 3' || ['ABC', 'DEFG', 'HIJ']
  }

  @Unroll('_063 : #testCase')
  def '_063'() {
    given:
      setIn(input)
    when:
      _063.main()
    then:
      results == expected
    where:
      testCase | input               || expected
      'input1' | '0 10 23 35 57\n29' || [4]
      'input2' | '1 11 39 44 51\n58' || [6]
      'input3' | '3 13 24 45 50\n1'  || [1]
  }

  @Unroll('_064 : #testCase')
  def '_064'() {
    given:
      setIn(input)
    when:
      _064.main()
    then:
      results == expected
    where:
      testCase | input            || expected
      'input1' | 'if False'       || ['if True']
      'input2' | 'aprilFalsefool' || ['aprilTruefool']
      'input3' | 'aprilfool'      || ['aprilfool']
  }

  @Unroll('_065 : #testCase')
  def '_065'() {
    given:
      setIn(input)
    when:
      _065.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '200' || ['ok']
      'input2' | '404' || ['error']
      'input3' | '999' || ['unknown']
  }

  @Unroll('_066 : #testCase')
  def '_066'() {
    given:
      setIn(input)
    when:
      _066.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '10 13' || [3]
      'input2' | '18 13' || ['No']
      'input3' | '4 4'   || [0]
  }

  @Unroll('_067 : #testCase')
  def '_067'() {
    given:
      setIn(input)
    when:
      _067.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '3'   || ['ON']
      'input2' | '6'   || ['OFF']
  }

  @Unroll('_068 : #testCase')
  def '_068'() {
    given:
      setIn(input)
    when:
      _068.main()
    then:
      results == expected
    where:
      testCase | input            || expected
      'input1' | '5\nSSRSR'       || ['3 2']
      'input2' | '10\nSSSSSSSSSS' || ['10 0']
  }

  @Unroll('_069 : #testCase')
  def '_069'() {
    given:
      setIn(input)
    when:
      _069.main()
    then:
      results == expected
    where:
      testCase  | input                   || expected
      'input1'  | '50 40 50 60 30 80 100' || [58.6]
      'input2'  | '0 10 20 30 40 50 60'   || [30.0]
      'input3 ' | '0 0 0 0 0 0 0'         || [0.0]
  }

  @Unroll('_070 : #testCase')
  def '_070'() {
    given:
      setIn(input)
    when:
      _070.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '30 10' || [20]
      'input2' | '58 24' || [34]
  }

  @Unroll('_071 : #testCase')
  def '_071'() {
    given:
      setIn(input)
    when:
      _071.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '25 50' || ['Yes']
      'input2' | '10 20' || ['Yes']
      'input3' | '20 50' || ['No']
  }

  @Unroll('_072 : #testCase')
  def '_072'() {
    given:
      setIn(input)
    when:
      _072.main()
    then:
      results == expected
    where:
      testCase | input        || expected
      'input1' | '10 3 10000' || [40000]
      'input2' | '3 3 2500'   || [2500]
  }

  @Unroll('_073 : #testCase')
  def '_073'() {
    given:
      setIn(input)
    when:
      _073.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | 'ABCDE' || ['EDCBA']
      'input2' | 'ABCBA' || ['ABCBA']
  }

  @Unroll('_074 : #testCase')
  def '_074'() {
    given:
      setIn(input)
    when:
      _074.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '32'  || [8]
      'input2' | '47'  || [23]
  }

  @Unroll('_075 : #testCase')
  def '_075'() {
    given:
      setIn(input)
    when:
      _075.main()
    then:
      results == expected
    where:
      testCase | input        || expected
      'input1' | '1\n3\n2\n5' || [4]
      'input2' | '4\n3\n2\n1' || [5]
  }

  @Unroll('_076 : #testCase')
  def '_076'() {
    given:
      setIn(input)
    when:
      _076.main()
    then:
      results == expected
    where:
      testCase | input                      || expected
      'input1' | 'ngword\nthisisngword'     || ['NG']
      'input2' | 'ngword\npaizaprogramming' || ['paizaprogramming']
  }

  @Unroll('_077 : #testCase')
  def '_077'() {
    given:
      setIn(input)
    when:
      _077.main()
    then:
      results == expected
    where:
      testCase | input     || expected
      'input1' | '100 20'  || [2000]
      'input2' | '9999 29' || ['NG']
  }

  @Unroll('_078 : #testCase')
  def '_078'() {
    given:
      setIn(input)
    when:
      _078.main()
    then:
      results == expected
    where:
      testCase | input                      || expected
      'input1' | '50 50 70 40 70 80 60\n60' || ['pass']
      'input2' | '50 30 50 24 14 50 60\n40' || ['failure']
  }

  @Unroll('_079 : #testCase')
  def '_079'() {
    given:
      setIn(input)
    when:
      _079.main()
    then:
      results == expected
    where:
      testCase | input      || expected
      'input1' | 'AAAAAAAA' || ['NG']
      'input2' | 'Paiza'    || ['OK']
  }

  @Unroll('_080 : #testCase')
  def '_080'() {
    given:
      setIn(input)
    when:
      _080.main()
    then:
      results == expected
    where:
      testCase | input  || expected
      'input1' | '12 5' || [92000]
      'input2' | '1 1'  || [10000]
  }

  @Unroll('_081 : #testCase')
  def '_081'() {
    given:
      setIn(input)
    when:
      _081.main()
    then:
      results == expected
    where:
      testCase | input      || expected
      'input1' | '3\n5 5'   || [1]
      'input2' | '7\n10 20' || [4]
  }

  @Unroll('_083 : #testCase')
  def '_083'() {
    given:
      setIn(input)
    when:
      _083.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '1 7'   || ['HIT']
      'input2' | '10 11' || ['STAND']
  }

  @Unroll('_087 : #testCase')
  def '_087'() {
    given:
      setIn(input)
    when:
      _087.main()
    then:
      results == expected
    where:
      testCase | input              || expected
      'input1' | '5\nP\na\ni\nz\na' || ['Paiza']
      'input2' | '4\nR\nb\nD\nx'    || ['RbDx']
  }

  @Unroll('_088 : #testCase')
  def '_088'() {
    given:
      setIn(input)
    when:
      _088.main()
    then:
      results == expected
    where:
      testCase | input   || expected
      'input1' | '7 -3'  || [10]
      'input2' | '18 17' || [1]
  }

  @Unroll('_089 : #testCase')
  def '_089'() {
    given:
      setIn(input)
    when:
      _089.main()
    then:
      results == expected
    where:
      testCase | input      || expected
      'input1' | '100paiza' || ['100']
      'input2' | '18km'     || ['18']
  }

  @Unroll('_091 : #testCase')
  def '_091'() {
    given:
      setIn(input)
    when:
      _091.main()
    then:
      results == expected
    where:
      testCase | input                 || expected
      'input1' | '1 1 3 4 5 4 4 2 3 1' || [4]
      'input2' | '4 3 4 5 4 3 4 5 5 3' || [0]
  }

  @Unroll('_095 : #testCase')
  def '_095'() {
    given:
      setIn(input)
    when:
      _095.main()
    then:
      results == expected
    where:
      testCase | input      || expected
      'input1' | '500\n180' || [2]
      'input2' | '295\n295' || [1]
  }

  @Unroll('_097 : #testCase')
  def '_097'() {
    given:
      setIn(input)
    when:
      _097.main()
    then:
      results == expected
    where:
      testCase | input           || expected
      'input1' | '1 0 1 1 1 1 0' || ['yes']
      'input2' | '0 0 0 0 1 0 0' || ['no']
  }

  @Unroll('_098 : #testCase')
  def '_098'() {
    given:
      setIn(input)
    when:
      _098.main()
    then:
      results == expected
    where:
      testCase | input      || expected
      'input1' | '250000 3' || [750000]
      'input2' | '280000 2' || [560000]
  }

  @Unroll('_099 : #testCase')
  def '_099'() {
    given:
      setIn(input)
    when:
      _099.main()
    then:
      results == expected
    where:
      testCase | input    || expected
      'input1' | 'paiza'  || ['p', 'a', 'i', 'z', 'a']
      'input2' | 'coding' || ['c', 'o', 'd', 'i', 'n', 'g']
  }

  @Unroll('_102 : #testCase')
  def '_102'() {
    given:
      setIn(input)
    when:
      _102.main()
    then:
      results == expected
    where:
      testCase | input || expected
      'input1' | '3'   || [130]
      'input2' | '19'  || [290]
  }

  @Unroll('_103 : #testCase')
  def '_103'() {
    given:
      setIn(input)
    when:
      _103.main()
    then:
      results == expected
    where:
      testCase | input         || expected
      'input1' | 'paiza'       || ['aziap']
      'input2' | 'programming' || ['gnimmargorp']
  }

}
package p_challenge

import p_challenge.c.*
import spock.lang.Unroll
import utils.BaseSpecification

class c_Spec extends BaseSpecification {

  @Unroll('_005 : #testCase')
  def '_005'() {
    given:
      setIn(input)
    when:
      _005.main()
    then:
      results == expected
    where:
      testCase | input                                                     || expected
      'input1' | '4\n192.168.0.1\n192.168.0.2\n192.168.0.3\n192.168.0.4'   || ['True', 'True', 'True', 'True']
      'input2' | '4\n192.400.1.10.1000...\n4.3.2.1\n0..33.444...\n1.2.3.4' || ['False', 'True', 'False', 'True']
  }

  @Unroll('_006 : #testCase')
  def '_006'() {
    given:
      setIn(input)
    when:
      _006.main()
    then:
      results == expected
    where:
      testCase << ['input1']
      input << [
          '4 10 3\n' +
              '1.5 1.2 2 0.4\n' +
              '208 200 3 99988\n' +
              '255 150 50 65472\n' +
              '103 748 39 48571\n' +
              '159 403 32 89928\n' +
              '254 300 67 78492\n' +
              '249 298 47 45637\n' +
              '253 382 89 37282\n' +
              '250 350 78 76782\n' +
              '251 371 69 67821\n' +
              '256 312 89 54382']
      expected << [[40553, 36757, 32272]]
  }

  @Unroll('_008 : #testCase')
  def '_008'() {
    given:
      setIn(input)
    when:
      _008.main()
    then:
      results == expected
    where:
      testCase | input                                                                      || expected
      'input1' | '<abc> <xyz>\nhoge<abc>piyo<xyz>'                                          || ['piyo']
      'input2' | '<abc> <ijk>\n<abc>xxxx<ijk>yyyyyy<abc>zzz<ijk>'                           || ['xxxx', 'zzz']
      'input3' | '<paiza> <poh>\n<paiza><poh>'                                              || ['<blank>']
      'input4' | '<Banana> <Cupcake>\nApplePie<Banana>Bread<Cupcake>Apple<Banana><Cupcake>' || ['Bread', '<blank>']
  }

  @Unroll('_010 : #testCase')
  def '_010'() {
    given:
      setIn(input)
    when:
      _010.main()
    then:
      results == expected
    where:
      testCase | input                                      || expected
      'input1' | '20 10 10\n3\n25 10\n20 15\n70 70'         || ['noisy', 'noisy', 'silent']
      'input2' | '50 50 100\n4\n0 0\n0 100\n100 0\n100 100' || ['noisy', 'noisy', 'noisy', 'noisy']
  }

  @Unroll('_013 : #testCase')
  def '_013'() {
    given:
      setIn(input)
    when:
      _013.main()
    then:
      results == expected
    where:
      testCase | input                                || expected
      'input1' | '4\n5\n101\n204\n301\n401\n501'      || ['101', '301', '501']
      'input2' | '9\n3\n409\n509\n109'                || ['none']
      'input3' | '1\n6\n101\n102\n205\n224\n231\n314' || ['205', '224']
  }

  @Unroll('_014 : #testCase')
  def '_014'() {
    given:
      setIn(input)
    when:
      _014.main()
    then:
      results == expected
    where:
      testCase | input                                                          || expected
      'input1' | '4 2\n6 6 6\n4 6 4\n6 1 1\n4 4 4'                              || [1, 2, 4]
      'input2' | '3 5\n2 184 12\n135 169 37\n99 121 41'                         || [2, 3]
      'input3' | '5 7\n197 78 14\n14 197 62\n80 14 109\n138 145 147\n9 130 175' || [1, 2, 3, 4]
  }

  @Unroll('_015 : #testCase')
  def '_015'() {
    given:
      setIn(input)
    when:
      _015.main()
    then:
      results == expected
    where:
      testCase | input                         || expected
      'input1' | '3\n1 1024\n11 2048\n21 4196' || [71]
      'input2' | '1\n30 340'                   || [10]
      'input3' | '2\n5 10000\n12 10000'        || [600]
  }

  @Unroll('_016 : #testCase')
  def '_016'() {
    given:
      setIn(input)
    when:
      _016.main()
    then:
      results == expected
    where:
      testCase | input        || expected
      'input1' | 'PAIZA'      || ['P4124']
      'input2' | 'ALANTURING' || ['4L4NTUR1N6']
  }

  @Unroll('_017 : #testCase')
  def '_017'() {
    given:
      setIn(input)
    when:
      _017.main()
    then:
      results == expected
    where:
      testCase | input                         || expected
      'input1' | '5 1\n2\n7 2\n1 4'            || ['Low', 'High']
      'input2' | '7 3\n4\n7 1\n7 4\n5 3\n10 1' || ['Low', 'High', 'High', 'Low']
  }

  @Unroll('_018 : #testCase')
  def '_018'() {
    given:
      setIn(input)
    when:
      _018.main()
    then:
      results == expected
    where:
      testCase | input                                                                                           || expected
      'input1' | '4\nsupaisu 5\nimo 2\nniku 2\nmizu 3\n6\nmizu 7\nimo 4\nninjin 10\nunagi 6\nsupaisu 20\nniku 5' || [2]
      'input2' | '2\ngohan 1\nokazu 1\n1\nmizu 10000'                                                            || [0]
  }

  @Unroll('_019 : #testCase')
  def '_019'() {
    given:
      setIn(input)
    when:
      _019.main()
    then:
      results == expected
    where:
      testCase | input            || expected
      'input1' | '3\n28\n16\n777' || ['perfect', 'nearly', 'neither']
      'input2' | '4\n3\n4\n5\n6'  || ['neither', 'nearly', 'neither', 'perfect']
  }

  @Unroll('_020 : #testCase')
  def '_020'() {
    given:
      setIn(input)
    when:
      _020.main()
    then:
      results == expected
    where:
      testCase | input      || expected
      'input1' | '1 80 40'  || [0.12]
      'input2' | '10 31 52' || [3.312]
  }

  @Unroll('_021 : #testCase')
  def '_021'() {
    given:
      setIn(input)
    when:
      _021.main()
    then:
      results == expected
    where:
      testCase | input                                   || expected
      'input1' | '0 0 1 2\n3\n0 0\n1 1\n4 2'             || ['no', 'yes', 'no']
      'input2' | '47 19 57 80\n3\n62 -52\n35 -70\n-81 2' || ['yes', 'no', 'no']
  }

  @Unroll('_022 : #testCase')
  def '_022'() {
    given:
      setIn(input)
    when:
      _022.main()
    then:
      results == expected
    where:
      testCase | input                                                                                                                            || expected
      'input1' | '5\n11 14 16 10\n12 15 17 10\n13 11 14 11\n12 10 13 8\n11 13 14 10'                                                              || ['11 13 17 8']
      'input2' | '3\n15036 14936 15089 14919\n15009 15073 15084 14916\n14805 14738 14807 14672'                                                   || ['15036 14738 15089 14672']
      'input3' | '5\n16654 16544 16757 16543\n16535 16321 16602 16310\n16321 16315 16463 16273\n16313 16141 16313 15855\n15921 16117 16212 15918' || ['16654 16117 16757 15855']
  }

  @Unroll('_023 : #testCase')
  def '_023'() {
    given:
      setIn(input)
    when:
      _023.main()
    then:
      results == expected
    where:
      testCase | input                                                                       || expected
      'input1' | '1 2 3 4 5 6\n3\n1 5 4 2 3 6\n9 6 2 7 1 5\n32 9 87 33 41 60'                || [6, 4, 0]
      'input2' | '72 2 90 84 57 85\n3\n36 70 1 72 54 82\n36 2 40 12 3 58\n25 11 90 57 85 99' || [1, 1, 3]
  }

  @Unroll('_024 : #testCase')
  def '_024'() {
    given:
      setIn(input)
    when:
      _024.main()
    then:
      results == expected
    where:
      testCase | input                           || expected
      'input1' | '3\nSET 1 10\nSET 2 20\nADD 40' || ['10 50']
      'input2' | '3\nSET 1 -23\nSUB 77\nSET 1 0' || ['0 -100']
  }

  @Unroll('_025 : #testCase')
  def '_025'() {
    given:
      setIn(input)
    when:
      _025.main()
    then:
      results == expected
    where:
      testCase | input                                                                                   || expected
      'input1' | '50\n5\n3 20 70\n3 40 170\n3 59 90\n4 5 55\n4 25 40'                                    || [9]
      'input2' | '5\n10\n1 10 1\n1 20 1\n1 30 1\n1 40 1\n1 50 1\n2 10 1\n2 20 2\n2 30 3\n2 40 4\n2 50 5' || [4]
  }

  @Unroll('_026 : #testCase')
  def '_026'() {
    given:
      setIn(input)
    when:
      _026.main()
    then:
      results == expected
    where:
      testCase | input                          || expected
      'input1' | '3 5 2\n8 10\n7 6\n7 4'        || [2]
      'input2' | '3 64 10\n84 75\n73 53\n56 34' || ['not found']
  }

  @Unroll('_028 : #testCase')
  def '_028'() {
    given:
      setIn(input)
    when:
      _028.main()
    then:
      results == expected
    where:
      testCase | input                                                                                                                                                                                        || expected
      'input1' | '4\napple aple\norange olange\ngrape glepe\nlemon lemon'                                                                                                                                     || [3]
      'input2' | '12\njanuary januarry\nfebruary febrary\nmarch march\napril aplil\nmay may\njune june\njuly jury\naugust ougust\nseptember septenber\noctober october\nnovember novembar\ndecember dicembar' || [13]
  }

  @Unroll('_029 : #testCase')
  def '_029'() {
    given:
      setIn(input)
    when:
      _029.main()
    then:
      results == expected
    where:
      testCase | input                                                                 || expected
      'input1' | '7 3\n19 0\n20 0\n21 60\n22 30\n23 10\n24 10\n25 90'                  || ['22 24']
      'input2' | '10 4\n3 30\n4 25\n5 20\n6 65\n7 75\n8 0\n9 10\n10 100\n11 35\n12 80' || ['3 6']
  }

  @Unroll('_030 : #testCase')
  def '_030'() {
    given:
      setIn(input)
    when:
      _030.main()
    then:
      results == expected
    where:
      testCase | input                            || expected
      'input1' | '3 2\n128 127\n127 128\n128 127' || ['1 0', '0 1', '1 0']
      'input2' | '1 1\n0'                         || ['0']
  }

  @Unroll('_031 : #testCase')
  def '_031'() {
    given:
      setIn(input)
    when:
      _031.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          '6\n' +
              'tokyo 9\n' +
              'beijing 8\n' +
              'singapore 7\n' +
              'london 0\n' +
              'newyork -5\n' +
              'osaka 9\n' +
              'singapore 19:38',
          '27\n' +
              'howland -12\n' +
              'samoa -11\n' +
              'hawaii -10\n' +
              'alaska -9\n' +
              'california -8\n' +
              'arizona -7\n' +
              'texas -6\n' +
              'massachusetts -5\n' +
              'santiago -4\n' +
              'brasilia -3\n' +
              'greenland -2\n' +
              'verde -1\n' +
              'morocco 0\n' +
              'london 1\n' +
              'paris 2\n' +
              'athens 3\n' +
              'moscow 4\n' +
              'islamabad 5\n' +
              'astana 6\n' +
              'bangkok 7\n' +
              'hongkong 8\n' +
              'seoul 9\n' +
              'guam 10\n' +
              'kuril 11\n' +
              'southpole 12\n' +
              'nukualofa 13\n' +
              'tokelau 14\n' +
              'southpole 00:00']
      expected << [['21:38',
                    '20:38',
                    '19:38',
                    '12:38',
                    '07:38',
                    '21:38'],
                   ['00:00',
                    '01:00',
                    '02:00',
                    '03:00',
                    '04:00',
                    '05:00',
                    '06:00',
                    '07:00',
                    '08:00',
                    '09:00',
                    '10:00',
                    '11:00',
                    '12:00',
                    '13:00',
                    '14:00',
                    '15:00',
                    '16:00',
                    '17:00',
                    '18:00',
                    '19:00',
                    '20:00',
                    '21:00',
                    '22:00',
                    '23:00',
                    '00:00',
                    '01:00',
                    '02:00']]
  }

  @Unroll('_032 : #testCase')
  def '_032'() {
    given:
      setIn(input)
    when:
      _032.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          '6\n' +
              '0 200\n' +
              '1 240\n' +
              '0 120\n' +
              '3 460\n' +
              '1 240\n' +
              '2 3200',
          '24\n' +
              '1 4750\n' +
              '2 2860\n' +
              '2 8420\n' +
              '1 4520\n' +
              '0 2450\n' +
              '1 3540\n' +
              '1 4960\n' +
              '1 590\n' +
              '3 2160\n' +
              '3 9160\n' +
              '1 7900\n' +
              '3 8730\n' +
              '0 9450\n' +
              '1 8940\n' +
              '1 8680\n' +
              '0 4530\n' +
              '0 4420\n' +
              '1 2320\n' +
              '3 7960\n' +
              '0 2110\n' +
              '0 2020\n' +
              '2 3650\n' +
              '0 6280\n' +
              '2 3270']
      expected << [[95], [3590]]
  }

  @Unroll('_033 : #testCase')
  def '_033'() {
    given:
      setIn(input)
    when:
      _033.main()
    then:
      results == expected
    where:
      testCase | input                                       || expected
      'input1' | '5\nball\nstrike\nball\nstrike\nstrike'     || ['ball!', 'strike!', 'ball!', 'strike!', 'out!']
      'input2' | '6\nball\nstrike\nball\nball\nstrike\nball' || ['ball!', 'strike!', 'ball!', 'ball!', 'strike!', 'fourball!']
  }

  @Unroll('_034 : #testCase')
  def '_034'() {
    given:
      setIn(input)
    when:
      _034.main()
    then:
      results == expected
    where:
      testCase | input       || expected
      'input1' | '1 + 3 = x' || [4]
      'input2' | '6 - x = 3' || [3]
      'input3' | 'x - 1 = 5' || [6]
  }

  @Unroll('_035 : #testCase')
  def '_035'() {
    given:
      setIn(input)
    when:
      _035.main()
    then:
      results == expected
    where:
      testCase << ['input1', 'input2']
      input << [
          '5\n' +
              's 70 78 82 57 74\n' +
              'l 68 81 81 60 78\n' +
              's 63 76 55 80 75\n' +
              's 90 100 96 10 10\n' +
              'l 88 78 81 97 93',
          '20\n' +
              'l 100 67 39 85 87\n' +
              's 38 75 75 45 90\n' +
              'l 43 95 7 35 49\n' +
              'l 82 77 74 35 44\n' +
              's 96 80 92 58 84\n' +
              'l 23 60 44 27 3\n' +
              'l 42 24 52 23 63\n' +
              's 44 78 98 51 10\n' +
              'l 93 38 73 88 12\n' +
              'l 34 29 43 48 61\n' +
              'l 83 33 97 3 59\n' +
              'l 24 84 22 35 33\n' +
              's 81 42 80 34 87\n' +
              'l 8 87 82 80 100\n' +
              'l 48 75 75 3 50\n' +
              'l 93 76 25 71 31\n' +
              's 60 92 64 66 11\n' +
              'l 61 47 6 21 83\n' +
              'l 68 1 47 81 78\n' +
              'l 8 72 54 20 25']
      expected << [[2], [3]]
  }

  @Unroll('_036 : #testCase')
  def '_036'() {
    given:
      setIn(input)
    when:
      _036.main()
    then:
      results == expected
    where:
      testCase | input                                || expected
      'input1' | '1 3\n2 4\n988 876 921 906\n866 911' || [2, 3]
      'input2' | '3 2\n4 1\n833 897 901 925\n870 855' || [2, 1]
  }

  @Unroll('_037 : #testCase')
  def '_037'() {
    given:
      setIn(input)
    when:
      _037.main()
    then:
      results == expected
    where:
      testCase | input         || expected
      'input1' | '01/27 24:30' || ['01/28 00:30']
      'input2' | '02/31 73:59' || ['02/34 01:59']
      'input3' | '12/31 00:00' || ['12/31 00:00']
      'input4' | '12/31 25:01' || ['12/32 01:01']
  }

  @Unroll('_038 : #testCase')
  def '_038'() {
    given:
      setIn(input)
    when:
      _038.main()
    then:
      results == expected
    where:
      testCase | input                   || expected
      'input1' | '3 7\n2\n3\n4'          || [2]
      'input2' | '5 15\n12\n13\n7\n5\n8' || [4]
  }

  @Unroll('_039 : #testCase')
  def '_039'() {
    given:
      setIn(input)
    when:
      _039.main()
    then:
      results == expected
    where:
      testCase | input                                   || expected
      'input1' | '///+////'                              || [7]
      'input2' | '<///////+<<</+////'                    || [52]
      'input3' | '<<<<<<<<</////////+<<<<<<<<</////////' || [198]
  }

  @Unroll('_040 : #testCase')
  def '_040'() {
    given:
      setIn(input)
    when:
      _040.main()
    then:
      results == expected
    where:
      testCase | input                                                                                                                                                      || expected
      'input1' | '6\nle 120.3\nge 115.7\nle 122.0\nge 116.9\nle 119.1\nle 117.6'                                                                                            || ['116.9 117.6']
      'input2' | '15\nge 121.7\nge 125.0\nle 162.4\nle 153.5\nge 119.6\nle 182.4\nle 149.4\nle 192.7\nle 168.8\nge 110.0\nle 180.9\nge 119.9\nle 152.7\nle 180.8\nle 152.4' || ['125.0 149.4']
  }

  @Unroll('_041 : #testCase')
  def '_041'() {
    given:
      setIn(input)
    when:
      _041.main()
    then:
      results == expected
    where:
      testCase | input                                                                                                   || expected
      'input1' | '6\n3 5 9\n15 20 35\n30 45 72\n15 20 31\n27 33 59\n27 35 77'                                            || ['30 45 72', '27 35 77', '27 33 59', '15 20 35', '15 20 31', '3 5 9']
      'input2' | '10\n28 33 59\n14 18 28\n28 36 38\n2 42 73\n22 52 81\n21 58 71\n23 57 82\n28 33 59\n16 16 19\n16 47 92' || ['28 36 38', '28 33 59', '28 33 59', '23 57 82', '22 52 81', '21 58 71', '16 47 92', '16 16 19', '14 18 28', '2 42 73']
  }

  @Unroll('_042 : #testCase')
  def '_042'() {
    given:
      setIn(input)
    when:
      _042.main()
    then:
      results == expected
    where:
      testCase | input                                                 || expected
      'input1' | '3\n1 3\n2 1\n2 3'                                    || ['- L W', 'W - W', 'L L -']
      'input2' | '5\n5 2\n1 4\n2 3\n3 4\n1 5\n2 4\n1 2\n5 3\n1 3\n5 4' || ['- W W W W', 'L - W W L', 'L L - W L', 'L L L - L', 'L W W W -']
  }

  @Unroll('_043 : #testCase')
  def '_043'() {
    given:
      setIn(input)
    when:
      _043.main()
    then:
      results == expected
    where:
      testCase | input          || expected
      'input1' | '5\n1 1 2 2 3' || ['1 2']
      'input2' | '5\n1 2 3 1 1' || ['1']
  }
}
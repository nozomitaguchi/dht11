package utils

import spock.lang.Specification


class BaseSpecification extends Specification {

  // 出力結果
  def results = []

  /**
   * テスト毎の設定を行う．
   */
  def setup() {
    setOut()
  }

  /**
   * 入力値の設定を行う．
   * @param input 入力値
   */
  def setIn(String input) {
    System.setIn(new ByteArrayInputStream(input.getBytes()))
  }

  /**
   * 出力値をリストに詰める．
   */
  def setOut() {
    def stream = new PrintStream('_') {
      @Override
      void println() {
        results += '\n'
      }

      @Override
      void println(boolean x) {
        results += x
      }

      @Override
      void println(char x) {
        results += x
      }

      @Override
      void println(int x) {
        results += x
      }

      @Override
      void println(long x) {
        results += x
      }

      @Override
      void println(float x) {
        results += x
      }

      @Override
      void println(double x) {
        results += x
      }

      @Override
      void println(char[] x) {
        results += x
      }

      @Override
      void println(String x) {
        results += x
      }

      @Override
      void println(Object x) {
        results += x
      }
    }
    System.setOut(stream)
  }
}
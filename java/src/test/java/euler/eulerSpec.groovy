package euler

import spock.lang.Unroll
import utils.BaseSpecification

class eulerSpec extends BaseSpecification {

  @Unroll('_0001 : #testCase')
  def '_0001'() {
    given:
      setIn(input)
    when:
      _0001.main()
    then:
      results == expected
    where:
      testCase | input  || expected
      'input1' | '1000' || [233168]
  }

  @Unroll('_0002 : #testCase')
  def '_0002'() {
    given:
      setIn(input)
    when:
      _0002.main()
    then:
      results == expected
    where:
      testCase | input     || expected
      'input1' | '4000000' || [4613732]
  }

  @Unroll('_0003 : #testCase')
  def '_0003'() {
    given:
      setIn(input)
    when:
      _0003.main()
    then:
      results == expected
    where:
      testCase | input          || expected
      'input1' | '13195'        || [29]
      'input2' | '600851475143' || [6857]
  }

  @Unroll('_0004 : #testCase')
  def '_0004'() {
    given:
      setIn(input)
    when:
      _0004.main()
    then:
      results == expected
    where:
      testCase | input     || expected
      'input1' | '100 999' || [906609]
      'input2' | '1 2000'  || [3848483]
      'input3' | '1 10000' || [99000099]
  }

}
package daily_coding_problem

import spock.lang.Unroll
import utils.BaseSpecification

class DailyCodingProblemSpec extends BaseSpecification {

  @Unroll('_0001 : #testCase')
  def '_0001'() {
    given:
      setIn(input)
    when:
      _0001.main()
    then:
      results == expected
    where:
      testCase | input           || expected
      'input1' | '1, 2, 3, 4, 5' || ['1, 5, 2, 4, 3']
      'input2' | '1, 2, 3, 4'    || ['1, 4, 2, 3']
  }

  @Unroll('_0001free : #testCase')
  def '_0001free'() {
    given:
      setIn(input)
    when:
      _0001free.main()
    then:
      results == expected
    where:
      testCase | input           || expected
      'input1' | '1, 2, 3, 4, 5' || ['1, 5, 2, 4, 3']
      'input2' | '1, 2, 3, 4'    || ['1, 4, 2, 3']
  }

  @Unroll('_0002 : #testCase')
  def '_0002'() {
    given:
      setIn(input)
    when:
      _0002.main()
    then:
      results == expected
    where:
      testCase | input           || expected
      'input1' | '1, 2, 3, 4, 5' || ['120, 60, 40, 30, 24']
      'input2' | '3, 2, 1'       || ['2, 3, 6']
  }

  @Unroll('_0002free : #testCase')
  def '_0002free'() {
    given:
      setIn(input)
    when:
      _0002free.main()
    then:
      results == expected
    where:
      testCase | input           || expected
      'input1' | '1, 2, 3, 4, 5' || ['120, 60, 40, 30, 24']
      'input2' | '3, 2, 1'       || ['2, 3, 6']
  }

}

name := "scala"

version := "0.1"

scalaVersion := "2.12.3"

libraryDependencies ++= Seq(
  "org.json4s" %% "json4s-native" % "3.5.3",
  "org.specs2" %% "specs2-core" % "4.0.1" % "compile;test",
  "org.specs2" %% "specs2-mock" % "4.0.1" % "test"
)
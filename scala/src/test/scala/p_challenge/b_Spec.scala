package p_challenge

import p_challenge.b._
import utils.BaseSpecs2Specification


class b_Spec extends BaseSpecs2Specification {

  "_004" should {
    val applyWith = when(target = _004.main, _: String)
    "input1" >> {
      val input =
        """192.168.186.70
          |3
          |192.168.110.238 - - [10/Jul/2013:18:40:43 +0900] "GET /top.html HTTP/1.1" 404 8922 "http://gi-no.jp" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36"
          |192.168.186.70 - - [10/Jul/2013:18:52:12 +0900] "GET /top.html HTTP/1.1" 404 3628 "http://facebook.com" "Mozilla/5.0 (Windows NT 5.1; rv:22.0) Gecko/20100101 Firefox/22.0"
          |192.168.105.56 - - [10/Jul/2013:20:13:52 +0900] "GET /top.html HTTP/1.1" 200 1863 "http://paiza.jp" "Mozilla/5.0 (iPad; CPU OS 6_0_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A523 Safari/8536.25"
          |""".stripMargin
      val expected =
        """192.168.186.70 10/Jul/2013:18:52:12 /top.html
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """192.168.[0-100].*
          |3
          |192.168.99.112 - - [10/Jul/2013:13:53:15 +0900] "GET /top.html HTTP/1.1" 404 1426 "http://facebook.com" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36"
          |192.168.81.20 - - [10/Jul/2013:15:06:33 +0900] "GET /hogehoge.html HTTP/1.1" 404 4374 "http://paiza.jp" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36"
          |192.168.223.58 - - [10/Jul/2013:21:32:01 +0900] "GET /hoge.html HTTP/1.1" 304 6601 "http://paiza.jp" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:22.0) Gecko/20100101 Firefox/22.0"
          |""".stripMargin
      val expected =
        """192.168.99.112 10/Jul/2013:13:53:15 /top.html
          |192.168.81.20 10/Jul/2013:15:06:33 /hogehoge.html
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_006" should {
    val applyWith = when(target = _006.main, _: String)
    "input1" >> {
      val input =
        """10 10 10
          |10 10 10
          |""".stripMargin
      val expected =
        """Hit 3.3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """10 15 45
          |10 10 10
          |""".stripMargin
      val expected =
        """Miss
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_008" should {
    val applyWith = when(target = _008.main, _: String)
    "input1" >> {
      val input =
        """3 5
          |3 0 -2
          |1 5 1
          |-4 1 0
          |-1 -2 3
          |0 0 2
          |""".stripMargin
      val expected =
        """10
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """4 7
          |12 0 -19 -12
          |-7 -5 7 -17
          |7 -17 -10 10
          |-17 -4 -6 -12
          |-4 -9 -7 8
          |2 -15 -7 -7
          |0 -15 1 10
          |""".stripMargin
      val expected =
        """0
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """5 30
          |-70 -35 19 90 -8
          |-69 4 -54 5 -50
          |63 -81 -17 16 -28
          |93 -33 -21 78 11
          |6 -92 -90 -39 29
          |-61 -38 85 -56 47
          |-62 -74 61 4 -57
          |7 -58 19 96 99
          |89 -24 -78 -75 -6
          |97 11 -68 68 71
          |72 8 83 42 55
          |50 -6 11 59 -36
          |52 -97 -45 1 -69
          |14 9 -80 -78 38
          |-54 -33 78 -28 -6
          |15 91 67 20 -20
          |9 58 -16 95 -88
          |59 45 -11 59 84
          |99 -43 77 79 -37
          |-58 -80 -77 38 -91
          |-49 12 -47 -67 31
          |-20 62 -70 34 38
          |-90 -46 5 91 -63
          |5 79 80 -44 -61
          |60 68 29 -65 -36
          |-38 -83 -40 43 96
          |-20 -98 -71 6 85
          |95 15 36 -82 41
          |-74 -99 26 96 -70
          |-83 94 51 -36 -32
          |""".stripMargin
      val expected =
        """1714
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_009" should {
    val applyWith = when(target = _009.main, _: String)
    "input1" >> {
      val input =
        """3
          |jobs 45
          |gates 60
          |larry 15
          |""".stripMargin
      val expected =
        """10:00 - 10:45 jobs
          |10:55 - 11:55 gates
          |12:55 - 13:10 larry
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """4
          |r 51
          |yshwgfdci 12
          |elqbndnbye 54
          |lg 47
          |""".stripMargin
      val expected =
        """10:00 - 10:51 r
          |11:01 - 11:13 yshwgfdci
          |12:13 - 13:07 elqbndnbye
          |13:17 - 14:04 lg
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """2
          |vkdjh 21
          |yoboqmetph 37
          |""".stripMargin
      val expected =
        """10:00 - 10:21 vkdjh
          |10:31 - 11:08 yoboqmetph
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

}
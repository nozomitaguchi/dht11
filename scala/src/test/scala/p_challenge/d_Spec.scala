package p_challenge

import p_challenge.d._
import utils.BaseSpecs2Specification

class d_Spec extends BaseSpecs2Specification {

  "_002" should {
    val applyWith = when(target = _002.main, _: String)
    "input1" >> {
      val input =
        """10 20
          |""".stripMargin
      val expected =
        """20
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """3 3
          |""".stripMargin
      val expected =
        """eq
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_003" should {
    val applyWith = when(target = _003.main, _: String)
    "input1" >> {
      val input =
        """4
          |""".stripMargin
      val expected =
        """4 8 12 16 20 24 28 32 36
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """99
          |""".stripMargin
      val expected =
        """99 198 297 396 495 594 693 792 891
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_004" should {
    val applyWith = when(target = _004.main, _: String)
    "input1" >> {
      val input =
        """2
          |Paiza
          |Gino
          |""".stripMargin
      val expected =
        """Hello Paiza,Gino.
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """5
          |Alice
          |Bob
          |Carol
          |Dave
          |Ellen
          |""".stripMargin
      val expected =
        """Hello Alice,Bob,Carol,Dave,Ellen.
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_005" should {
    val applyWith = when(target = _005.main, _: String)
    "input1" >> {
      val input =
        """3 3
          |""".stripMargin
      val expected =
        """3 6 9 12 15 18 21 24 27 30
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """5 10
          |""".stripMargin
      val expected =
        """5 15 25 35 45 55 65 75 85 95
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """1 3
          |""".stripMargin
      val expected =
        """1 4 7 10 13 16 19 22 25 28
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_006" should {
    val applyWith = when(target = _006.main, _: String)
    "input1" >> {
      val input =
        """54 km
          |""".stripMargin
      val expected =
        """54000000
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """2 cm
          |""".stripMargin
      val expected =
        """20
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """12 m
          |""".stripMargin
      val expected =
        """12000
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_007" should {
    val applyWith = when(target = _007.main, _: String)
    "input1" >> {
      val input =
        """4
          |""".stripMargin
      val expected =
        """****
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """6
          |""".stripMargin
      val expected =
        """******
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """2
          |""".stripMargin
      val expected =
        """**
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_008" should {
    val applyWith = when(target = _008.main, _: String)
    "input1" >> {
      val input =
        """4
          |""".stripMargin
      val expected =
        """even
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """5
          |""".stripMargin
      val expected =
        """odd
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """2
          |""".stripMargin
      val expected =
        """even
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_009" should {
    val applyWith = when(target = _009.main, _: String)
    "input1" >> {
      val input =
        """1999 2000
          |""".stripMargin
      val expected =
        """1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """794 1192
          |""".stripMargin
      val expected =
        """398
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_010" should {
    val applyWith = when(target = _010.main, _: String)
    "input1" >> {
      val input =
        """paiza
          |example.com
          |""".stripMargin
      val expected =
        """paiza@example.com
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """paiza.tarou2015
          |paiza.jp
          |""".stripMargin
      val expected =
        """paiza.tarou2015@paiza.jp
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_011" should {
    val applyWith = when(target = _011.main, _: String)
    "input1" >> {
      val input =
        """C
          |""".stripMargin
      val expected =
        """3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """X
          |""".stripMargin
      val expected =
        """24
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_012" should {
    val applyWith = when(target = _012.main, _: String)
    "input1" >> {
      val input =
        """-1
          |""".stripMargin
      val expected =
        """1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """15
          |""".stripMargin
      val expected =
        """15
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_013" should {
    val applyWith = when(target = _013.main, _: String)
    "input1" >> {
      val input =
        """10 3
          |""".stripMargin
      val expected =
        """3 1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """12 14
          |""".stripMargin
      val expected =
        """0 12
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_014" should {
    val applyWith = when(target = _014.main, _: String)
    "input1" >> {
      val input =
        """paiza
          |""".stripMargin
      val expected =
        """PAIZA
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """abcdefghijklmnopqrstuvwxyz
          |""".stripMargin
      val expected =
        """ABCDEFGHIJKLMNOPQRSTUVWXYZ
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_015" should {
    val applyWith = when(target = _015.main, _: String)
    "input1" >> {
      val input =
        """3
          |""".stripMargin
      val expected =
        """3
          |2
          |1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """10
          |""".stripMargin
      val expected =
        """10
          |9
          |8
          |7
          |6
          |5
          |4
          |3
          |2
          |1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_016" should {
    val applyWith = when(target = _016.main, _: String)
    "input1" >> {
      val input =
        """aabbccdd
          |5
          |""".stripMargin
      val expected =
        """aabbc
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """oiuytrer
          |8
          |""".stripMargin
      val expected =
        """oiuytrer
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_017" should {
    val applyWith = when(target = _017.main, _: String)
    "input1" >> {
      val input =
        """9
          |12
          |3
          |6
          |10
          |""".stripMargin
      val expected =
        """12
          |3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """1
          |1
          |2
          |2
          |3
          |""".stripMargin
      val expected =
        """3
          |1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_019" should {
    val applyWith = when(target = _019.main, _: String)
    "input1" >> {
      val input =
        """paiza 3
          |""".stripMargin
      val expected =
        """i
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """abcdefghij 5
          |""".stripMargin
      val expected =
        """e
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_021" should {
    val applyWith = when(target = _021.main, _: String)
    "input1" >> {
      val input =
        """paiza
          |paiza
          |""".stripMargin
      val expected =
        """Yes
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """gino
          |paiza
          |""".stripMargin
      val expected =
        """No
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_022" should {
    val applyWith = when(target = _022.main, _: String)
    "input1" >> {
      val input =
        """4
          |""".stripMargin
      val expected =
        """96
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """1
          |""".stripMargin
      val expected =
        """6
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_023" should {
    val applyWith = when(target = _023.main, _: String)
    "input1" >> {
      val input =
        """PAIZA
          |""".stripMargin
      val expected =
        """2
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """aAaAaAa
          |""".stripMargin
      val expected =
        """3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_024" should {
    val applyWith = when(target = _024.main, _: String)
    "input1" >> {
      val input =
        """60
          |90
          |""".stripMargin
      val expected =
        """30
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """45
          |45
          |""".stripMargin
      val expected =
        """90
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_025" should {
    val applyWith = when(target = _025.main, _: String)
    "input1" >> {
      val input =
        """98
          |""".stripMargin
      val expected =
        """098
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """2
          |""".stripMargin
      val expected =
        """002
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_026" should {
    val applyWith = when(target = _026.main, _: String)
    "input1" >> {
      val input =
        """yes
          |yes
          |yes
          |yes
          |no
          |no
          |yes
          |""".stripMargin
      val expected =
        """2
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """yes
          |no
          |no
          |no
          |no
          |no
          |yes
          |""".stripMargin
      val expected =
        """5
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_027" should {
    val applyWith = when(target = _027.main, _: String)
    "input1" >> {
      val input =
        """10
          |""".stripMargin
      val expected =
        """55
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """99
          |""".stripMargin
      val expected =
        """4950
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_027" should {
    val applyWith = when(target = _027.main, _: String)
    "input1" >> {
      val input =
        """10
          |""".stripMargin
      val expected =
        """55
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """99
          |""".stripMargin
      val expected =
        """4950
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_028" should {
    val applyWith = when(target = _028.main, _: String)
    "input1" >> {
      val input =
        """100
          |""".stripMargin
      val expected =
        """3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """4304
          |""".stripMargin
      val expected =
        """4
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_029" should {
    val applyWith = when(target = _029.main, _: String)
    "input1" >> {
      val input =
        """3
          |""".stripMargin
      val expected =
        """4
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """1
          |""".stripMargin
      val expected =
        """6
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_031" should {
    val applyWith = when(target = _031.main, _: String)
    "input1" >> {
      val input =
        """16
          |""".stripMargin
      val expected =
        """960
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """3
          |""".stripMargin
      val expected =
        """180
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_032" should {
    val applyWith = when(target = _032.main, _: String)
    "input1" >> {
      val input =
        """70
          |""".stripMargin
      val expected =
        """30
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """25
          |""".stripMargin
      val expected =
        """75
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_033" should {
    val applyWith = when(target = _033.main, _: String)
    "input1" >> {
      val input =
        """Midorikawa Tsubame
          |""".stripMargin
      val expected =
        """M.T
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """Paiza Tarou
          |""".stripMargin
      val expected =
        """P.T
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_034" should {
    val applyWith = when(target = _034.main, _: String)
    "input1" >> {
      val input =
        """4
          |""".stripMargin
      val expected =
        """1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """3
          |""".stripMargin
      val expected =
        """3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_035" should {
    val applyWith = when(target = _035.main, _: String)
    "input1" >> {
      val input =
        """2016 2 12
          |""".stripMargin
      val expected =
        """2016/2/12
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """2000 1 1
          |""".stripMargin
      val expected =
        """2000/1/1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_036" should {
    val applyWith = when(target = _036.main, _: String)
    "input1" >> {
      val input =
        """paizaatcodeattest
          |""".stripMargin
      val expected =
        """paiza@code@test
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """atatatpaizaatatat
          |""".stripMargin
      val expected =
        """@@@paiza@@@
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_037" should {
    val applyWith = when(target = _037.main, _: String)
    "input1" >> {
      val input =
        """7
          |30
          |""".stripMargin
      val expected =
        """5
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """3
          |100
          |""".stripMargin
      val expected =
        """34
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_038" should {
    val applyWith = when(target = _038.main, _: String)
    "input1" >> {
      val input =
        """4
          |""".stripMargin
      val expected =
        """6
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """10
          |""".stripMargin
      val expected =
        """45
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_039" should {
    val applyWith = when(target = _039.main, _: String)
    "input1" >> {
      val input =
        """14
          |14
          |14
          |""".stripMargin
      val expected =
        """YES
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """5
          |6
          |5
          |""".stripMargin
      val expected =
        """NO
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_040" should {
    val applyWith = when(target = _040.main, _: String)
    "input1" >> {
      val input =
        """13
          |0
          |15
          |30
          |89
          |100
          |31
          |""".stripMargin
      val expected =
        """4
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """0
          |14
          |18
          |22
          |0
          |2
          |4
          |""".stripMargin
      val expected =
        """7
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_041" should {
    val applyWith = when(target = _041.main, _: String)
    "input1" >> {
      val input =
        """400 5 85
          |""".stripMargin
      val expected =
        """OK
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """600 4 70
          |""".stripMargin
      val expected =
        """NG
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_043" should {
    val applyWith = when(target = _043.main, _: String)
    "input1" >> {
      val input =
        """31
          |""".stripMargin
      val expected =
        """cloudy
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """2
          |""".stripMargin
      val expected =
        """sunny
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_044" should {
    val applyWith = when(target = _044.main, _: String)
    "input1" >> {
      val input =
        """Noda F
          |""".stripMargin
      val expected =
        """Hi, Ms. Noda
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """Paiza M
          |""".stripMargin
      val expected =
        """Hi, Mr. Paiza
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_045" should {
    val applyWith = when(target = _045.main, _: String)
    "input1" >> {
      val input =
        """5
          |""".stripMargin
      val expected =
        """A
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """3
          |""".stripMargin
      val expected =
        """C
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_046" should {
    val applyWith = when(target = _046.main, _: String)
    "input1" >> {
      val input =
        """1 3 5
          |""".stripMargin
      val expected =
        """5
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """2 5 5
          |""".stripMargin
      val expected =
        """5
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_047" should {
    val applyWith = when(target = _047.main, _: String)
    "input1" >> {
      val input =
        """Ono
          |Orujov
          |Shavdatuashvili
          |""".stripMargin
      val expected =
        """Gold Ono
          |Silver Orujov
          |Bronze Shavdatuashvili
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """Ogino
          |Kalisz
          |Seto
          |""".stripMargin
      val expected =
        """Gold Ogino
          |Silver Kalisz
          |Bronze Seto
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_048" should {
    val applyWith = when(target = _048.main, _: String)
    "input1" >> {
      val input =
        """5
          |8
          |19
          |25
          |31
          |""".stripMargin
      val expected =
        """3
          |11
          |6
          |6
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """2
          |3
          |7
          |9
          |28
          |""".stripMargin
      val expected =
        """1
          |4
          |2
          |19
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_049" should {
    val applyWith = when(target = _049.main, _: String)
    "input1" >> {
      val input =
        """korakunoaki
          |""".stripMargin
      val expected =
        """koraku
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """dokusyo
          |""".stripMargin
      val expected =
        """dokusyo
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """minorinoaki
          |""".stripMargin
      val expected =
        """minori
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input4" >> {
      val input =
        """yakiniku
          |""".stripMargin
      val expected =
        """yakiniku
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_050" should {
    val applyWith = when(target = _050.main, _: String)
    "input1" >> {
      val input =
        """3 50
          |""".stripMargin
      val expected =
        """8
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """100 500
          |""".stripMargin
      val expected =
        """10
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """2 4
          |""".stripMargin
      val expected =
        """6
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_051" should {
    val applyWith = when(target = _051.main, _: String)
    "input1" >> {
      val input =
        """W W W W W S S S S S
          |""".stripMargin
      val expected =
        """OK
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """S S W W S W S S S S
          |""".stripMargin
      val expected =
        """NG
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """W S S S S S S S S W
          |""".stripMargin
      val expected =
        """NG
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_052" should {
    val applyWith = when(target = _052.main, _: String)
    "input1" >> {
      val input =
        """4
          |""".stripMargin
      val expected =
        """10
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """10
          |""".stripMargin
      val expected =
        """55
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """50
          |""".stripMargin
      val expected =
        """1275
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_053" should {
    val applyWith = when(target = _053.main, _: String)
    "input1" >> {
      val input =
        """chocolate
          |""".stripMargin
      val expected =
        """Thanks!
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """candy
          |""".stripMargin
      val expected =
        """Thanks!
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """juice
          |""".stripMargin
      val expected =
        """No!
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_054" should {
    val applyWith = when(target = _054.main, _: String)
    "input1" >> {
      val input =
        """11111111111
          |""".stripMargin
      val expected =
        """OK
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """1111
          |""".stripMargin
      val expected =
        """7
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """111111111111111111
          |""".stripMargin
      val expected =
        """OK
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_055" should {
    val applyWith = when(target = _055.main, _: String)
    "input1" >> {
      val input =
        """a decade
          |""".stripMargin
      val expected =
        """Best in a decade
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """the world
          |""".stripMargin
      val expected =
        """Best in the world
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """history ever
          |""".stripMargin
      val expected =
        """Best in history ever
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_056" should {
    val applyWith = when(target = _056.main, _: String)
    "input1" >> {
      val input =
        """5 2
          |""".stripMargin
      val expected =
        """117
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """6 4
          |""".stripMargin
      val expected =
        """152
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """2 1
          |""".stripMargin
      val expected =
        """7
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_057" should {
    val applyWith = when(target = _057.main, _: String)
    "input1" >> {
      val input =
        """3
          |cartoy plamodel gameconsole
          |""".stripMargin
      val expected =
        """gameconsole
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """2
          |gloves muffler sweater
          |""".stripMargin
      val expected =
        """muffler
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """1
          |pizza steak sushi
          |""".stripMargin
      val expected =
        """pizza
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_058" should {
    val applyWith = when(target = _058.main, _: String)
    "input1" >> {
      val input =
        """1 1 1
          |""".stripMargin
      val expected =
        """ABA
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """5 7 5
          |""".stripMargin
      val expected =
        """AAAAABBBBBBBAAAAA
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_059" should {
    val applyWith = when(target = _059.main, _: String)
    "input1" >> {
      val input =
        """K Q
          |""".stripMargin
      val expected =
        """K Q
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """J J
          |""".stripMargin
      val expected =
        """J Q
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_060" should {
    val applyWith = when(target = _060.main, _: String)
    "input1" >> {
      val input =
        """3 4
          |""".stripMargin
      val expected =
        """-1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """80 44
          |""".stripMargin
      val expected =
        """36
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_061" should {
    val applyWith = when(target = _061.main, _: String)
    "input1" >> {
      val input =
        """3
          |""".stripMargin
      val expected =
        """9
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """0
          |""".stripMargin
      val expected =
        """1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """52
          |""".stripMargin
      val expected =
        """156
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_062" should {
    val applyWith = when(target = _062.main, _: String)
    "input1" >> {
      val input =
        """2 3 5
          |""".stripMargin
      val expected =
        """AB
          |CDE
          |FGHIJ
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """1 1 8
          |""".stripMargin
      val expected =
        """A
          |B
          |CDEFGHIJ
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """3 4 3
          |""".stripMargin
      val expected =
        """ABC
          |DEFG
          |HIJ
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_063" should {
    val applyWith = when(target = _063.main, _: String)
    "input1" >> {
      val input =
        """0 10 23 35 57
          |29
          |""".stripMargin
      val expected =
        """4
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """1 11 39 44 51
          |58
          |""".stripMargin
      val expected =
        """6
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """3 13 24 45 50
          |1
          |""".stripMargin
      val expected =
        """1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_064" should {
    val applyWith = when(target = _064.main, _: String)
    "input1" >> {
      val input =
        """if False
          |""".stripMargin
      val expected =
        """if True
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """aprilFalsefool
          |""".stripMargin
      val expected =
        """aprilTruefool
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """aprilfool
          |""".stripMargin
      val expected =
        """aprilfool
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_065" should {
    val applyWith = when(target = _065.main, _: String)
    "input1" >> {
      val input =
        """404
          |""".stripMargin
      val expected =
        """error
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """200
          |""".stripMargin
      val expected =
        """ok
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """999
          |""".stripMargin
      val expected =
        """unknown
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_066" should {
    val applyWith = when(target = _066.main, _: String)
    "input1" >> {
      val input =
        """10 13
          |""".stripMargin
      val expected =
        """3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """18 13
          |""".stripMargin
      val expected =
        """No
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """4 4
          |""".stripMargin
      val expected =
        """0
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_067" should {
    val applyWith = when(target = _067.main, _: String)
    "input1" >> {
      val input =
        """3
          |""".stripMargin
      val expected =
        """ON
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """6
          |""".stripMargin
      val expected =
        """OFF
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_068" should {
    val applyWith = when(target = _068.main, _: String)
    "input1" >> {
      val input =
        """5
          |SSRSR
          |""".stripMargin
      val expected =
        """3 2
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """10
          |SSSSSSSSSS
          |""".stripMargin
      val expected =
        """10 0
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_069" should {
    val applyWith = when(target = _069.main, _: String)
    "input1" >> {
      val input =
        """50 40 50 60 30 80 100
          |""".stripMargin
      val expected =
        """58.6
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """0 10 20 30 40 50 60
          |""".stripMargin
      val expected =
        """30.0
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """0 0 0 0 0 0 0
          |""".stripMargin
      val expected =
        """0.0
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_070" should {
    val applyWith = when(target = _070.main, _: String)
    "input1" >> {
      val input =
        """30 10
          |""".stripMargin
      val expected =
        """20
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """58 24
          |""".stripMargin
      val expected =
        """34
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_071" should {
    val applyWith = when(target = _071.main, _: String)
    "input1" >> {
      val input =
        """25 50
          |""".stripMargin
      val expected =
        """Yes
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """10 20
          |""".stripMargin
      val expected =
        """Yes
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """20 50
          |""".stripMargin
      val expected =
        """No
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_072" should {
    val applyWith = when(target = _072.main, _: String)
    "input1" >> {
      val input =
        """10 3 10000
          |""".stripMargin
      val expected =
        """40000
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """3 3 2500
          |""".stripMargin
      val expected =
        """2500
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_073" should {
    val applyWith = when(target = _073.main, _: String)
    "input1" >> {
      val input =
        """ABCDE
          |""".stripMargin
      val expected =
        """EDCBA
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """ABCBA
          |""".stripMargin
      val expected =
        """ABCBA
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

}
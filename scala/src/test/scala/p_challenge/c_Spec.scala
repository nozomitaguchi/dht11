package p_challenge

import p_challenge.c._
import utils.BaseSpecs2Specification


class c_Spec extends BaseSpecs2Specification {

  "_005" should {
    val applyWith = when(target = _005.main, _: String)
    "input1" >> {
      val input =
        """4
          |192.168.0.1
          |192.168.0.2
          |192.168.0.3
          |192.168.0.4
          |""".stripMargin
      val expected =
        """True
          |True
          |True
          |True
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """4
          |192.400.1.10.1000...
          |4.3.2.1
          |0..33.444...
          |1.2.3.4
          |""".stripMargin
      val expected =
        """False
          |True
          |False
          |True
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_006" should {
    val applyWith = when(target = _006.main, _: String)
    "input1" >> {
      val input =
        """4 10 3
          |1.5 1.2 2 0.4
          |208 200 3 99988
          |255 150 50 65472
          |103 748 39 48571
          |159 403 32 89928
          |254 300 67 78492
          |249 298 47 45637
          |253 382 89 37282
          |250 350 78 76782
          |251 371 69 67821
          |256 312 89 54382
          |""".stripMargin
      val expected =
        """40553
          |36757
          |32272
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_008" should {
    val applyWith = when(target = _008.main, _: String)
    "input1" >> {
      val input =
        """<abc> <xyz>
          |hoge<abc>piyo<xyz>
          |""".stripMargin
      val expected =
        """piyo
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """<abc> <ijk>
          |<abc>xxxx<ijk>yyyyyy<abc>zzz<ijk>
          |""".stripMargin
      val expected =
        """xxxx
          |zzz
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """<paiza> <poh>
          |<paiza><poh>
          |""".stripMargin
      val expected =
        """<blank>
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input4" >> {
      val input =
        """<Banana> <Cupcake>
          |ApplePie<Banana>Bread<Cupcake>Apple<Banana><Cupcake>
          |""".stripMargin
      val expected =
        """Bread
          |<blank>
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_010" should {
    val applyWith = when(target = _010.main, _: String)
    "input1" >> {
      val input =
        """20 10 10
          |3
          |25 10
          |20 15
          |70 70
          |""".stripMargin
      val expected =
        """noisy
          |noisy
          |silent
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """50 50 100
          |4
          |0 0
          |0 100
          |100 0
          |100 100
          |""".stripMargin
      val expected =
        """noisy
          |noisy
          |noisy
          |noisy
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_013" should {
    val applyWith = when(target = _013.main, _: String)
    "input1" >> {
      val input =
        """4
          |5
          |101
          |204
          |301
          |401
          |501
          |""".stripMargin
      val expected =
        """101
          |301
          |501
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """9
          |3
          |409
          |509
          |109
          |""".stripMargin
      val expected =
        """none
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """1
          |6
          |101
          |102
          |205
          |224
          |231
          |314
          |""".stripMargin
      val expected =
        """205
          |224
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_014" should {
    val applyWith = when(target = _014.main, _: String)
    "input1" >> {
      val input =
        """4 2
          |6 6 6
          |4 6 4
          |6 1 1
          |4 4 4
          |""".stripMargin
      val expected =
        """1
          |2
          |4
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """3 5
          |2 184 12
          |135 169 37
          |99 121 41
          |""".stripMargin
      val expected =
        """2
          |3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """5 7
          |197 78 14
          |14 197 62
          |80 14 109
          |138 145 147
          |9 130 175
          |""".stripMargin
      val expected =
        """1
          |2
          |3
          |4
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_015" should {
    val applyWith = when(target = _015.main, _: String)
    "input1" >> {
      val input =
        """3
          |1 1024
          |11 2048
          |21 4196
          |""".stripMargin
      val expected =
        """71
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """1
          |30 340
          |""".stripMargin
      val expected =
        """10
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """2
          |5 10000
          |12 10000
          |""".stripMargin
      val expected =
        """600
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_016" should {
    val applyWith = when(target = _016.main, _: String)
    "input1" >> {
      val input =
        """PAIZA
          |""".stripMargin
      val expected =
        """P4124
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """ALANTURING
          |""".stripMargin
      val expected =
        """4L4NTUR1N6
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_017" should {
    val applyWith = when(target = _017.main, _: String)
    "input1" >> {
      val input =
        """5 1
          |2
          |7 2
          |1 4
          |""".stripMargin
      val expected =
        """Low
          |High
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """7 3
          |4
          |7 1
          |7 4
          |5 3
          |10 1
          |""".stripMargin
      val expected =
        """Low
          |High
          |High
          |Low
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_018" should {
    val applyWith = when(target = _018.main, _: String)
    "input1" >> {
      val input =
        """4
          |supaisu 5
          |imo 2
          |niku 2
          |mizu 3
          |6
          |mizu 7
          |imo 4
          |ninjin 10
          |unagi 6
          |supaisu 20
          |niku 5
          |""".stripMargin
      val expected =
        """2
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """2
          |gohan 1
          |okazu 1
          |1
          |mizu 10000
          |""".stripMargin
      val expected =
        """0
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_019" should {
    val applyWith = when(target = _019.main, _: String)
    "input1" >> {
      val input =
        """3
          |28
          |16
          |777
          |""".stripMargin
      val expected =
        """perfect
          |nearly
          |neither
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """4
          |3
          |4
          |5
          |6
          |""".stripMargin
      val expected =
        """neither
          |nearly
          |neither
          |perfect
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_020" should {
    val applyWith = when(target = _020.main, _: String)
    "input1" >> {
      val input =
        """1 80 40
          |""".stripMargin
      val expected =
        """0.12
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """10 31 52
          |""".stripMargin
      val expected =
        """3.312
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_021" should {
    val applyWith = when(target = _021.main, _: String)
    "input1" >> {
      val input =
        """0 0 1 2
          |3
          |0 0
          |1 1
          |4 2
          |""".stripMargin
      val expected =
        """no
          |yes
          |no
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """47 19 57 80
          |3
          |62 -52
          |35 -70
          |-81 2
          |""".stripMargin
      val expected =
        """yes
          |no
          |no
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_022" should {
    val applyWith = when(target = _022.main, _: String)
    "input1" >> {
      val input =
        """5
          |11 14 16 10
          |12 15 17 10
          |13 11 14 11
          |12 10 13 8
          |11 13 14 10
          |""".stripMargin
      val expected =
        """11 13 17 8
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """3
          |15036 14936 15089 14919
          |15009 15073 15084 14916
          |14805 14738 14807 14672
          |""".stripMargin
      val expected =
        """15036 14738 15089 14672
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """5
          |16654 16544 16757 16543
          |16535 16321 16602 16310
          |16321 16315 16463 16273
          |16313 16141 16313 15855
          |15921 16117 16212 15918
          |""".stripMargin
      val expected =
        """16654 16117 16757 15855
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_023" should {
    val applyWith = when(target = _023.main, _: String)
    "input1" >> {
      val input =
        """1 2 3 4 5 6
          |3
          |1 5 4 2 3 6
          |9 6 2 7 1 5
          |32 9 87 33 41 60
          |""".stripMargin
      val expected =
        """6
          |4
          |0
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """72 2 90 84 57 85
          |3
          |36 70 1 72 54 82
          |36 2 40 12 3 58
          |25 11 90 57 85 99
          |""".stripMargin
      val expected =
        """1
          |1
          |3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_024" should {
    val applyWith = when(target = _024.main, _: String)
    "input1" >> {
      val input =
        """3
          |SET 1 10
          |SET 2 20
          |ADD 40
          |""".stripMargin
      val expected =
        """10 50
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """3
          |SET 1 -23
          |SUB 77
          |SET 1 0
          |""".stripMargin
      val expected =
        """0 -100
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_025" should {
    val applyWith = when(target = _025.main, _: String)
    "input1" >> {
      val input =
        """50
          |5
          |3 20 70
          |3 40 170
          |3 59 90
          |4 5 55
          |4 25 40
          |""".stripMargin
      val expected =
        """9
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """5
          |10
          |1 10 1
          |1 20 1
          |1 30 1
          |1 40 1
          |1 50 1
          |2 10 1
          |2 20 2
          |2 30 3
          |2 40 4
          |2 50 5
          |""".stripMargin
      val expected =
        """4
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_026" should {
    val applyWith = when(target = _026.main, _: String)
    "input1" >> {
      val input =
        """3 5 2
          |8 10
          |7 6
          |7 4
          |""".stripMargin
      val expected =
        """2
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """3 64 10
          |84 75
          |73 53
          |56 34
          |""".stripMargin
      val expected =
        """not found
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_028" should {
    val applyWith = when(target = _028.main, _: String)
    "input1" >> {
      val input =
        """4
          |apple aple
          |orange olange
          |grape glepe
          |lemon lemon
          |""".stripMargin
      val expected =
        """3
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """12
          |january januarry
          |february febrary
          |march march
          |april aplil
          |may may
          |june june
          |july jury
          |august ougust
          |september septenber
          |october october
          |november novembar
          |december dicembar
          |""".stripMargin
      val expected =
        """13
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_029" should {
    val applyWith = when(target = _029.main, _: String)
    "input1" >> {
      val input =
        """7 3
          |19 0
          |20 0
          |21 60
          |22 30
          |23 10
          |24 10
          |25 90
          |""".stripMargin
      val expected =
        """22 24
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """10 4
          |3 30
          |4 25
          |5 20
          |6 65
          |7 75
          |8 0
          |9 10
          |10 100
          |11 35
          |12 80
          |""".stripMargin
      val expected =
        """3 6
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_030" should {
    val applyWith = when(target = _030.main, _: String)
    "input1" >> {
      val input =
        """3 2
          |128 127
          |127 128
          |128 127
          |""".stripMargin
      val expected =
        """1 0
          |0 1
          |1 0
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """1 1
          |0
          |""".stripMargin
      val expected =
        """0
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_031" should {
    val applyWith = when(target = _031.main, _: String)
    "input1" >> {
      val input =
        """6
          |tokyo 9
          |beijing 8
          |singapore 7
          |london 0
          |newyork -5
          |osaka 9
          |singapore 19:38
          |""".stripMargin
      val expected =
        """21:38
          |20:38
          |19:38
          |12:38
          |07:38
          |21:38
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """27
          |howland -12
          |samoa -11
          |hawaii -10
          |alaska -9
          |california -8
          |arizona -7
          |texas -6
          |massachusetts -5
          |santiago -4
          |brasilia -3
          |greenland -2
          |verde -1
          |morocco 0
          |london 1
          |paris 2
          |athens 3
          |moscow 4
          |islamabad 5
          |astana 6
          |bangkok 7
          |hongkong 8
          |seoul 9
          |guam 10
          |kuril 11
          |southpole 12
          |nukualofa 13
          |tokelau 14
          |southpole 00:00
          |""".stripMargin
      val expected =
        """00:00
          |01:00
          |02:00
          |03:00
          |04:00
          |05:00
          |06:00
          |07:00
          |08:00
          |09:00
          |10:00
          |11:00
          |12:00
          |13:00
          |14:00
          |15:00
          |16:00
          |17:00
          |18:00
          |19:00
          |20:00
          |21:00
          |22:00
          |23:00
          |00:00
          |01:00
          |02:00
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_032" should {
    val applyWith = when(target = _032.main, _: String)
    "input1" >> {
      val input =
        """6
          |0 200
          |1 240
          |0 120
          |3 460
          |1 240
          |2 3200
          |""".stripMargin
      val expected =
        """95
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """24
          |1 4750
          |2 2860
          |2 8420
          |1 4520
          |0 2450
          |1 3540
          |1 4960
          |1 590
          |3 2160
          |3 9160
          |1 7900
          |3 8730
          |0 9450
          |1 8940
          |1 8680
          |0 4530
          |0 4420
          |1 2320
          |3 7960
          |0 2110
          |0 2020
          |2 3650
          |0 6280
          |2 3270
          |""".stripMargin
      val expected =
        """3590
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_033" should {
    val applyWith = when(target = _033.main, _: String)
    "input1" >> {
      val input =
        """5
          |ball
          |strike
          |ball
          |strike
          |strike
          |""".stripMargin
      val expected =
        """ball!
          |strike!
          |ball!
          |strike!
          |out!
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """6
          |ball
          |strike
          |ball
          |ball
          |strike
          |ball
          |""".stripMargin
      val expected =
        """ball!
          |strike!
          |ball!
          |ball!
          |strike!
          |fourball!
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_034" should {
    val applyWith = when(target = _034.main, _: String)
    "input1" >> {
      val input =
        """1 + 3 = x
          |""".stripMargin
      val expected =
        """4
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """6 - x = 3
          |""".stripMargin
      val expected =
        """3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """x - 1 = 5
          |""".stripMargin
      val expected =
        """6
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_035" should {
    val applyWith = when(target = _035.main, _: String)
    "input1" >> {
      val input =
        """5
          |s 70 78 82 57 74
          |l 68 81 81 60 78
          |s 63 76 55 80 75
          |s 90 100 96 10 10
          |l 88 78 81 97 93
          |""".stripMargin
      val expected =
        """2
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """20
          |l 100 67 39 85 87
          |s 38 75 75 45 90
          |l 43 95 7 35 49
          |l 82 77 74 35 44
          |s 96 80 92 58 84
          |l 23 60 44 27 3
          |l 42 24 52 23 63
          |s 44 78 98 51 10
          |l 93 38 73 88 12
          |l 34 29 43 48 61
          |l 83 33 97 3 59
          |l 24 84 22 35 33
          |s 81 42 80 34 87
          |l 8 87 82 80 100
          |l 48 75 75 3 50
          |l 93 76 25 71 31
          |s 60 92 64 66 11
          |l 61 47 6 21 83
          |l 68 1 47 81 78
          |l 8 72 54 20 25
          |""".stripMargin
      val expected =
        """3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_036" should {
    val applyWith = when(target = _036.main, _: String)
    "input1" >> {
      val input =
        """1 3
          |2 4
          |988 876 921 906
          |866 911
          |""".stripMargin
      val expected =
        """2
          |3
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """3 2
          |4 1
          |833 897 901 925
          |870 855
          |""".stripMargin
      val expected =
        """2
          |1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_037" should {
    val applyWith = when(target = _037.main, _: String)
    "input1" >> {
      val input =
        """01/27 24:30
          |""".stripMargin
      val expected =
        """01/28 00:30
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """02/31 73:59
          |""".stripMargin
      val expected =
        """02/34 01:59
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """12/31 00:00
          |""".stripMargin
      val expected =
        """12/31 00:00
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """12/31 25:01
          |""".stripMargin
      val expected =
        """12/32 01:01
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_038" should {
    val applyWith = when(target = _038.main, _: String)
    "input1" >> {
      val input =
        """3 7
          |2
          |3
          |4
          |""".stripMargin
      val expected =
        """2
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """5 15
          |12
          |13
          |7
          |5
          |8
          |""".stripMargin
      val expected =
        """4
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_039" should {
    val applyWith = when(target = _039.main, _: String)
    "input1" >> {
      val input =
        """///+////
          |""".stripMargin
      val expected =
        """7
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """<///////+<<</+////
          |""".stripMargin
      val expected =
        """52
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """<<<<<<<<</////////+<<<<<<<<</////////
          |""".stripMargin
      val expected =
        """198
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_040" should {
    val applyWith = when(target = _040.main, _: String)
    "input1" >> {
      val input =
        """6
          |le 120.3
          |ge 115.7
          |le 122.0
          |ge 116.9
          |le 119.1
          |le 117.6
          |""".stripMargin
      val expected =
        """116.9 117.6
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """15
          |ge 121.7
          |ge 125.0
          |le 162.4
          |le 153.5
          |ge 119.6
          |le 182.4
          |le 149.4
          |le 192.7
          |le 168.8
          |ge 110.0
          |le 180.9
          |ge 119.9
          |le 152.7
          |le 180.8
          |le 152.4
          |""".stripMargin
      val expected =
        """125.0 149.4
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_041" should {
    val applyWith = when(target = _041.main, _: String)
    "input1" >> {
      val input =
        """6
          |3 5 9
          |15 20 35
          |30 45 72
          |15 20 31
          |27 33 59
          |27 35 77
          |""".stripMargin
      val expected =
        """30 45 72
          |27 35 77
          |27 33 59
          |15 20 35
          |15 20 31
          |3 5 9
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """10
          |28 33 59
          |14 18 28
          |28 36 38
          |2 42 73
          |22 52 81
          |21 58 71
          |23 57 82
          |28 33 59
          |16 16 19
          |16 47 92
          |""".stripMargin
      val expected =
        """28 36 38
          |28 33 59
          |28 33 59
          |23 57 82
          |22 52 81
          |21 58 71
          |16 47 92
          |16 16 19
          |14 18 28
          |2 42 73
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_042" should {
    val applyWith = when(target = _042.main, _: String)
    "input1" >> {
      val input =
        """3
          |1 3
          |2 1
          |2 3
          |""".stripMargin
      val expected =
        """- L W
          |W - W
          |L L -
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """5
          |5 2
          |1 4
          |2 3
          |3 4
          |1 5
          |2 4
          |1 2
          |5 3
          |1 3
          |5 4
          |""".stripMargin
      val expected =
        """- W W W W
          |L - W W L
          |L L - W L
          |L L L - L
          |L W W W -
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_043" should {
    val applyWith = when(target = _043.main, _: String)
    "input1" >> {
      val input =
        """5
          |1 1 2 2 3
          |""".stripMargin
      val expected =
        """1 2
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """5
          |1 2 3 1 1
          |""".stripMargin
      val expected =
        """1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

}
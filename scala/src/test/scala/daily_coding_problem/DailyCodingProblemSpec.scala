package daily_coding_problem

import utils.BaseSpecs2Specification

class DailyCodingProblemSpec extends BaseSpecs2Specification {

  "_0001" should {

    val applyWith = when(target = _0001.main, _: String)

    "input1" >> {
      val input = "1, 2, 3, 4, 5"
      val expected = "1, 5, 2, 4, 3"
      applyWith(input) mustEqual expected
    }

    "input2" >> {
      val input = "1, 2, 3, 4"
      val expected = "1, 4, 2, 3"
      applyWith(input) mustEqual expected
    }

  }

  "_0001_2" should {

    val applyWith = when(target = _0001_2.main, _: String)

    "input1" >> {
      val input = "1, 2, 3, 4, 5"
      val expected = "1, 5, 2, 4, 3"
      applyWith(input) mustEqual expected
    }

    "input2" >> {
      val input = "1, 2, 3, 4"
      val expected = "1, 4, 2, 3"
      applyWith(input) mustEqual expected
    }

  }

  "_0002" should {

    val applyWith = when(target = _0002.main, _: String)

    "input1" >> {
      val input = "1, 2, 3, 4, 5"
      val expected = "120, 60, 40, 30, 24"
      applyWith(input) mustEqual expected
    }

    "input2" >> {
      val input = "3, 2, 1"
      val expected = "2, 3, 6"
      applyWith(input) mustEqual expected
    }

  }

}

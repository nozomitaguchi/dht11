package paiza.learning.d

import utils.BaseSpecs2Specification

class AdditionSpec extends BaseSpecs2Specification {

  "Addition" should {
    val applyWith = when(target = Addition.main, _: String)
    "input1" >> {
      val input =
        """1 1
          |""".stripMargin
      val expected =
        """2
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """0 99
          |""".stripMargin
      val expected =
        """99
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }
}

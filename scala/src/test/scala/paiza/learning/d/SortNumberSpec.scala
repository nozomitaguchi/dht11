package paiza.learning.d

import utils.BaseSpecs2Specification

class SortNumberSpec extends BaseSpecs2Specification {

  "SortNumber" should {
    val applyWith = when(target = SortNumber.main, _: String)
    "input1" >> {
      val input =
        """5
          |3
          |5
          |19
          |1
          |2
          |""".stripMargin
      val expected =
        """1
          |2
          |3
          |5
          |19
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """10
          |2
          |3
          |4
          |5
          |4
          |3
          |2
          |1
          |1
          |8
          |""".stripMargin
      val expected =
        """1
          |1
          |2
          |2
          |3
          |3
          |4
          |4
          |5
          |8
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }
}

package paiza.learning.d

import utils.BaseSpecs2Specification

class MultiplicationSpec extends BaseSpecs2Specification {

  "Multiplication" should {
    val applyWith = when(target = Multiplication.main, _: String)
    "input1" >> {
      val input =
        """2
          |3
          |""".stripMargin
      val expected =
        """6
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """0
          |99
          |""".stripMargin
      val expected =
        """0
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }
}

package paiza.learning.b

import utils.BaseSpecs2Specification

class LongTableSpec extends BaseSpecs2Specification {

  "LongTable" should {
    val applyWith = when(target = LongTable.main, _: String)
    "input1" >> {
      val input =
        """6 3
          |3 2
          |1 6
          |2 5
          |""".stripMargin
      val expected =
        """4
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """12 6
          |4 6
          |4 8
          |4 10
          |4 12
          |4 2
          |4 4
          |""".stripMargin
      val expected =
        """12
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }
}

package paiza.learning.a

import utils.BaseSpecs2Specification

class JankenSpec extends BaseSpecs2Specification {

  "Janken" should {
    val applyWith = when(target = Janken.main, _: String)
    "input1" >> {
      val input =
        """4 7
          |CGPC
          |""".stripMargin
      val expected =
        """4
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """5 10
          |GPCPC
          |""".stripMargin
      val expected =
        """3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input3" >> {
      val input =
        """1 0
          |C
          |""".stripMargin
      val expected =
        """1
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input4" >> {
      val input =
        """10 0
          |CCCCCCCCCC
          |""".stripMargin
      val expected =
        """10
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input5" >> {
      val input =
        """10 0
          |PPPPPPPPPP
          |""".stripMargin
      val expected =
        """0
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input6" >> {
      val input =
        """10 0
          |GGGGGGGGGG
          |""".stripMargin
      val expected =
        """0
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input7" >> {
      val input =
        """15 59
          |GGGGGGGGGGGPPPP
          |""".stripMargin
      val expected =
        """13
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input8" >> {
      val input =
        """10 33
          |GGGGGPPPPP
          |""".stripMargin
      val expected =
        """9
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input9" >> {
      val input =
        """10 23
          |GGGPPPPPPP
          |""".stripMargin
      val expected =
        """8
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }
}
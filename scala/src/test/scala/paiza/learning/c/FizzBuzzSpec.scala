package paiza.learning.c

import utils.BaseSpecs2Specification

class FizzBuzzSpec extends BaseSpecs2Specification {

  "FizzBuzz" should {
    val applyWith = when(target = FizzBuzz.main, _: String)
    "input1" >> {
      val input =
        """5
          |""".stripMargin
      val expected =
        """1
          |2
          |Fizz
          |4
          |Buzz
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """20
          |""".stripMargin
      val expected =
        """1
          |2
          |Fizz
          |4
          |Buzz
          |Fizz
          |7
          |8
          |Fizz
          |Buzz
          |11
          |Fizz
          |13
          |14
          |Fizz Buzz
          |16
          |17
          |Fizz
          |19
          |Buzz
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }
}

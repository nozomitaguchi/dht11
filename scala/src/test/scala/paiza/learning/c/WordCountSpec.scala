package paiza.learning.c

import utils.BaseSpecs2Specification

class WordCountSpec extends BaseSpecs2Specification {

  "WordCount" should {
    val applyWith = when(target = WordCount.main, _: String)
    "input1" >> {
      val input =
        """red green blue blue green blue
          |""".stripMargin
      val expected =
        """red 1
          |green 2
          |blue 3
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """Nagato Yukikaze Akagi Kitakami Nagato Akagi Akagi Kitakami
          |""".stripMargin
      val expected =
        """Nagato 2
          |Yukikaze 1
          |Akagi 3
          |Kitakami 2
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }
}

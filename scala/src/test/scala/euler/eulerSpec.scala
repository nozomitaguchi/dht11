package euler

import utils.BaseSpecs2Specification

class eulerSpec extends BaseSpecs2Specification {

  "_0001" should {
    val applyWith = when(target = _0001.main, _: String)
    "input1" >> {
      val input =
        """1000
          |""".stripMargin
      val expected =
        """233168
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_0002" should {
    val applyWith = when(target = _0002.main, _: String)
    "input1" >> {
      val input =
        """4000000
          |""".stripMargin
      val expected =
        """4613732
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

  "_0003" should {
    val applyWith = when(target = _0003.main, _: String)
    "input1" >> {
      val input =
        """13195
          |""".stripMargin
      val expected =
        """29
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
    "input2" >> {
      val input =
        """600851475143
          |""".stripMargin
      val expected =
        """6857
          |""".stripMargin
      applyWith(input) mustEqual expected
    }
  }

}

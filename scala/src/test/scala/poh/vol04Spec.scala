package poh

import utils.BaseSpecs2Specification
import vol04.{_0001, _0002, _0003}

class vol04Spec extends BaseSpecs2Specification {

  "_0001" should {

    val applyWith = when(target = _0001.main, _: String)

    "input1" >> {
      val input =
        """4
          |5
          |10
          |2
          |3
          |""".stripMargin

      val expected =
        """20
          |""".stripMargin

      applyWith(input) mustEqual expected
    }

    "input2" >> {
      val input =
        """3
          |100
          |23
          |12
          |""".stripMargin

      val expected =
        """135
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
  }

  "_0002" should {

    val applyWith = when(target = _0002.main, _: String)

    "input1" >> {
      val input =
        """4
          |10 5 100
          |4 0 300
          |2 2 200
          |2 3 100
          |""".stripMargin

      val expected =
        """1700
          |""".stripMargin

      applyWith(input) mustEqual expected
    }

    "input2" >> {
      val input =
        """3
          |100 99 2034
          |300 233 303
          |20 12 1022
          |""".stripMargin

      val expected =
        """30511
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
  }

  "_0003" should {

    val applyWith = when(target = _0003.main, _: String)

    "input1" >> {
      val input =
        """3 7
          |4
          |5
          |1
          |10
          |3
          |4
          |1
          |""".stripMargin

      val expected =
        """17
          |""".stripMargin

      applyWith(input) mustEqual expected
    }

    "input2" >> {
      val input =
        """6 12
          |0
          |123
          |222
          |21
          |1
          |332
          |22
          |99
          |3
          |444
          |24
          |10
          |""".stripMargin

      val expected =
        """924
          |""".stripMargin

      applyWith(input) mustEqual expected
    }
  }

}

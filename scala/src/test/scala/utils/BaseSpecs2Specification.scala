package utils

import java.io.{BufferedOutputStream, ByteArrayInputStream, ByteArrayOutputStream, PrintStream}

import org.specs2.mutable.Specification

class BaseSpecs2Specification extends Specification {
  // 並列実行しない
  sequential

  def when(target: (Array[String] => Unit), input: String): String = {
    val in = new ByteArrayInputStream(input.getBytes())
    System.setIn(in)
    val outStream = new ByteArrayOutputStream
    val out = new PrintStream(new BufferedOutputStream(outStream), true, "utf-8")
    Console.withOut(out) {
      target.apply(null)
      out.flush()
    }
    outStream.toString("utf-8")
  }

}
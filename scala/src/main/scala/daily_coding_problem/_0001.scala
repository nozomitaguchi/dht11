package daily_coding_problem

import java.util.Scanner

/** if the stack is [1, 2, 3, 4, 5],
  * it should become [1, 5, 2, 4, 3].
  * If the stack is [1, 2, 3, 4],
  * it should become [1, 4, 2, 3].
  */
object _0001 extends App {
  val sc: Scanner = new java.util.Scanner(System.in)
  val list: List[String] = sc.nextLine.split(", ").toList
  val sorted: List[String] = list.zip(list.reverse).flatMap { case (a, b) => Seq(a, b) }.distinct
  print(sorted.mkString(", "))
}

package euler

/** Problem 1 「3と5の倍数」
  *
  * 10未満の自然数のうち, 3 もしくは 5 の倍数になっているものは 3, 5, 6, 9 の4つがあり, これらの合計は 23 になる.
  *
  * 同じようにして, 1000 未満の 3 か 5 の倍数になっている数字の合計を求めよ.
  */
object _0001 extends App {
  val sc = new java.util.Scanner(System.in)
  println((1 until sc.nextInt).filter(i => i % 3 == 0 || i % 5 == 0).sum)
}

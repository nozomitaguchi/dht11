package euler

/** Problem 2 「偶数のフィボナッチ数」
  *
  * フィボナッチ数列の項は前の2つの項の和である. 最初の2項を 1, 2 とすれば, 最初の10項は以下の通りである.
  *
  * 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
  * 数列の項の値が400万以下の, 偶数値の項の総和を求めよ.
  */
object _0002 extends App {
  val limit = new java.util.Scanner(System.in).nextInt
  lazy val fib: Stream[Int] = Stream.cons(0, Stream.cons(1, fib.zip(fib.tail).map(p => p._1 + p._2)))
  println(fib.takeWhile(_ <= limit).filter(_ % 2 == 0).sum)
}

package euler

/** Problem 3 「最大の素因数」
  *
  * 13195 の素因数は 5, 7, 13, 29 である.
  *
  * 600851475143 の素因数のうち最大のものを求めよ.
  */
object _0003 extends App {
  // TODO: scalaっぽく書く. var を使わない.
  var (index, target) = (2L, new java.util.Scanner(System.in).nextLong)
  while (index <= target) if (target % index == 0) target /= index else index += 1
  println(index)
}

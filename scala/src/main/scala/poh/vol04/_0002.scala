package poh.vol04

object _0002 extends App {
  val sc = new java.util.Scanner(System.in)
  println(Seq.fill(sc.nextInt)((sc.nextInt - sc.nextInt) * sc.nextInt max 0).sum)
}

package poh.vol04

object _0003 extends App {
  val sc = new java.util.Scanner(System.in)
  val n, t, s = sc.nextInt
  val accumulations = Seq.iterate(s, t)(_ + sc.nextInt)
  val max =
    if (n == t) accumulations(t - 1)
    else (0 until t - n).map(i => accumulations(i + n) - accumulations(i)).max // ここの処理はslidingに置換え可能かも．
  println(max)
}

package p_challenge.c

object _020 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val a, b, c = sc.nextInt
    println(a * (100 - b) * (100 - c) / 10000.0)
  }
}

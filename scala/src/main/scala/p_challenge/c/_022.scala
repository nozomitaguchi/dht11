package p_challenge.c

object _022 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val ar = Seq.fill(sc.nextInt)(Seq.fill(4)(sc.nextInt))
    println(Seq(
      ar.head.head,
      ar.last(1),
      ar.indices.map(i => ar(i)(2)).max,
      ar.indices.map(i => ar(i)(3)).min).mkString(" "))
  }
}

package p_challenge.c

object _026 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val n, s, p = sc.nextInt
    // case classを使った方が分かりやすくなりそう
    val isBetterSweet: Seq[Int] => Boolean = carrot => (s - p until s + p).contains(carrot(2))
    val sweetCarrots = (1 until n).map(i => Seq(i, sc.nextInt, sc.nextInt)).filter(isBetterSweet)
    println(if (sweetCarrots.isEmpty) "not found" else sweetCarrots.maxBy(_ (1)).head)
  }
}

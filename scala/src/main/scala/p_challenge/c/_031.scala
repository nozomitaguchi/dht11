package p_challenge.c

object _031 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    // key: 国名, value: 時差
    val timeDifferences = Seq.fill(sc.nextInt)(sc.next -> sc.nextInt)
    val country = sc.next
    // TODO: ダサいのでLinkedHashMap的なやつに変える
    val diff = timeDifferences.filter(_._1 == country).head._2
    val postTime = sc.next.split(":")
    // 前日になることを考慮して24を加算後、余りを求める。
    val getLocalTime: Int => Int = time => (postTime(0).toInt + (time - diff) + 24) % 24
    timeDifferences.map(v => "%02d".format(getLocalTime(v._2)) + ":" + postTime(1)).foreach(println)
  }
}
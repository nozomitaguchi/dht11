package p_challenge.c

object _042 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val n = sc.nextInt
    val winners = Seq.fill(n * (n - 1) / 2)(sc.nextInt -> sc.nextInt).groupBy(_._1).mapValues(_.map(_._2))
    (1 to n).map(i => (1 to n).map(j => if (i == j) "-" else if (winners.get(i).exists(_.contains(j))) "W" else "L").mkString(" ")).foreach(println)
  }
}
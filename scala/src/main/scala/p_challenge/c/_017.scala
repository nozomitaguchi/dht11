package p_challenge.c

object _017 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val a, b = sc.nextInt
    Seq.fill(sc.nextInt) {
      val A, B = sc.nextInt
      if (a < A || (a == A && b > B)) "Low" else "High"
    }.foreach(println)
  }
}

package p_challenge.c

object _037 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val (_date, _time) = (sc.next.split("/"), sc.next.split(":"))
    val date = Seq(_date(0), "%02d".format(_date(1).toInt + _time(0).toInt / 24)).mkString("/")
    val time = Seq("%02d".format(_time(0).toInt % 24), _time(1)).mkString(":")
    println(Seq(date, time).mkString(" "))
  }
}
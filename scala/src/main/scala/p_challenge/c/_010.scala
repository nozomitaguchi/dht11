package p_challenge.c

import scala.math.pow

object _010 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val a, b, r = sc.nextDouble
    Seq.fill(sc.nextInt)(
      if (pow(sc.nextDouble - a, 2) + pow(sc.nextDouble - b, 2) >= pow(r, 2)) "silent" else "noisy"
    ).foreach(println)
  }
}

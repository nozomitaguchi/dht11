package p_challenge.c

object _034 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val a, op, b, _, c = sc.next
    val sign = if ("+" == op) 1 else -1
    println(if ("x" == a) c.toInt - b.toInt * sign else if ("x" == b) (c.toInt - a.toInt) * sign else a.toInt + b.toInt * sign)
  }
}
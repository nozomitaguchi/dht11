package p_challenge.c

object _013 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val n = sc.next
    Seq.fill(sc.nextInt)(sc.next).filterNot(_.contains(n)) match {
      case hateRooms if hateRooms.isEmpty => println("none")
      case hateRooms => hateRooms.foreach(println)
    }
  }
}

package p_challenge.c

object _038 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val m, n = sc.nextInt
    val machines = (1 to m).map(sc.nextInt -> _).toMap
    println(machines(machines.keys.groupBy(n % _).minBy(_._1)._2.max))
  }
}
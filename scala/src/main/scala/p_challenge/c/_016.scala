package p_challenge.c

object _016 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(sc.next
      .replaceAll("A", "4")
      .replaceAll("E", "3")
      .replaceAll("G", "6")
      .replaceAll("I", "1")
      .replaceAll("O", "0")
      .replaceAll("S", "5")
      .replaceAll("Z", "2")
    )
  }
}

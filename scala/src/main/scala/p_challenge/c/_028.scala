package p_challenge.c

object _028 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)

    val getDeffPoint: (String, String) => (Int) = (a, b) => if (a == b) 2 else if (a.length == b.length && (a diff b length) == 1) 1 else 0

    println(Seq.fill(sc.nextInt)(getDeffPoint(sc.next, sc.next)).sum)
  }
}
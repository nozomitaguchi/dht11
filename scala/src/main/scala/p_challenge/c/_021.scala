package p_challenge.c

import scala.math.pow

object _021 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val xc, yc = sc.nextInt
    val squaring = (i: Int) => pow(i, 2).toInt
    val r1, r2 = squaring(sc.nextInt)
    Seq.fill(sc.nextInt)(squaring(sc.nextInt - xc) + squaring(sc.nextInt - yc))
      .map(i => if (r1 <= i && i <= r2) "yes" else "no")
      .foreach(println)
  }
}

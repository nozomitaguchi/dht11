package p_challenge.c

object _014 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val n, radius = sc.nextInt
    (1 to n).filter(_ => radius * 2 <= Seq.fill(3)(sc.nextInt).min).foreach(println)
  }
}

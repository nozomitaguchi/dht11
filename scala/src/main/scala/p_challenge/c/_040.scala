package p_challenge.c

object _040 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val heights = Seq.fill(sc.nextInt)((sc.next, sc.nextDouble)).groupBy(_._1).mapValues(_.map(_._2))
    println(Seq(heights("ge").max, heights("le").min).mkString(" "))
  }
}
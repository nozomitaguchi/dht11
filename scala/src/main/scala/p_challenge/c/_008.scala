package p_challenge.c

object _008 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val start, end = sc.next
    val finds = s"$start.*?$end".r.findAllIn(sc.next).map(s => s.drop(start.length).dropRight(end.length))
    finds.map(s => if (s.isEmpty) "<blank>" else s).foreach(println)
  }
}
package p_challenge.c

object _041 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val rates = Seq.fill(sc.nextInt)(Seq.fill(3)(sc.nextInt))
    rates.sortBy(s => (s.head, s(1), s.last)).reverse.map(_.mkString(" ")).foreach(println)
  }
}
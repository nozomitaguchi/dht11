package p_challenge.c

object _018 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val recipe, pieceInHand = Seq.fill(sc.nextInt)(sc.next, sc.nextInt).toMap
    println(recipe.map(e => pieceInHand.getOrElse(e._1, 0) / e._2).min)
  }
}

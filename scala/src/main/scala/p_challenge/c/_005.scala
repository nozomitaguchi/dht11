package p_challenge.c

object _005 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val isIP: Array[String] => Boolean = ip => ip.length == 4 && ip.forall(s => !s.isEmpty && s.toDouble <= 255d)
    Seq.fill(sc.nextInt)(isIP(sc.next.split("[.]", -1)).toString.capitalize).foreach(println)
  }
}
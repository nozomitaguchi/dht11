package p_challenge.c

object _023 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val winningNums = List.fill(6)(sc.nextInt)
    Seq.fill(sc.nextInt)(Seq.fill(6)(sc.nextInt).count(winningNums.contains)).foreach(println)
  }
}

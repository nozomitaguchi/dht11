package p_challenge.c

object _030 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val h, w = sc.nextInt
    Seq.fill(h)(Seq.fill(w)(sc.nextInt).map(i => if (i < 128) 0 else 1).mkString(" ")).foreach(println)
  }
}
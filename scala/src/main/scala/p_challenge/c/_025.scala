package p_challenge.c

import scala.math.ceil

object _025 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val m, n = sc.nextInt
    // TODO: sparkだと、groupByKeyというのが使えるらしいので調べる。
    val sheetsPerHour = Seq.fill(n)(Seq.fill(3)(sc.nextInt)).groupBy(_.head).values
      .map(s => ceil(s.map(_ (2)).sum.toDouble / m).toInt)
    println(sheetsPerHour.sum)
  }
}

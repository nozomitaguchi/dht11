package p_challenge.c

import scala.math.round

object _006 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val n, m, k = sc.nextInt
    val scores = Seq.fill(m + 1)(Seq.fill(n)(sc.nextDouble)) match {
      case (rates :: nums) => nums.map(rates.zip(_).map { case (a, b) => a * b }.sum)
    }
    scores.sorted.reverseMap(round).take(k).foreach(println)
  }
}
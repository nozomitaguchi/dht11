package p_challenge.c

object _024 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val ar = Array(0, 0)
    Seq.fill(sc.nextLine.toInt)(sc.nextLine.split(" ")).foreach {
      case Array("SET", i, a) => ar(i.toInt - 1) = a.toInt
      case Array("ADD", a) => ar(1) = ar(0) + a.toInt
      case Array("SUB", a) => ar(1) = ar(0) - a.toInt
    }
    println(s"${ar(0)} ${ar(1)}")
  }
}

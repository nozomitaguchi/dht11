package p_challenge.c

object _029 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val m, n = sc.nextInt
    val rainyPercentByDate = Seq.fill(m)(sc.nextInt -> sc.nextInt)
    val bastDates = (0 to m - n).map(i => rainyPercentByDate.slice(i, i + n)).minBy(s => s.map(_._2).sum)
    println(bastDates.head._1 + " " + bastDates.last._1)
  }
}
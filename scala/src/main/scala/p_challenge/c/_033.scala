package p_challenge.c

object _033 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val pitches = Seq.fill(sc.nextInt)(s"${sc.next}!")
    pitches.init.foreach(println)
    println(if ("ball!" == pitches.last) "fourball!" else "out!")
  }
}
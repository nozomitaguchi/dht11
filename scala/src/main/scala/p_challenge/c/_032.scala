package p_challenge.c

object _032 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val pointRate: (Int) => Int = (i: Int) => if (i == 0) 5 else if (i == 1) 3 else if (i == 2) 2 else 1
    val prices = Seq.fill(sc.nextInt)((sc.nextInt, sc.nextInt))
    val priceSums = prices.groupBy(_._1).mapValues(_.map(_._2).sum)
    println(priceSums.map(sum => sum._2 / 100 * pointRate(sum._1)).sum)
  }
}
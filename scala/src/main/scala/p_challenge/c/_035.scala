package p_challenge.c

object _035 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(Seq.fill(sc.nextInt)(sc.next -> Seq.fill(5)(sc.nextInt)).count {
      case (faculty, records@Seq(_, b, c, d, e)) => 350 <= records.sum && (160 <= (if (faculty == "s") b + c else d + e))
    })
  }
}
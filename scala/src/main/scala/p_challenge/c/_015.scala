package p_challenge.c

object _015 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val getRate: String => Double = { s => if (s.contains('5')) 0.05 else if (s.contains('3')) 0.03 else 0.01 }
    println(Seq.fill(sc.nextInt)(getRate(sc.next) * sc.nextDouble).map(_.toInt).sum)
  }
}

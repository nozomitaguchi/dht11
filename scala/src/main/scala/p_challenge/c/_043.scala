package p_challenge.c

object _043 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val counts = Seq.fill(sc.nextInt)(sc.nextInt).groupBy(identity).mapValues(_.size)
    println(counts.filter(_._2 == counts.values.max).keys.toSeq.sorted.mkString(" "))
  }
}
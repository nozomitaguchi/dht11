package p_challenge.c

import scala.math.abs

object _019 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val convert: Int => String = i => if (i == 0) "perfect" else if (abs(i) == 1) "nearly" else "neither"
    Seq.fill(sc.nextInt)(sc.nextInt).map(i => (1 until i).filter(j => i % j == 0).sum - i).map(convert).foreach(println)
  }
}

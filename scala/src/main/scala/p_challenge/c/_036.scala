package p_challenge.c

object _036 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val contestants = ((sc.nextInt, sc.nextInt), (sc.nextInt, sc.nextInt))
    val times = 0 +: Seq.fill(4)(sc.nextInt)
    val race: ((Int, Int)) => Int = c => if (times(c._1) < times(c._2)) c._1 else c._2
    val winners = (race(contestants._1), race(contestants._2)) match {
      case (win1, win2) if sc.nextInt < sc.nextInt => Seq(win2, win1)
      case (win1, win2) => Seq(win1, win2)
    }
    winners.foreach(println)
  }
}
package p_challenge.c

object _039 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(sc.nextLine.toCharArray.map(s => if ('<' == s) 10 else if ('/' == s) 1 else 0).sum)
  }
}
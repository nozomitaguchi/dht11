package p_challenge.b

object _008 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val n = sc.nextInt
    println(Seq.fill(sc.nextInt)(Seq.fill(n)(sc.nextInt).sum).filter(0 < _).sum)
  }
}

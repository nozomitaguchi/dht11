package p_challenge.b

object _004 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val ipRanges = sc.nextLine.split('.').map {
      case ip if ip == "*" => Array(0, 225)
      case ip if ip.head == '[' => ip.tail.init.split('-').map(_.toInt)
      case ip => Array(ip.toInt, ip.toInt)
    }.map { case Array(a, b) => a to b }

    val logs = Seq.fill(sc.nextLine.toInt)(sc.nextLine.split(' '))
    val isInRange: (Array[String]) => Boolean =
      log => log(0).split('.').map(_.toInt).zip(ipRanges).forall { case (ip, range) => range.contains(ip) }

    logs.filter(isInRange).map(log => s"${log(0)} ${log(3).tail} ${log(6)}").foreach(println)
  }
}

package p_challenge.b

import java.math.BigDecimal

object _006 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val oY, s, theta, x, y, a = sc.nextInt
    val (g, radian, r) = (9.8, Math.toRadians(theta), a / 2d)
    val h = oY + x * Math.tan(radian) - (g * Math.pow(x, 2)) / (2 * Math.pow(s, 2) * Math.pow(Math.cos(radian), 2))
    val point = Math.abs(y - h)
    println(if (point > r) "Miss" else "Hit " + new BigDecimal(point).setScale(1, BigDecimal.ROUND_HALF_UP))
  }
}

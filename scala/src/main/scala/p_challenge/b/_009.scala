package p_challenge.b

object _009 {
  def main(args: Array[String]): Unit = {
    // TODO: 無駄に複雑なので簡単にする.
    val sc = new java.util.Scanner(System.in)
    val n = sc.nextInt
    val table = Seq.fill(n)(Seq(sc.next -> sc.nextInt, "rest" -> 10)).flatten.init
    val showTimeSums = table.inits.toList.init.reverseMap(shows => shows.last._1 -> shows.map(_._2).sum)
    val breakTaker = showTimeSums.filterNot(_._1 == "rest").find(2 * 60 <= _._2).map(_._1)
    val afterLunch = breakTaker.flatMap(name => table.zipWithIndex.find(_._1._1 == name).map(_._2))
    val addLunchTime: Int => Int = index => if (afterLunch.isDefined && afterLunch.get <= index) 50 else 0
    val timeToHHMM: Int => String = time => "%02d".format(time / 60) + ":" + "%02d".format(time % 60)
    table.zip(showTimeSums.map(_._2)).zipWithIndex.filterNot(_._1._1._1 == "rest").map {
      case (((name, time), sum), index) => val end = 10 * 60 + sum + addLunchTime(index)
        Seq(timeToHHMM(end - time), "-", timeToHHMM(end), name).mkString(" ")
    }.foreach(println)
  }
}

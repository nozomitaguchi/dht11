package p_challenge.d

object _059 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val line = sc.nextLine
    println(if (line == "J J") "J Q" else line)
  }
}
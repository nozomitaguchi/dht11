package p_challenge.d

object _048 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val dates = Array.fill(5)(sc.nextInt)
    dates.zip(dates.tail).map { case (a, b) => b - a }.foreach(println)
  }
}
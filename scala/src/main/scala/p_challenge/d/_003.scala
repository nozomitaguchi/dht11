package p_challenge.d

object _003 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val n = sc.nextInt
    println(1 to 9 map (_ * n) mkString " ")
  }
}
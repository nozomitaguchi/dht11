package p_challenge.d

object _005 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val m, n = sc.nextInt
    println(List.iterate(m, 10)(_ + n).mkString(" "))
  }
}
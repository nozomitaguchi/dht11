package p_challenge.d

object _071 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val isDayForLaundry = sc.nextInt match {
      case temperature if 25 <= temperature => 40 < sc.nextInt
      case _ => sc.nextInt <= 40
    }
    println(if (isDayForLaundry) "Yes" else "No")
  }
}
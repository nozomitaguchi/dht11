package p_challenge.d

import scala.math.pow

object _056 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println((pow(sc.nextDouble, 3) - pow(sc.nextDouble, 3)).toInt)
  }
}
package p_challenge.d

object _002 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val a, b = sc.nextInt
    println(if (a == b) "eq" else a max b)
  }
}
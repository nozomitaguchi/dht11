package p_challenge.d

object _058 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(Seq("A", "B", "A").flatMap(str => Array.fill(sc.nextInt)(str)).mkString)
  }
}
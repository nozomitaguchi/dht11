package p_challenge.d

import scala.math.abs

object _012 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(abs(sc.nextInt))
  }
}
package p_challenge.d

object _034 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val n = 21 % sc.nextInt
    println(if (n == 0) 3 else n)
  }
}
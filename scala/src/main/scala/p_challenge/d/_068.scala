package p_challenge.d

object _068 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val n = sc.nextInt
    val sunDayCount = sc.next.toCharArray.count(_ == 'S')
    println(sunDayCount + " " + (n - sunDayCount))
  }
}
package p_challenge.d

object _015 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    (1 to sc.nextInt).reverseMap(_.toString).foreach(println)
  }
}
package p_challenge.d

import scala.math.pow

object _022 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(pow(sc.nextDouble, 2).toInt * 6)
  }
}
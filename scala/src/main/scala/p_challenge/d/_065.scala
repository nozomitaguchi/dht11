package p_challenge.d

object _065 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val head = sc.nextInt / 100
    println(if (head == 2) "ok" else if (head == 4) "error" else "unknown")
  }
}
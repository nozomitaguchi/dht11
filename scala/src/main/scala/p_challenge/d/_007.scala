package p_challenge.d

object _007 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    // List.fill(sc.nextInt)("*").mkString
    // Array.fill(sc.nextInt)("*").mkString
    // これが早いらしい。http://www.ne.jp/asahi/hishidama/home/tech/scala/collection/seq.html
    println("".padTo(sc.nextInt, '*'))
  }
}
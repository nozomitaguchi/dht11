package p_challenge.d

object _069 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val average = List.fill(7)(sc.nextDouble).sum / 7.0
    println(BigDecimal.double2bigDecimal(average).setScale(1, scala.math.BigDecimal.RoundingMode.HALF_UP))
  }
}
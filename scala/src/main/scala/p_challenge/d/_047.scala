package p_challenge.d

object _047 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val medals = Array("Gold", "Silver", "Bronze")
    val winners = Array.fill(3)(sc.next)
    medals.zip(winners).map { case (medal, winner) => s"$medal $winner" }.foreach(println)
  }
}
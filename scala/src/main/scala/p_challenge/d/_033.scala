package p_challenge.d

object _033 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(List.fill(2)(sc.next.head).mkString("."))
  }
}
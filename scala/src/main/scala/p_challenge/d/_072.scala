package p_challenge.d

import scala.math.ceil

object _072 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(ceil(sc.nextDouble / sc.nextDouble).toInt * sc.nextInt)
  }
}
package p_challenge.d

object _021 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(if (sc.next == sc.next) "Yes" else "No")
  }
}
package p_challenge.d

object _038 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val n = sc.nextInt
    println(n * (n - 1) / 2)
  }
}
package p_challenge.d

object _044 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val name = sc.next
    val honorific = if (sc.next == "F") "Ms." else "Mr."
    println(s"Hi, $honorific $name")
  }
}
package p_challenge.d

object _006 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val rates = Map("km" -> 1000000, "m" -> 1000, "cm" -> 10)
    println(sc.nextInt * rates.getOrElse(sc.next, 0))
  }
}
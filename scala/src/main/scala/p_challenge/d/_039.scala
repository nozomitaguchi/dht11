package p_challenge.d

object _039 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val a, b, c = sc.nextInt
    println(if (a == b && b == c) "YES" else "NO")
  }
}
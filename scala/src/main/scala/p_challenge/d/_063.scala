package p_challenge.d

object _063 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val nums = Array.fill(6)(sc.nextInt)
    println(nums.sorted.indexOf(nums.last) + 1)
  }
}
package p_challenge.d

object _040 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(Array.fill(7)(sc.nextInt).count(_ <= 30))
  }
}
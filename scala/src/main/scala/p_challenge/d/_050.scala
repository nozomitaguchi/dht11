package p_challenge.d

object _050 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(Array.fill(2)(5 min sc.nextInt).sum)
  }
}
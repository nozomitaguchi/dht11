package p_challenge.d

object _041 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(if (sc.nextInt < sc.nextInt * sc.nextInt) "OK" else "NG")
  }
}
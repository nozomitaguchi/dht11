package p_challenge.d

object _062 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val nums = Array.iterate(0, 4)(i => i + sc.nextInt)
    nums.zip(nums.tail).map { case (a, b) => "ABCDEFGHIJ".substring(a, b) }.foreach(println)
  }
}
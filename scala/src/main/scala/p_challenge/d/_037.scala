package p_challenge.d

import scala.math.ceil

object _037 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val m, n = sc.nextDouble
    println(ceil(n / m).toInt)
  }
}
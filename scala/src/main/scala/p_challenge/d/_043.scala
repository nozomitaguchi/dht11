package p_challenge.d

object _043 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val i = sc.nextInt
    println(if (i <= 30) "sunny" else if (71 <= i) "rainy" else "cloudy")
  }
}
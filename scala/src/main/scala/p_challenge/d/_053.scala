package p_challenge.d

object _053 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(if (Seq("candy", "chocolate") contains sc.next) "Thanks!" else "No!")
  }
}
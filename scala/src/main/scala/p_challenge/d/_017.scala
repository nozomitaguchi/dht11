package p_challenge.d

object _017 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val ints = List.fill(5)(sc.nextInt)
    println(s"${ints.max}\n${ints.min}")
  }
}
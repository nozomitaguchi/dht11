package p_challenge.d

object _054 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val count = 11 - sc.next.length
    println(if (0 < count) count else "OK")
  }
}
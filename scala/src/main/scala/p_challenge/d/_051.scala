package p_challenge.d

object _051 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(if (5 <= Array.fill(10)(sc.next).count(_ == "W")) "OK" else "NG")
  }
}
package p_challenge.d

object _066 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val m, n = sc.nextInt
    println(if (m > n) "No" else n - m)
  }
}
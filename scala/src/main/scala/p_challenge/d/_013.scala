package p_challenge.d

object _013 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val m, n = sc.nextInt
    println(m / n + " " + m % n)
  }
}
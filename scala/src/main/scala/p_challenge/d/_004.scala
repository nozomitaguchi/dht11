package p_challenge.d

object _004 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val names = List.fill(sc.nextInt)(sc.next).mkString(",")
    println(s"""Hello $names.""")
  }
}
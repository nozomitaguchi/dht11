package p_challenge.d

object _008 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(if (sc.nextInt % 2 == 0) "even" else "odd")
  }
}
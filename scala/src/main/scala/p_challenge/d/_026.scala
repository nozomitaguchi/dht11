package p_challenge.d

object _026 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    println(List.fill(7)(sc.next).count(_ == "no"))
  }
}
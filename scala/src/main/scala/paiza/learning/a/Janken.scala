package paiza.learning.a

import paiza.learning.a.ScissorsPaperRock.{Paper, Rock, Scissor, Toss, winnableTosses}

object Janken extends App {
  val sc = new java.util.Scanner(System.in)
  val tossPatterns = new TossPatterns(sc.nextInt(), sc.nextInt())
  val winnableCounts = {
    winnableTosses(sc.next().split("")).groupBy(identity).mapValues(_.length)
  }
  println(new TossAnalyzer(tossPatterns, winnableCounts).maxOfWinnable)
}

/** 勝ちパターンを分析するクラス.
  *
  * @param tossPatterns   手札のパターン
  * @param winnableCounts 勝ちパターン
  */
class TossAnalyzer(tossPatterns: TossPatterns, winnableCounts: Map[Toss, Int]) {

  val maxOfWinnable: Int = tossPatterns.filteredNColumns.map(countWinnable).max

  /** 手札が何回勝てるか数える.
    *
    * @param tosses 手札
    * @return 勝利回数
    */
  private def countWinnable(tosses: Map[Toss, Int]): Int = {
    winnableCounts.map(m => Math.min(tosses.getOrElse(m._1, 0), m._2)).sum
  }

}

/** 手札のパターンクラス.
  *
  * @param n 要素数
  * @param m 指の合計数
  */
class TossPatterns(n: Int, m: Int) {

  val lcm: Int = 10

  /* m を 5, 2 の要素数に分解するが,
   * 最大公約数の 10 が含まれるの場合, (5, 5), (2, 2, 2, 2, 2)の 2 パターン存在するので
   * パターン数と, それ以外の 5, 2 の出現数に分解する.
   *
   * m = 23 の時, (1, 1, 4)
   */
  val (patternCount, modPapers, modScissors) = {
    val isOdd = m % 2 == 0
    val patternCount: Int = if (isOdd) m / lcm else Math.max((m - Paper.fingers) / lcm, 0)
    val mod: Int = m - patternCount * lcm
    val paperCounts = if (isOdd) 0 else 1 // データセット上, 奇数の場合は必ず5が含まれる.
    val scissorCounts = (mod - paperCounts * Paper.fingers) / Scissor.fingers
    (patternCount, paperCounts, scissorCounts)
  }

  // 要素数 n 個のものに絞った手札
  val filteredNColumns: Seq[Map[Toss, Int]] = {
    (0 to patternCount).filter(hasNColumns).map(tosses)
  }

  /** 作成No.に沿って n 個の要素からなる指の数の合計が m の手札を作成する.
    *
    * @param i 作成No.
    * @return 手札
    */
  private def tosses(i: Int): Map[Toss, Int] = {
    Map(Rock -> countRocks(i), Scissor -> countScissors(i), Paper -> countPapers(i))
  }

  private def countPapers(i: Int): Int = i * 2 + modPapers

  private def countScissors(i: Int): Int = (patternCount - i) * 5 + modScissors

  private def countRocks(i: Int): Int = n - countPapers(i) - countScissors(i)

  /** 手札が要素数が n と一致するかを判断する.
    * もともと計算はしているが, グーの出現数がマイナスの場合は不正値として扱う.
    *
    * @param i 作成No.
    * @return 手札が要素数が n の場合 true, それ以外は false
    */
  private def hasNColumns(i: Int): Boolean = 0 <= countRocks(i)

}

/** じゃんけんオブジェクト */
object ScissorsPaperRock {

  sealed abstract class Toss(val fingers: Int) {
    val lossHand: Toss
  }

  case object Rock extends Toss(0) {
    val lossHand: Toss = Paper
  }

  case object Paper extends Toss(5) {
    val lossHand: Toss = Scissor
  }

  case object Scissor extends Toss(2) {
    val lossHand: Toss = Rock
  }

  def convert(hand: String): Toss = {
    hand match {
      case "C" => Scissor
      case "P" => Paper
      case "G" => Rock
      case _ => throw new Exception
    }
  }

  /** 相手の手札に勝つことのできる手札を取得する.
    *
    * @param toss 相手の手札
    * @return 相手の手札に勝つことのできる手札
    */
  def winnableTosses(toss: Array[String]): Seq[Toss] = toss.map(convert(_).lossHand)

}

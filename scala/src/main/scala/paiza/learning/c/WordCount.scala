package paiza.learning.c

object WordCount extends App {

  val words: Array[String] = new java.util.Scanner(System.in).nextLine().split(" ")

  val wordCounts: Map[String, Int] = words.groupBy(identity).mapValues(_.length)

  words.distinct.map(word => s"$word ${wordCounts.getOrElse(word, 0)}").foreach(println)

}

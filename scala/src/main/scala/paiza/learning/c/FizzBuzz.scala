package paiza.learning.c

object FizzBuzz extends App {

  val count = new java.util.Scanner(System.in).nextInt()

  (1 to count).map {
    case i if i % 15 == 0 => "Fizz Buzz"
    case i if i % 3 == 0 => "Fizz"
    case i if i % 5 == 0 => "Buzz"
    case i => i
  }.foreach(println)

}

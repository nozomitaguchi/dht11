package paiza.learning.d

object SortNumber extends App {
  val sc = new java.util.Scanner(System.in)
  Seq.fill(sc.nextInt)(sc.nextInt).sorted.foreach(println)
}

package paiza.learning.b

object LongTable {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    val n, m = sc.nextInt()
    val groups: Seq[(Int, Int)] = Seq.fill(m)((sc.nextInt(), sc.nextInt()))
    val seated: Seq[Int] = groups.map(positions(n, _)).fold(Seq.empty)(sitDown)
    println(seated.length)
  }

  def positions(n: Int, group: (Int, Int)): Seq[Int] = {
    (group._2 until (group._1 + group._2)).map(_ % n)
  }

  def sitDown(seated: Seq[Int], notSeated: Seq[Int]): Seq[Int] = {
    if (notSeated.exists(seated.contains)) seated else seated ++ notSeated
    //    seated intersect notSeated match {
    //      case seq if seq.isEmpty => seated ++ notSeated
    //      case _ => seated
    //    }
  }
}
